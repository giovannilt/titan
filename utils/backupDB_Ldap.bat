@echo off
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set date=%%c%%a%%b)
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a%%b)
echo %date%_%mytime%

set BK_BASE=D:\PLM4U\WindchillBK
cd %BK_BASE%
mkdir %date%

set BK_FOLDER=%BK_BASE%\%date%

exp pdmlink/pdmlink@wind file=%BK_FOLDER%\oracle_pdmlink.dmp log=%BK_FOLDER%\oracle_pdmlink.log

D:\ptc\WindchillDS\server\bat\export-ldif.bat "--ldifFile" "%BK_FOLDER%\root.ldif" "--backendID" "userRoot" "--hostname" "0.0.0.0" "--port" "4444" "--bindDN" "cn=Manager" "--bindPassword" "ldapadmin" "--trustAll" "--noPropertiesFile"

rem imp pdmlink/pdmlink@wind file=D:\backup\FromW9\20201218_1547_oracle_pdmlink.dmp log= D:\Installer\temp\log_import.log full=y
rem D:\ptc\WindchillDS\server\bat\import-ldif.bat --ldifFile D:\backup\FromW9\20201218_1540_root.ldif --backendID userRoot --append --hostname 0.0.0.0 --port 4444 --bindDN cn=Manager --bindPassword ldapadmin --trustAll -- noPropertiesFile