<%@ page import="ext.custom.titan.windchill.numbering.WTPartNumberGenerator"%>
<%
	WTPartNumberGenerator generator = WTPartNumberGenerator.TitanWTPartNumberGenerator();
	String error = "";
	String genResult= "" ;
	String typeV= "" ;
	String typeValue= request.getParameter("typeValue");
	if(typeValue == null || typeValue.equals("")){
		error = "Unable to estabilish type value";
	}else{
		if(typeValue.equalsIgnoreCase("DEFINITIVO")){
			typeV ="Released ";
			genResult = generator.getNextReleasedWTPartNumber();
		}else{
			typeV ="Provisional ";
			genResult = generator.getNextProvisionalWTPartNumber();
		}
	}
	if("".equals(error)){
%>
		<div>
			<span style="color: blue; font-size: 16px; font-weight: bold;">New Code generated:</span>
			<input type="text" readonly="readonly" value="<%=genResult%>">
			<span style="color: red; font-size: 16px; font-weight: bold;"> <%=typeV%></span>
		</div>
<%
	}
	else{
%>
		<span style="color: red; font-size: 16px; font-weight: bold;">Errors while creating new code:</span>
		<%=error%>
<%
	}
%>