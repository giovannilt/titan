<%@ include file="/netmarkets/jsp/util/begin.jspf"%>
<%
	request.setAttribute("contextType","titan");
	request.setAttribute("contextAction", "codifica");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<script type="text/javascript" language="javascript">
	
	function generateNewCode(){
		var resultContainer = document.getElementById("resultContainer");
		var type = document.getElementById("type");
		if(type == null){
			alert('Unable to estabilish type= definitivo o provvisorio');
			return;
		}
		
		var typeINDEX = type.selectedIndex;
		if(typeINDEX < 0){
			alert('Unable to estabilish type= definitivo o provvisorio');
			return;
		}
		
		var typeValue = type.options[typeINDEX].text;
		
		try {
			var url = getBaseHref()+ "/netmarkets/jsp/ext/partNumber/partNumberGeneratorDelegate.jsp";
			var options = {
				method: 'get',
				asynchronous: true,
				parameters: 'typeValue=' + encodeURIComponent(typeValue),
				onSuccess : function (transport){
					resultContainer.innerHTML = transport.responseText;
				}
			};
			var transport = requestHandler.doRequest(url, options);
		}
		catch (e) {
		}
	}
</script>
<div style="margin-left: 25px;">
	<table style="width:50%">
	  <tr>
		<td>Type: <select id="type" name="type">
			<option value="DEFINITIVO">DEFINITIVO</option>
			<option value="PROVVISORIO">PROVVISORIO</option>
		</select></td>
		<td> 
		</td>
	  </tr>
	  <tr>
		<td><input type="button" value="Take the new code" onClick="javascript:generateNewCode();"></td> 
	  <td></td>
	  </tr>
	  <tr>
		<td><div id="resultContainer" style="text-align: left; padding-left: 50px; width: 600px;">&nbsp;</div></td>
	 <td></td>
	  
	 </tr>
	</table>
</div>
<%@ include file="/netmarkets/jsp/util/end.jspf"%>
