<%@ page import="ext.custom.titan.windchill.pdf.queue.PrintQueueHelper"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Arrays"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	
	String oids = request.getParameter("oids");
	String printer = request.getParameter("printer");
	String paperFormat = request.getParameter("paperFormat");
	String color = request.getParameter("color");
	String copies = request.getParameter("copies");
	
	if(oids == null || printer == null || paperFormat == null || color == null || copies == null
		|| "".equals(printer) || "".equals(oids) || "".equals(paperFormat) || "".equals(color) || "".equals(copies)){
		
		out.println("<script type=\"text/javascript\" language=\"javascript\">");
		out.println("alert('Parametri richiesti mancanti');");
		out.println("window.close();");
		out.println("</script>");
	}
	else{
		String[] partOids = oids.split("~");
		List l = Arrays.asList(partOids);
		Vector<String> v = new Vector(l);
		try{
			PrintQueueHelper.service.addToPrintQueue(v, printer, paperFormat, copies, color);
			out.println("<script type=\"text/javascript\" language=\"javascript\">");
			out.println("alert('File inviati alla stampante: " + partOids.length + "');");
			out.println("window.close();");
			out.println("</script>");
		}
		catch(Exception e){
			e.printStackTrace();
			String error = e.getLocalizedMessage();
			error = error.replaceAll("\n", "");
			out.println("<script type=\"text/javascript\" language=\"javascript\">");
			out.println("alert('Errore di invio in stampa " + error + "');");
			out.println("window.close();");
			out.println("</script>");
		}
	}
%>
<%@ include file="/netmarkets/jsp/util/end.jspf"%>