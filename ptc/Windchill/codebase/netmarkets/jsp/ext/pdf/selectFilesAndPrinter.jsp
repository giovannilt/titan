<%@ include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%@ taglib prefix="jca" uri="http://www.ptc.com/windchill/taglib/components" %>
<%@ page import="ext.custom.titan.windchill.pdf.queue.PrintQueueHelper,
				 com.ptc.netmarkets.model.NmOid,
				 wt.part.WTPart,
				 wt.fc.Persistable,
				 wt.util.WTException,
                 com.ptc.netmarkets.object.objectResource,
                 com.ptc.netmarkets.project.NmProject,
                 com.ptc.netmarkets.project.NmProjectCommands,
                 com.ptc.netmarkets.project.projectResource,
                 java.util.ResourceBundle,
				 java.util.Arrays,
				 java.util.*,
				 java.util.Vector,
				 wt.fc.ReferenceFactory"
%>
<jsp:useBean id="printers" class="ext.custom.titan.windchill.pdf.beans.PrinterBean" scope="request" />
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	
	List<Persistable> persistableList = printers.getOidObjects(commandBean);

	String oids = "";
	ReferenceFactory factory = new ReferenceFactory();
	
	for(int i = 0; i < persistableList.size(); i++){
		WTPart part = (WTPart) persistableList.get(i);
		oids= part.getPersistInfo().getObjectIdentifier()+"~"+oids;
	}
%>
<script type="text/javascript" language="javascript">
	function sendToPrinter(){
		var mainForm = document.getElementById("mainform");
		mainForm.action = "netmarkets/jsp/ext/pdf/selectFilesAndPrinter_Step.jsp";
		mainForm.method = "post";
		mainForm.target = "_self";
		mainForm.submit();
	}
	
	function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
		if(charCode == 8) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;

		return true;
    }
	
</script>
<style type="text/css">
	.leaftFloating{
		float: left;
	}
	
	.fixed150width{
		width: 150px;
	}
	
	#summaryTable {
		border-style: solid;
		border-width: 1px;
		border-color: black;
	}
	
	#summaryTable th {
		background-color: #FF4951;
		width: 50%;
	}
	
	#summaryTable td {
		width: 200px;
	}
	
	.oddColor{
		background-color: #F8F0C4;
	}
	
	.pairColor{
		background-color: #D8EAEA;
	}
	
	#summaryContainer{
		text-align: center;
		width: 100%;
		overflow: true;
		height: 150px;
		overflow: auto;
	}
</style>
<div>
	<div id="summaryContainer">
		<table id="summaryTable">
			<thead>
				<tr>
					<th>
						Numero
					</th>
					<th>
						Descrizione
					</th>
				</tr>
			</thead>
			<tbody>
<%
				for(int i = 0; i < persistableList.size(); i++){
					WTPart part = (WTPart) persistableList.get(i);
					String tdClass = ((i % 2) == 0) ? "pairColor" : "oddColor";
%>
				<tr>
					<td class="<%=tdClass%>">
						<%=part.getNumber()%>
					</td>
					<td class="<%=tdClass%>">
						<%=part.getName()%>
					</td>
				</tr>
<%
				}
%>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Stampante:</span>
	</div>
	<div class="leaftFloating">
		<select id="printer" name="printer">
			<c:forEach var="option" items="${printers.printerMap}">
			<option value="${option.key}">${option.value}</option>
			</c:forEach>
		</select>
	</div><br clear="all">
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Formato Carta:</span>
	</div>
	<div class="leaftFloating">
		<select id="paperFormat" name="paperFormat">
			<option value="pdf" selected="selected">Auto</option>
			<option value="841x1189mm">A0</option>
			<option value="594x841mm">A1</option>
			<option value="66">A2</option>
			<option value="8">A3</option>
			<option value="9">A4</option>
		</select>
	</div><br clear="all">
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Numero Copie:</span>
	</div>
	<div class="leaftFloating">
		<input type="text" value="1" name="copies" onKeyPress="javascript:return isNumberKey(event);">
	</div><br clear="all">
	
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Opzioni Cromatiche:</span>
	</div>
	<div class="leaftFloating">
		<input type="radio" name="color" value="1" checked="checked">B/N<br>
		<input type="radio" name="color" value="2">Colori<br>
	</div><br clear="all">
	
	<input type="hidden" value="<%=oids%>" name="oids" />
	
	<button value="Print" class=" wizBtn" onClick="javascript:sendToPrinter();">Print</button>
</div>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>