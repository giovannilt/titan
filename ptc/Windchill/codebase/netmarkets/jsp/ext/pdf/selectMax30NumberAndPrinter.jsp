<%@ include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%
	request.setAttribute("contextType","titan");
	request.setAttribute("contextAction", "list_print");
%>
<jsp:useBean id="printers" class="ext.custom.titan.windchill.pdf.beans.PrinterBean" scope="request" />
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	
%>
<script type="text/javascript" language="javascript">
	function sendToPrinter(){
		var mainForm = document.getElementById("mainform");
		mainForm.action = "netmarkets/jsp/ext/pdf/selectMax30NumberAndPrinter_Step.jsp";
		mainForm.method = "post";
		mainForm.submit();
	}
	
	function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
		if(charCode == 8) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;

		return true;
    }
	
</script>
<style type="text/css">
	.leaftFloating{
		float: left;
	}
	
	.fixed150width{
		width: 150px;
	}
	
	#summaryTable {
		border-style: solid;
		border-width: 1px;
		border-color: black;
	}
	
	#summaryTable th {
		background-color: #FF4951;
		width: 50%;
	}
	
	#summaryTable td {
		width: 200px;
	}
	
	.oddColor{
		background-color: #F8F0C4;
	}
	
	.pairColor{
		background-color: #D8EAEA;
	}
	
	#summaryContainer{
		text-align: center;
		width: 100%;
		overflow: true;
		height: 150px;
		overflow: auto;
	}
</style>
<br>
<div style="float: left; margin-left: 50px; width: 500px;">
	<div id="print_list">
		<%
			for(int i = 0; i < 30; i++){
		%>
			<div style="float: left; width: 200px">Number: <%=(i + 1)%>:</div>
			<div><input type="text" value="" name="print_object_<%=i%>"></div>
		<%
			}
		%>
	</div>
	<br>
	<br>
</div>
<div>
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Printer:</span>
	</div>
	<div class="">
		<select id="printer" name="printer">
			<c:forEach var="option" items="${printers.printerMap}">
			<option value="${option.key}">${option.value}</option>
			</c:forEach>
		</select>
	</div><br>
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Paper Format:</span>
	</div>
	<div class="">
		<select id="paperFormat" name="paperFormat">
			<option value="pdf" selected="selected">Auto</option>
			<option value="841x1189mm">A0</option>
			<option value="594x841mm">A1</option>
			<option value="66">A2</option>
			<option value="8">A3</option>
			<option value="9">A4</option>
		</select>
	</div><br>
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Copies Number:</span>
	</div>
	<div class="">
		<input type="text" value="1" name="copies" onKeyPress="javascript:return isNumberKey(event);">
	</div><br>
	
	
	<div class="leaftFloating fixed150width">
		<span style="font-weight: bold;">Color:</span>
	</div>
	<div class="">
		<input type="radio" name="color" value="1" checked="checked">B/N<br>
		<input type="radio" name="color" value="2">Color<br>
	</div><br>
</div>

<button value="Stampa Oggetti" class="" onClick="javascript:sendToPrinter();">Send to Printer</button>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>