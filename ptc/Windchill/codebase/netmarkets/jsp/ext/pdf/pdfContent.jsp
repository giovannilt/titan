<% request.setAttribute(NmAction.SHOW_CONTEXT_INFO, "false"); %>
<%@ page import="wt.part.WTPart"%>
<%@ include file="/netmarkets/jsp/util/begin.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/components" prefix="jca"%>

<jca:describeTable var="described" id="part.pdfContent" type="wt.content.ApplicationData"  label="PDF" configurable="false">
   <jca:describeColumn id="formatIcon"/>
   <jca:describeColumn id="name"/>
</jca:describeTable>

<%-->Get a component model for our table<--%>
<jca:getModel var="tableModelDesc" descriptor="${described}"
              serviceName="ext.custom.titan.windchill.pdf.commands.PDFContentCommand" 
              methodName="getPDFContent" >
    <jca:addServiceArgument type="wt.part.WTPart" value="${param.oid}"/>
</jca:getModel>

<%-->Get the NmHTMLTable from the command<--%>
<jca:renderTable model="${tableModelDesc}"  pageLimit="0" showCount="true"/>

<%-->*****************************************************************************************************<--%>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>