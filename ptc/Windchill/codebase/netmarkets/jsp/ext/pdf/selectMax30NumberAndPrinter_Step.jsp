<%@ include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%@ page import="ext.custom.titan.windchill.pdf.queue.PrintQueueHelper"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Vector"%>
<%@ page import="wt.part.WTPart"%>
<%@ page import="ext.custom.titan.windchill.pdf.utils.WTPartUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<style type="text/css">
	.leaftFloating{
		float: left;
	}
	
	.fixed150width{
		width: 150px;
	}
	
	#summaryTable {
		border-style: solid;
		border-width: 1px;
		border-color: black;
	}
	
	#summaryTable th {
		background-color: #FF4951;
		width: 50%;
	}
	
	#summaryTable td {
		width: 200px;
	}
	
	.oddColor{
		background-color: #F8F0C4;
	}
	
	.pairColor{
		background-color: #D8EAEA;
	}
	
	#summaryContainer{
		text-align: center;
		width: 100%;
		overflow: true;
		height: 150px;
		overflow: auto;
	}
</style>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	
	String printer = request.getParameter("printer");
	String paperFormat = request.getParameter("paperFormat");
	String color = request.getParameter("color");
	String copies = request.getParameter("copies");
	
	if(printer == null || paperFormat == null || color == null || copies == null
		|| "".equals(printer) || "".equals(paperFormat) || "".equals(color) || "".equals(copies)){
		//Errore di parametro richiesto mancante
%>
	<div>
		<br>
		<br>
		Missing parameters
	</div>
<%
		return;
	}
	else{
		ArrayList<String> missingParts = new ArrayList<String>();
		ArrayList<WTPart> printParts = new ArrayList<WTPart>();
		Map<?,?> parms = request.getParameterMap();
		for(Iterator<?> iter = parms.keySet().iterator(); iter.hasNext(); ) {
			Object o = iter.next();
			if(o instanceof String) {
				String key = (String) o;
				if(key.indexOf("print_object_") > -1) {
					String value = request.getParameter(key);
					if(value != null && !value.equals("")){
						value = value.trim();
						WTPart part = WTPartUtils.getLatestVersionWTPart(value, "Design");
						if(part == null)
							missingParts.add(value);
						else
							printParts.add(part);
					}
				}
			}
		}
		try{
%>
		<div style="text-align: center;">
<%
			if(printParts.size() > 0) {
				Vector<String> v = new Vector<String>();
				for(int i = 0; i < printParts.size(); i++) {
					v.add(printParts.get(i).toString());
				}
				PrintQueueHelper.service.addToPrintQueue(v, printer, paperFormat, copies, color);
%>
					<br>
					<br>
					Parts sent to printer<br>
					<table id="summaryTable">
						<thead>
							<tr>
								<th>
									Number
								</th>
								<th>
									Description
								</th>
							</tr>
						</thead>
						<tbody>
<%
				for(int j = 0; j < printParts.size(); j++) {
					String tdClass = ((j % 2) == 0) ? "pairColor" : "oddColor";
%>
							<tr>
								<td class="<%=tdClass%>"><%=printParts.get(j).getNumber()%></td>
								<td class="<%=tdClass%>"><%=printParts.get(j).getName()%></td>
							</tr>
<%
				}
%>
						</tbody>
					</table>
<%
			}
			if(missingParts.size() > 0) {
%>
					<br>
					<br>
					Parts Missing<br>
					<table id="summaryTable">
						<thead>
							<tr>
								<th>
									Number
								</th>
							</tr>
						</thead>
						<tbody>
<%
				for(int k = 0; k < missingParts.size(); k++) {
					String tdClass = ((k % 2) == 0) ? "pairColor" : "oddColor";
%>
							<tr>
								<td class="<%=tdClass%>"><%=missingParts.get(k)%></td>
							</tr>
<%
				}
%>
						</tbody>
					</table>
					<br>
					<br>
				</div>
<%
			}
		}
		catch(Exception e){
			e.printStackTrace();
			String error = e.getLocalizedMessage();
			error = error.replaceAll("\n", "");
			error = error.replaceAll("\"", "\\\"");
			error = error.replaceAll("\'", "\\\'");
			out.println("<script type=\"text/javascript\" language=\"javascript\">");
			out.println("alert('Errore di invio in stampa " + error + "');");
			out.println("window.close();");
			out.println("</script>");
		}
	}
%>
<%@ include file="/netmarkets/jsp/util/end.jspf"%>