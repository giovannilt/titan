package ext.custom.tools;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import wt.csm.constraint.liteconstraint.AbstractCSMAttributeConstraintView;
import wt.csm.navigation.ClassificationNode;
import wt.csm.navigation.litenavigation.ClassificationNodeDefaultView;
import wt.csm.navigation.litenavigation.ClassificationNodeNodeView;
import wt.csm.navigation.litenavigation.ClassificationStructDefaultView;
import wt.csm.navigation.service.ClassificationHelper;
import wt.csm.navigation.service.NavigationHelper;
import wt.fc.Persistable;
import wt.fc.ReferenceFactory;
import wt.iba.constraint.DiscreteSetConstraint;
import wt.iba.constraint.SingleDefinitionConstraint;
import wt.iba.definition.AbstractAttributeDefinition;
import wt.iba.definition.ReferenceDefinition;
import wt.iba.definition.litedefinition.AttributeDefDefaultView;
import wt.iba.definition.litedefinition.ReferenceDefView;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.IBAHolder;
import wt.iba.value.IBAReferenceableReference;
import wt.iba.value.ReferenceValue;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.service.IBAValueHelper;
import wt.units.service.MeasurementSystemDefaultView;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;

public class ClassificationUtils {
    private static WTProperties wtp;
    private static boolean VERBOSE;
    public static final String DEFAULT_PATH_SEP = "/";

    static {
        try {
            wtp = WTProperties.getLocalProperties();
            VERBOSE = wtp.getProperty("ext.com.adv.windchill.cl.verbose", false);
        } catch (Exception var1) {
            var1.printStackTrace();
        }

    }

    public ClassificationUtils() {
    }

    public static ArrayList<ClassificationNode> getClassificationNodes(IBAHolder holder, Locale locale) {
        AttributeDefDefaultView attributedefdefaultviewReference = null;
        ReferenceDefinition referencedefinition = null;
        ReferenceValue referencevalue = null;
        ArrayList<ClassificationNode> clnodes = new ArrayList();
        ReferenceFactory reffact = new ReferenceFactory();

        try {
            IBAHolder ibaholder = IBAValueHelper.service.refreshAttributeContainer(holder, (Object)null, locale, (MeasurementSystemDefaultView)null);
            DefaultAttributeContainer defaultattributecontainer = (DefaultAttributeContainer)ibaholder.getAttributeContainer();
            AttributeDefDefaultView[] aattributedefdefaultview = defaultattributecontainer.getAttributeDefinitions();

            for(int i = 0; i < aattributedefdefaultview.length; ++i) {
                AttributeDefDefaultView attributedefdefaultview = aattributedefdefaultview[i];
                if (attributedefdefaultview.getAttributeDefinitionClassName().equals("wt.iba.definition.ReferenceDefinition")) {
                    attributedefdefaultviewReference = attributedefdefaultview;
                    break;
                }
            }

            if (attributedefdefaultviewReference != null) {
                if (VERBOSE) {
                    System.out.println("ClassificationUtils - getClassificationNode() - IBAReferenceName= " + attributedefdefaultviewReference.getName());
                    System.out.println("ClassificationUtils - getClassificationNode() - ObjectID= " + attributedefdefaultviewReference.getObjectID());
                }

                Persistable pers = reffact.getReference(attributedefdefaultviewReference.getObjectID().getStringValue()).getObject();
                if (pers instanceof ReferenceDefinition) {
                    referencedefinition = (ReferenceDefinition)pers;
                } else {
                    System.out.println("ClassificationUtils - getClassificationNode() - WARNING - ReferenceDefinition not found: " + attributedefdefaultviewReference.getName());
                }
            } else {
                System.out.println("ClassificationUtils - getClassificationNode() - WARNING - AttributeDefDefaultViewReference is null!!!");
            }

            if (referencedefinition != null) {
                AbstractValueView[] aavv = defaultattributecontainer.getAttributeValues(attributedefdefaultviewReference);

                for(int i = 0; i < aavv.length; ++i) {
                    AbstractValueView avv = aavv[i];
                    Persistable pers = reffact.getReference(avv.getObjectID().getStringValue()).getObject();
                    if (pers instanceof ReferenceValue) {
                        referencevalue = (ReferenceValue)pers;
                        Object referenceobj = referencevalue.getValueObject();
                        if (referenceobj instanceof IBAReferenceableReference && ((IBAReferenceableReference)referenceobj).getObject() instanceof ClassificationNode) {
                            ClassificationNode clnode = (ClassificationNode)((IBAReferenceableReference)referenceobj).getObject();
                            if (VERBOSE) {
                                System.out.println("ClassificationUtils - getClassificationNode() - ClassificationNode= " + clnode.getName());
                            }

                            clnodes.add(clnode);
                        } else {
                            System.out.println("ClassificationUtils - getClassificationNode() - WARNING - ReferenceObj in not a IBAReferenceableReference or Object is not a ClassificationNode!!!");
                        }
                    } else {
                        System.out.println("ClassificationUtils - getClassificationNode() - WARNING - ReferenceDefinition has not a ReferenceValue!!!");
                    }
                }
            } else {
                System.out.println("ClassificationUtils - getClassificationNode() - WARNING - ReferenceDefinition is null!!!");
            }
        } catch (Exception var16) {
            var16.printStackTrace();
        }

        return clnodes;
    }

    public static ClassificationNodeNodeView getClassificationNode(String className, String fullPath) throws WTException, RemoteException {
        StringTokenizer tokens = new StringTokenizer(fullPath, "/");
        if (tokens.hasMoreTokens()) {
            fullPath = tokens.nextToken();
        }

        ClassificationStructDefaultView cStructView = ClassificationHelper.service.getClassificationStructDefaultView(className);
        ClassificationNodeNodeView[] nodeView = ClassificationHelper.service.getClassificationStructureRootNodes(cStructView);
        ClassificationNodeNodeView requestedNode = null;

        for(int i = 0; i < nodeView.length; ++i) {
            if (VERBOSE) {
                System.out.println("ClassificationUtils - getClassificationNode() - Root Node: " + nodeView[i].getName());
            }

            if (nodeView[i].getName().equals(fullPath)) {
                if (tokens.hasMoreTokens()) {
                    requestedNode = getClassificationChildNode(nodeView[i], tokens);
                } else {
                    requestedNode = nodeView[i];
                }
                break;
            }
        }

        return requestedNode;
    }

    public static ClassificationNodeNodeView getClassificationChildNode(ClassificationNodeNodeView nodeView, StringTokenizer tokens) throws WTException, RemoteException {
        ClassificationNodeNodeView node = null;
        if (tokens.hasMoreTokens()) {
            String sPath = tokens.nextToken();
            if (VERBOSE) {
                System.out.println("ClassificationUtils - getClassificationChildNode() - Next node: " + sPath);
            }

            ClassificationNodeNodeView[] nodeViewSet = ClassificationHelper.service.getClassificationNodeChildren(nodeView);

            for(int j = 0; j < nodeViewSet.length; ++j) {
                if (nodeViewSet[j].getName().equals(sPath)) {
                    node = getClassificationChildNode(nodeViewSet[j], tokens);
                    if (node == null) {
                        node = nodeViewSet[j];
                    }
                }
            }
        }

        return node;
    }

    public static HashMap<ClassificationNode, ArrayList<ClassificationNode>> getClassificationNodeTree(IBAHolder holder, Locale locale) {
        HashMap<ClassificationNode, ArrayList<ClassificationNode>> nodesTree = new HashMap();
        ArrayList<ClassificationNode> clNodes = getClassificationNodes(holder, locale);

        for(int i = 0; i < clNodes.size(); ++i) {
            ClassificationNode clNode = (ClassificationNode)clNodes.get(i);
            ArrayList<ClassificationNode> tree = new ArrayList();
            getClassificationParentTree(clNode, tree);
            tree.add(clNode);
            nodesTree.put(clNode, tree);
        }

        return nodesTree;
    }

    public static void getClassificationParentTree(ClassificationNode node, ArrayList<ClassificationNode> tree) {
        ClassificationNode parent = node.getParent();
        if (parent != null) {
            getClassificationParentTree(parent, tree);
            tree.add(0, parent);
        }

    }

    public static ArrayList<AbstractAttributeDefinition> getClassificationAttributes(String className) {
        ArrayList attrs = new ArrayList();

        try {
            ClassificationStructDefaultView cStructView = ClassificationHelper.service.getClassificationStructDefaultView(className);
            ReferenceDefView referencedefview = cStructView.getReferenceDefView();
            if (VERBOSE) {
                System.out.println("ClassificationUtils - getAllClassificationAttributes() - ClassificationStructDefaultView=" + referencedefview.getName());
            }

            getClStructAbstractAttributeDefinition(cStructView, attrs);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return attrs;
    }

    public static void getClStructAbstractAttributeDefinition(ClassificationStructDefaultView csdv, ArrayList<AbstractAttributeDefinition> attrs) throws WTException, RemoteException {
        ClassificationNodeNodeView[] rootnodes = ClassificationHelper.service.getClassificationStructureRootNodes(csdv);

        for(int j = 0; j < rootnodes.length; ++j) {
            ClassificationNodeNodeView clnnview = rootnodes[j];
            if (VERBOSE) {
                System.out.println("ClassificationUtils - getClStructAbstractAttributeDefinition() - " + clnnview.getName());
            }

            getTreeAbstractAttributeDefinition(clnnview, attrs);
        }

    }

    public static void getTreeAbstractAttributeDefinition(ClassificationNodeNodeView cnnv, ArrayList<AbstractAttributeDefinition> attrs) throws WTException, RemoteException {
        ClassificationNodeDefaultView clnodedefview = null;
        ClassificationNodeNodeView[] clnnviews = ClassificationHelper.service.getClassificationNodeChildren(cnnv);
        if (VERBOSE) {
            System.out.println("ClassificationUtils - getTreeAbstractAttributeDefinition() - Children=" + clnnviews.length);
        }

        for(int j = 0; j < clnnviews.length; ++j) {
            ClassificationNodeNodeView clnnview = clnnviews[j];
            if (VERBOSE) {
                System.out.println("ClassificationUtils - getTreeAbstractAttributeDefinition() - ClassificationNodeNodeView=" + clnnview.getName());
            }

            getTreeAbstractAttributeDefinition(clnnview, attrs);
            ClassificationNode clnode = (ClassificationNode)(new ReferenceFactory()).getReference(clnnview.getObjectID().getStringValue()).getObject();
            ArrayList<AbstractAttributeDefinition> temp = IBAUtils.getAbstractAttributeDefinitions(clnode, Locale.getDefault());

            for(int i = 0; i < temp.size(); ++i) {
                if (!attrs.contains(temp.get(i))) {
                    attrs.add((AbstractAttributeDefinition)temp.get(i));
                }
            }
        }

        clnodedefview = ClassificationHelper.service.getClassificationNodeDefaultView(cnnv);
        ClassificationNode clnode = (ClassificationNode)(new ReferenceFactory()).getReference(clnodedefview.getObjectID().getStringValue()).getObject();
        ArrayList<AbstractAttributeDefinition> temp = IBAUtils.getAbstractAttributeDefinitions(clnode, Locale.getDefault());

        for(int i = 0; i < temp.size(); ++i) {
            if (!attrs.contains(temp.get(i))) {
                attrs.add((AbstractAttributeDefinition)temp.get(i));
            }
        }

    }

    public static DiscreteSetConstraint findClassificationConstraints(ClassificationNode node, AbstractAttributeDefinition ibaDefinition) throws WTException, RemoteException {
        String ibaname = ibaDefinition.getName();
        DiscreteSetConstraint constraints = null;
        ClassificationNodeNodeView nodeNodeView = (ClassificationNodeNodeView)NavigationHelper.service.getNavigationNodeFromID(node.toString());
        ClassificationNodeDefaultView nodeDefView = ClassificationHelper.service.getClassificationNodeDefaultView(nodeNodeView);
        AbstractCSMAttributeConstraintView[] csmAttributeConstraintViews = nodeDefView.getCSMAttributeConstraints();
        if (VERBOSE) {
            System.out.println("findClassificationConstraints - CSMConstraints Objects: " + csmAttributeConstraintViews.length);
        }

        for(int j = 0; j < csmAttributeConstraintViews.length; ++j) {
            AbstractCSMAttributeConstraintView csmAttributeConstraintView = csmAttributeConstraintViews[j];
            Persistable pers = (new ReferenceFactory()).getReference(csmAttributeConstraintView.getObjectID().getStringValue()).getObject();
            if (pers instanceof SingleDefinitionConstraint) {
                SingleDefinitionConstraint singleDefConst = (SingleDefinitionConstraint)pers;
                AbstractAttributeDefinition aad = (AbstractAttributeDefinition)singleDefConst.getAttributeDefReference().getObject();
                if (VERBOSE) {
                    System.out.println("findClassificationConstraints - Constraints on AbstractAttributeDefinition: " + aad.getName());
                }

                if (aad.equals(ibaDefinition)) {
                    if (csmAttributeConstraintView.getValueConstraint() instanceof DiscreteSetConstraint) {
                        constraints = (DiscreteSetConstraint)csmAttributeConstraintView.getValueConstraint();
                    } else if (VERBOSE) {
                        System.out.println("findClassificationConstraints - CSMAttributeConstraintView is not a DiscreteSetConstraint for IBA: " + ibaname);
                    }
                    break;
                }
            } else if (VERBOSE) {
                System.out.println("findClassificationConstraints - Persistable is not a CSMSingleDefConstraint for IBA: " + ibaname);
            }
        }

        return constraints;
    }

    public static LinkedHashMap<ClassificationNode, String> getClassificationStructMap(String className) throws WTException, RemoteException, WTPropertyVetoException {
        LinkedHashMap<ClassificationNode, String> structMap = new LinkedHashMap();
        ClassificationStructDefaultView cStructView = ClassificationHelper.service.getClassificationStructDefaultView(className);
        ClassificationNodeNodeView[] rootnodes = ClassificationHelper.service.getClassificationStructureRootNodes(cStructView);

        for(int j = 0; j < rootnodes.length; ++j) {
            ClassificationNodeNodeView clnnview = rootnodes[j];
            getClassificationStructMap(clnnview, structMap, clnnview.getName());
        }

        return structMap;
    }

    public static void getClassificationStructMap(ClassificationNodeNodeView cnnv, LinkedHashMap<ClassificationNode, String> map, String clPath) throws WTException, RemoteException, WTPropertyVetoException {
        ClassificationNodeDefaultView clnodedefview = null;
        ClassificationNodeNodeView[] clnnviews = ClassificationHelper.service.getClassificationNodeChildren(cnnv);
        clnodedefview = ClassificationHelper.service.getClassificationNodeDefaultView(cnnv);
        ClassificationNode clnode = (ClassificationNode)(new ReferenceFactory()).getReference(clnodedefview.getObjectID().getStringValue()).getObject();
        map.put(clnode, clPath);

        for(int i = 0; i < clnnviews.length; ++i) {
            ClassificationNodeNodeView clnnview = clnnviews[i];
            String tempPath = clPath + "/" + clnnview.getName();
            clnode = (ClassificationNode)(new ReferenceFactory()).getReference(clnnview.getObjectID().getStringValue()).getObject();
            map.put(clnode, tempPath);
            getClassificationStructMap(clnnview, map, tempPath);
        }

    }
}
