package ext.custom.tools;

import com.ptc.core.foundation.container.common.FdnWTContainerHelper;
import com.ptc.core.foundation.type.server.impl.SoftAttributesHelper;
import com.ptc.core.meta.common.AttributeIdentifier;
import com.ptc.core.meta.common.AttributeTypeIdentifier;
import com.ptc.core.meta.common.AttributeTypeIdentifierSet;
import com.ptc.core.meta.common.DataSet;
import com.ptc.core.meta.common.DiscreteSet;
import com.ptc.core.meta.common.FloatingPoint;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.TypeInstanceIdentifier;
import com.ptc.core.meta.common.impl.InstanceBasedAttributeTypeIdentifier;
import com.ptc.core.meta.container.common.AttributeTypeSummary;
import com.ptc.core.meta.server.IBAModel;
import com.ptc.core.meta.type.common.TypeInstance;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.iba.definition.AbstractAttributeDefinition;
import wt.iba.definition.AttributeDefinitionReference;
import wt.iba.definition.AttributeOrganizer;
import wt.iba.definition.BooleanDefinition;
import wt.iba.definition.FloatDefinition;
import wt.iba.definition.IntegerDefinition;
import wt.iba.definition.StringDefinition;
import wt.iba.definition.URLDefinition;
import wt.iba.definition.litedefinition.AttributeDefDefaultView;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.IBAHolder;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.litevalue.BooleanValueDefaultView;
import wt.iba.value.litevalue.FloatValueDefaultView;
import wt.iba.value.litevalue.IntegerValueDefaultView;
import wt.iba.value.litevalue.StringValueDefaultView;
import wt.iba.value.litevalue.TimestampValueDefaultView;
import wt.iba.value.litevalue.URLValueDefaultView;
import wt.iba.value.litevalue.UnitValueDefaultView;
import wt.iba.value.service.IBAValueHelper;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.type.TypedUtilityServiceHelper;
import wt.units.service.MeasurementSystemDefaultView;
import wt.util.WTException;

public class IBAUtils {
    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm";
    public static final String PDMLINK_DATE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final int HOURS_IN_A_DAY = 24;

    public IBAUtils() {
    }

    public static AbstractAttributeDefinition findIBADefinition(String name) throws WTException {
        QuerySpec qs = new QuerySpec(AbstractAttributeDefinition.class);
        int ibas = qs.appendClassList(AbstractAttributeDefinition.class, true);
        int[] a = new int[]{ibas, -1};
        SearchCondition s1 = new SearchCondition(AbstractAttributeDefinition.class, "name", "=", name);
        qs.appendWhere(s1, a);
        QueryResult qr = PersistenceHelper.manager.find(qs);
        if (qr.hasMoreElements()) {
            AbstractAttributeDefinition thisAbstractAttributeDefinition = (AbstractAttributeDefinition)((Persistable[])qr.nextElement())[ibas];
            return thisAbstractAttributeDefinition;
        } else {
            return null;
        }
    }

    public static String getAttribute(IBAHolder holder, String attributeName) {
        Locale locale = Locale.getDefault();
        AbstractValueView[] aabstractvalueview = (AbstractValueView[])null;
        String attributeValue = null;

        try {
            IBAHolder ibaholder = IBAValueHelper.service.refreshAttributeContainer(holder, (Object)null, locale, (MeasurementSystemDefaultView)null);
            DefaultAttributeContainer defaultattributecontainer = (DefaultAttributeContainer)ibaholder.getAttributeContainer();
            if (defaultattributecontainer != null) {
                aabstractvalueview = defaultattributecontainer.getAttributeValues();

                for(int j = 0; j < aabstractvalueview.length; ++j) {
                    if (aabstractvalueview[j].getDefinition().getName().equals(attributeName)) {
                        if (aabstractvalueview[j] instanceof URLValueDefaultView) {
                            URLValueDefaultView urlvaluedefaultview = (URLValueDefaultView)aabstractvalueview[j];
                            attributeValue = urlvaluedefaultview.getValue();
                            break;
                        }

                        if (aabstractvalueview[j] instanceof UnitValueDefaultView) {
                            UnitValueDefaultView unitvaluedefaultview = (UnitValueDefaultView)aabstractvalueview[j];
                            attributeValue = Double.toString(unitvaluedefaultview.getValue());
                            break;
                        }

                        if (aabstractvalueview[j] instanceof BooleanValueDefaultView) {
                            BooleanValueDefaultView booleanvaluedefaultview = (BooleanValueDefaultView)aabstractvalueview[j];
                            attributeValue = booleanvaluedefaultview.getValueAsString();
                            break;
                        }

                        if (aabstractvalueview[j] instanceof IntegerValueDefaultView) {
                            IntegerValueDefaultView integervaluedefaultview = (IntegerValueDefaultView)aabstractvalueview[j];
                            attributeValue = Long.toString(integervaluedefaultview.getValue());
                            break;
                        }

                        if (aabstractvalueview[j] instanceof FloatValueDefaultView) {
                            FloatValueDefaultView floatvaluedefaultview = (FloatValueDefaultView)aabstractvalueview[j];
                            attributeValue = Double.toString(floatvaluedefaultview.getValue());
                            break;
                        }

                        if (aabstractvalueview[j] instanceof StringValueDefaultView) {
                            StringValueDefaultView stringvaluedefaultview = (StringValueDefaultView)aabstractvalueview[j];
                            attributeValue = stringvaluedefaultview.getValue();
                            break;
                        }

                        if (aabstractvalueview[j] instanceof TimestampValueDefaultView) {
                            TimestampValueDefaultView time = (TimestampValueDefaultView)aabstractvalueview[j];
                            attributeValue = time.getLocalizedDisplayString(Locale.ITALIAN);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                            try {
                                String valueAsString = time.getValueAsString();
                                Date d = sdf.parse(valueAsString);
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(d);
                                int day = cal.get(5);
                                int hour = cal.get(11);
                                int minute = cal.get(12);
                                int second = cal.get(13);
                                int millis = cal.get(14);
                                if (hour != 0 && minute == 0 && second == 0 && millis == 0) {
                                    int hoursToMidnight = 24 - hour;
                                    if (hoursToMidnight < 12) {
                                        cal.add(10, hoursToMidnight);
                                    } else {
                                        cal.add(10, -hour);
                                    }

                                    day = cal.get(5);
                                    hour = cal.get(11);
                                    minute = cal.get(12);
                                    second = cal.get(13);
                                    millis = cal.get(14);
                                }

                                SimpleDateFormat defaultSDF = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                attributeValue = defaultSDF.format(cal.getTime());
                            } catch (ParseException var19) {
                                System.out.println("Parsing " + attributeValue + " with format " + "dd/MM/yyyy HH:mm");
                                var19.printStackTrace();
                                attributeValue = time.getLocalizedDisplayString(Locale.ITALIAN);
                            }

                            System.out.println("spdf.format[dd/MM/yyyy HH:mm] =" + attributeValue);
                            break;
                        }
                    }
                }
            }
        } catch (WTException var20) {
            System.out.println("WTException " + var20.toString());
        } catch (RemoteException var21) {
            System.out.println("RMIException " + var21.toString());
        }

        return attributeValue;
    }

    public static Vector<AbstractAttributeDefinition> findAllIBAsDefinition() throws WTException {
        Vector<AbstractAttributeDefinition> allIBAs = new Vector();
        QuerySpec qs = new QuerySpec();
        int ibas = qs.appendClassList(AbstractAttributeDefinition.class, true);
        int[] a = new int[]{ibas, -1};
        SearchCondition s1 = new SearchCondition(AbstractAttributeDefinition.class, "name", "LIKE", "%");
        qs.appendWhere(s1, a);
        QueryResult qr = PersistenceHelper.manager.find(qs);

        while(qr.hasMoreElements()) {
            AbstractAttributeDefinition thisAbstractAttributeDefinition = (AbstractAttributeDefinition)((Persistable[])qr.nextElement())[ibas];
            allIBAs.add(thisAbstractAttributeDefinition);
        }

        return allIBAs;
    }

    public static Vector<String> getSoftTypeIBAs(String typename) throws WTException {
        Vector<String> a = new Vector();
        TypeIdentifier typeid = FdnWTContainerHelper.toTypeIdentifier(typename);
        TypeInstanceIdentifier typeinstanceid = typeid.newTypeInstanceIdentifier();
        TypeInstance typeinst = SoftAttributesHelper.getSoftSchemaTypeInstance(typeinstanceid, (ArrayList)null, Locale.getDefault());
        AttributeIdentifier[] aattributeid = typeinst.getAttributeIdentifiers();

        for(int j = 0; j < aattributeid.length; ++j) {
            AttributeIdentifier ai = aattributeid[j];
            AttributeTypeIdentifier ati = (AttributeTypeIdentifier)ai.getDefinitionIdentifier();
            a.add(ati.getAttributeName());
        }

        return a;
    }

    public static boolean isSoftTypeIBADefined(String typename, String ibaname) throws WTException {
        boolean f = false;
        Vector<String> a = getSoftTypeIBAs(typename);

        for(int i = 0; i < a.size(); ++i) {
            String iba = (String)a.get(i);
            if (iba.equals(ibaname)) {
                f = true;
                break;
            }
        }

        return f;
    }

    public static boolean isSoftTypeIBADefined(IBAHolder ibaholder, String ibaname) throws WTException, RemoteException {
        boolean f = false;
        String softtypedef = TypedUtilityServiceHelper.service.getExternalTypeIdentifier(ibaholder);
        Vector<String> a = getSoftTypeIBAs(softtypedef);

        for(int i = 0; i < a.size(); ++i) {
            String iba = (String)a.get(i);
            if (iba.equals(ibaname)) {
                f = true;
                break;
            }
        }

        return f;
    }

    public static Vector<String> getSoftTypeIBAs(IBAHolder ibaholder) throws WTException, RemoteException {
        String softtypedef = TypedUtilityServiceHelper.service.getExternalTypeIdentifier(ibaholder);
        Vector<String> a = getSoftTypeIBAs(softtypedef);
        return a;
    }

    public static String getAttributeOrganizer(String ibaName) throws WTException {
        AbstractAttributeDefinition aad = findIBADefinition(ibaName);
        if (aad != null) {
            AttributeOrganizer ao = (AttributeOrganizer)aad.getDefinitionParentReference().getObject();
            return ao.getName();
        } else {
            throw new WTException("IBAUtils - IBANotFound " + ibaName);
        }
    }

    public static String getCompleteAttributeOrganizer(String ibaName) throws WTException {
        String orgPath = "";
        AbstractAttributeDefinition aad = findIBADefinition(ibaName);
        if (aad != null) {
            for(AttributeOrganizer ao = (AttributeOrganizer)aad.getDefinitionParentReference().getObject(); ao != null; ao = (AttributeOrganizer)ao.getOrganizerParentReference().getObject()) {
                if (orgPath.equals("")) {
                    orgPath = ao.getName();
                } else {
                    orgPath = ao.getName() + "/" + orgPath;
                }
            }

            return orgPath;
        } else {
            throw new WTException("IBAUtils - IBANotFound " + ibaName);
        }
    }

    public static String getIBADefaultValue(String ibaname) throws WTException {
        String defValue = "";
        AbstractAttributeDefinition aad = findIBADefinition(ibaname);
        if (aad != null) {
            AttributeDefinitionReference adr = aad.getAttributeDefinitionReference();
            Persistable pers = adr.getObject();
            System.out.println("IBAUtils - getIBADefaultValue: " + ibaname + " DefinitionType=" + pers.getConceptualClassname());
            if (pers instanceof StringDefinition) {
                defValue = "-";
            } else if (pers instanceof BooleanDefinition) {
                defValue = "false";
            } else if (pers instanceof FloatDefinition) {
                defValue = "0.0";
            } else if (pers instanceof IntegerDefinition) {
                defValue = "0";
            } else {
                if (!(pers instanceof URLDefinition)) {
                    throw new WTException("IBAUtils - getIBADefaultValue: IBA DefinitionType not found!");
                }

                defValue = "http://";
            }

            return defValue;
        } else {
            throw new WTException("IBAUtils - getIBADefaultValue: IBA " + ibaname + " non defined on PDMLink");
        }
    }

    public static Vector<String> getIBAConstraints(String fullTypeName, String IBAName) throws WTException {
        Vector<String> constraints = new Vector();
        AttributeTypeIdentifier selectedAti = null;
        TypeIdentifier typeid = FdnWTContainerHelper.toTypeIdentifier(fullTypeName);
        TypeInstanceIdentifier typeinstanceid = typeid.newTypeInstanceIdentifier();
        TypeInstance typeinst = SoftAttributesHelper.getSoftSchemaTypeInstance(typeinstanceid, (ArrayList)null, Locale.getDefault());
        AttributeIdentifier[] aattributeid = typeinst.getAttributeIdentifiers();

        for(int j = 0; j < aattributeid.length; ++j) {
            AttributeIdentifier ai = aattributeid[j];
            AttributeTypeIdentifier ati = (AttributeTypeIdentifier)ai.getDefinitionIdentifier();
            if (ati.getAttributeName().equals(IBAName)) {
                selectedAti = ati;
                break;
            }
        }

        if (selectedAti == null) {
            throw new WTException("IBAUtils - IBANotDefined " + IBAName + " on type " + typeid);
        } else {
            AttributeTypeSummary attributetypesummary = typeinst.getAttributeTypeSummary(selectedAti);
            DataSet set = attributetypesummary.getLegalValueSet();
            if (set != null && set instanceof DiscreteSet) {
                DiscreteSet constraintSet = (DiscreteSet)set;
                Object[] constraintsList = constraintSet.getElements();
                String dataType = attributetypesummary.getDataType();
                int i;
                String theValue;
                if (dataType.equals(String.class.getCanonicalName())) {
                    for(i = 0; i < constraintsList.length; ++i) {
                        theValue = (String)constraintsList[i];
                        constraints.add(theValue);
                    }
                } else if (dataType.equals(Long.class.getCanonicalName())) {
                    for(i = 0; i < constraintsList.length; ++i) {
                        theValue = ((Long)constraintsList[i]).toString();
                        constraints.add(theValue);
                    }
                } else if (dataType.equals(FloatingPoint.class.getCanonicalName())) {
                    for(i = 0; i < constraintsList.length; ++i) {
                        theValue = String.valueOf(((FloatingPoint)constraintsList[i]).getValue());
                        constraints.add(theValue);
                    }
                } else if (dataType.equals(Timestamp.class.getCanonicalName())) {
                    for(i = 0; i < constraintsList.length; ++i) {
                        theValue = ((Timestamp)constraintsList[i]).toString();
                        constraints.add(theValue);
                    }
                }

                Collections.sort(constraints);
            }

            return constraints;
        }
    }

    public static ArrayList<AbstractAttributeDefinition> getAbstractAttributeDefinitions(IBAHolder holder, Locale locale) {
        ArrayList attrs = new ArrayList();

        try {
            IBAHolder clholder = IBAValueHelper.service.refreshAttributeContainer(holder, (Object)null, locale, (MeasurementSystemDefaultView)null);
            DefaultAttributeContainer cldefaultattributecontainer = (DefaultAttributeContainer)clholder.getAttributeContainer();
            if (cldefaultattributecontainer != null) {
                AttributeDefDefaultView[] attributedefdefaultviews = cldefaultattributecontainer.getAttributeDefinitions();

                for(int k = 0; k < attributedefdefaultviews.length; ++k) {
                    AttributeDefDefaultView clattributedefdefaultview = attributedefdefaultviews[k];
                    AbstractAttributeDefinition ad = (AbstractAttributeDefinition)(new ReferenceFactory()).getReference(clattributedefdefaultview.getObjectID().getStringValue()).getObject();
                    attrs.add(ad);
                }
            }
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        return attrs;
    }

    public static boolean isIBAInAttributeContainer(IBAHolder holder, String ibaname, Locale locale) {
        ArrayList<AbstractAttributeDefinition> attributes = getAbstractAttributeDefinitions(holder, locale);

        for(int i = 0; i < attributes.size(); ++i) {
            AbstractAttributeDefinition addv = (AbstractAttributeDefinition)attributes.get(i);
            if (addv.getName().equals(ibaname)) {
                return true;
            }
        }

        return false;
    }

    public static ArrayList<AbstractAttributeDefinition> getAbstractAttributeDefinitions(String s) throws WTException {
        ArrayList<AbstractAttributeDefinition> attributes = new ArrayList();
        String softtype = s;
        if (!s.startsWith("WCTYPE")) {
            softtype = "WCTYPE|" + s;
        }

        AttributeTypeIdentifierSet atis = SoftAttributesHelper.getSoftSchemaAttributesSet(softtype);
        Object[] a = atis.toArray();

        for(int i = 0; i < a.length; ++i) {
            InstanceBasedAttributeTypeIdentifier ibati = (InstanceBasedAttributeTypeIdentifier)a[i];
            AbstractAttributeDefinition aad = IBAModel.getIBADefinitionByHid(ibati.getAttributeName());
            if (aad != null) {
                attributes.add(aad);
            } else {
                System.out.println("IBAUtils - getSoftTypeAbstractAttributeDefinition() - IBA not defined=" + ibati.getAttributeName());
            }
        }

        return attributes;
    }

    public static ArrayList<String> areSoftTypeIBADefined(String softtype, ArrayList<String> list) throws WTException {
        ArrayList<String> notDefined = new ArrayList();
        ArrayList<AbstractAttributeDefinition> softtypeIBAs = getAbstractAttributeDefinitions(softtype);

        for(int i = 0; i < list.size(); ++i) {
            boolean found = false;
            String ibaName = (String)list.get(i);

            for(int j = 0; j < softtypeIBAs.size(); ++j) {
                AbstractAttributeDefinition aad = (AbstractAttributeDefinition)softtypeIBAs.get(j);
                if (aad.getName().equals(ibaName)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                notDefined.add(ibaName);
            }
        }

        return notDefined;
    }

    public static ArrayList<String> areIBAInAttributeContainer(IBAHolder holder, ArrayList<String> list, Locale locale) throws WTException {
        ArrayList<String> notDefined = new ArrayList();
        ArrayList<AbstractAttributeDefinition> holderIBAs = getAbstractAttributeDefinitions(holder, locale);

        for(int i = 0; i < list.size(); ++i) {
            boolean found = false;
            String ibaName = (String)list.get(i);

            for(int j = 0; j < holderIBAs.size(); ++j) {
                AbstractAttributeDefinition aad = (AbstractAttributeDefinition)holderIBAs.get(j);
                if (aad.getName().equals(ibaName)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                notDefined.add(ibaName);
            }
        }

        return notDefined;
    }
}
