package ext.custom.titan.windchill.whereUsed;

import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.AbstractAttributeDataUtility;
import com.ptc.core.components.rendering.guicomponents.IconComponent;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.part.WTPart;
import wt.util.WTException;


public class SearchTLDataUtility extends AbstractAttributeDataUtility {
    public SearchTLDataUtility() {
    }

    public Object getDataValue(String var1, Object var2, ModelContext var3) throws WTException {
        return this.handleSearchData(var1, var2);
    }

    private IconComponent handleSearchData(String var1, Object var2) throws WTException {
        IconComponent var3 = null;
        if (var2 instanceof WTPart) {
            WTPart var4 = (WTPart) var2;
            var3 = this.getSearchDataPart(var4);
        } else if (var2 instanceof WTDocument ) {
            var3 = this.getSearchDataWTDocument( (WTDocument) var2);
        } else if (var2 instanceof EPMDocument ) {
            var3 = this.getSearchDataEPMDocument((EPMDocument) var2);
        } else {
            var3 = new IconComponent("Not a valid element: " + var2.getClass().toString());
        }

        return var3;
    }

    public IconComponent getSearchDataPart(WTPart var1) throws WTException {
        return getTrafficLight(var1.getState().toString());
    }

    public IconComponent getSearchDataWTDocument(WTDocument var1) throws WTException {
        String elementState =    var1.getState().toString();
        return getTrafficLight(elementState);
    }
    public IconComponent getSearchDataEPMDocument(EPMDocument var1) throws WTException {

        String elementState =  var1.getLifeCycleState().toString();
        return getTrafficLight(elementState);
    }


    private IconComponent getTrafficLight(String state){
        IconComponent trafficLight = new IconComponent();
        if (state.equalsIgnoreCase("PROGETTAZIONE")) {
            trafficLight.setSrc("netmarkets/images/yellow.gif");
        } else if (state.equalsIgnoreCase("ATTESA")) {
            trafficLight.setSrc("netmarkets/images/yellow.gif");
        } else if (state.equalsIgnoreCase("RELEASED")) {
            trafficLight.setSrc("netmarkets/images/green.gif");
        } else if (state.equalsIgnoreCase("PRE_CANCELLED")) {
            trafficLight.setSrc("netmarkets/images/yellow.gif");
        } else if (state.equalsIgnoreCase("CANCELLED")) {
            trafficLight.setSrc("netmarkets/images/red.gif");
        }
        return trafficLight;
    }
}
