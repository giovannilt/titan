//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ext.custom.titan.windchill.whereUsed;

import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.AbstractAttributeDataUtility;
import com.ptc.core.components.rendering.guicomponents.IconComponent;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.impl.TypeIdentifierUtilityHelper;
import ext.custom.titan.windchill.TitanProperties;
import java.util.Locale;
import java.util.Vector;
import wt.content.ApplicationData;
import wt.content.ContentHelper;
import wt.content.ContentHolder;
import wt.content.ContentRoleType;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentType;
import wt.fc.QueryResult;
import wt.identity.IdentityFactory;
import wt.part.PartDocHelper;
import wt.part.WTPart;
import wt.part.WTPartHelper;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.vc.VersionControlHelper;

public class PartPdfDataUtility extends AbstractAttributeDataUtility {
    public PartPdfDataUtility() {
    }

    public Object getDataValue(String var1, Object var2, ModelContext var3) throws WTException {
        return this.handlePDFLLink(var1, var2);
    }

    private IconComponent handlePDFLLink(String var1, Object var2) throws WTException {
        IconComponent var3 = null;
        if (var2 instanceof WTPart) {
            WTPart var4 = (WTPart)var2;
            var3 = this.getPartPdfLink(var4);
        } else {
            var3 = new IconComponent("Not a WTPart: " + var2.getClass().toString());
        }

        return var3;
    }

    public IconComponent getPartPdfLink(WTPart var1) throws WTException {
        IconComponent var2 = new IconComponent("No PDF");
        var2.setSrc("netmarkets/images/PDFMissing.gif");

        String var3 = "";

        try {
            EPMDocument var5 = null;
            QueryResult var6 = PartDocHelper.service.getAssociatedDocuments(var1);

            while(var6.hasMoreElements()) {
                Object var7 = var6.nextElement();
                if (var7 instanceof EPMDocument) {
                    EPMDocument var8 = (EPMDocument)var7;
                    if (VersionControlHelper.isLatestIteration(var8) && var8.getDocType().equals(EPMDocumentType.toEPMDocumentType("CADDRAWING"))) {
                        System.out.println("getContentDRW - Object " + var7.getClass().getName() + " " + IdentityFactory.getDisplayIdentifier(var7));
                        var5 = var8;
                        break;
                    }
                }
            }

            if (var5 != null) {
                ApplicationData var15 = null;
                ContentHolder var17 = ContentHelper.service.getContents(var5);
                Vector var9 = ContentHelper.getContentListAll(var17);

                for(int var10 = 0; var10 < var9.size(); ++var10) {
                    Object var11 = var9.get(var10);
                    if (var11 instanceof ApplicationData) {
                        ApplicationData var12 = (ApplicationData)var11;
                        if (var12.getRole().equals(ContentRoleType.SECONDARY) && var12.getFileName().toLowerCase().endsWith(".pdf")) {
                            var15 = var12;
                            break;
                        }
                    }
                }

                if (var15 != null) {
                    String  url =  WTProperties.getServerProperties().getProperty("wt.server.codebase");
                    url = url + "/servlet/AttachmentsDownloadDirectionServlet?oid=OR:";
                    url = url + var5.getPersistInfo().getObjectIdentifier().getStringValue();
                    url = url + "&cioids=";
                    url = url + var15.getPersistInfo().getObjectIdentifier().getStringValue();

                    var2 = new IconComponent(var3);
                    var2.setSrc("netmarkets/images/file_pdf.gif");
                    var2.setUrl(url);
                }
            } else {
                WTDocument var16 = null;
                var6 = WTPartHelper.service.getDescribedByWTDocuments(var1);

                while(var6.hasMoreElements()) {
                    Object var18 = var6.nextElement();
                    System.out.println(var18.getClass().toString());
                    if (var18 instanceof WTDocument) {
                        WTDocument var20 = (WTDocument)var18;
                        TypeIdentifier var22 = TypeIdentifierUtilityHelper.service.getTypeIdentifier(var20);
                        if (var22.getTypename().equals(TitanProperties.getProperties().getMe10PDFSoftType())) {
                            var16 = var20;
                            break;
                        }
                    }
                }

                if (var16 != null) {
                    ApplicationData var19 = null;
                    ContentHolder var21 = ContentHelper.service.getContents(var16);
                    Vector var23 = ContentHelper.getContentListAll(var21);

                    for(int var24 = 0; var24 < var23.size(); ++var24) {
                        Object var25 = var23.get(var24);
                        if (var25 instanceof ApplicationData) {
                            ApplicationData var13 = (ApplicationData)var25;
                            if (var13.getRole().equals(ContentRoleType.PRIMARY) && var13.getFileName().toLowerCase().endsWith(".pdf")) {
                                var19 = var13;
                                break;
                            }
                        }
                    }

                    if (var19 != null) {
                        String  url =  WTProperties.getServerProperties().getProperty("wt.server.codebase");
                        url = url + "/servlet/AttachmentsDownloadDirectionServlet?oid=OR:";
                        url = url + var5.getPersistInfo().getObjectIdentifier().getStringValue();
                        url = url + "&cioids=";
                        url = url + var19.getPersistInfo().getObjectIdentifier().getStringValue();


                        var2 = new IconComponent(var3);
                        var2.setSrc("netmarkets/images/file_pdf.gif");
                        var2.setUrl(url);

                    }
                }
            }
        } catch (Exception var14) {
            //var2 = new IconComponent(var14.getClass().toString());
            var2.setComponentHidden(true);
        }

        return var2;
    }
}