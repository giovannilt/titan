package ext.custom.titan.windchill.whereUsed;

import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.AbstractAttributeDataUtility;
import com.ptc.core.components.rendering.guicomponents.IconComponent;
import com.ptc.core.components.rendering.guicomponents.Label;
import com.ptc.core.components.rendering.guicomponents.TextDisplayComponent;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.part.WTPart;
import wt.util.WTException;
import wt.vc.VersionControlHelper;


public class IsLatestDataUtility extends AbstractAttributeDataUtility {

    public Object getDataValue(String var1, Object var2, ModelContext var3) throws WTException {
        return this.handleSearchData(var2);
    }

    private Label handleSearchData(Object var2) throws WTException {
        Label var3 = null;

        if (var2 instanceof WTPart) {
            WTPart var4 = (WTPart) var2;
            var3 = this.getSearchDataPart(var4);
        } else if (var2 instanceof WTDocument ) {
            var3 = this.getSearchDataWTDocument( (WTDocument) var2);
        } else if (var2 instanceof EPMDocument ) {
            var3 = this.getSearchDataEPMDocument((EPMDocument) var2);
        } else {
            var3 = new Label("Not a valid element: " + var2.getClass().toString());
        }

        return var3;
    }

    public Label getSearchDataPart(WTPart part) throws WTException {
        WTPart latest = (WTPart) VersionControlHelper.service.allVersionsOf(part.getMaster()).nextElement();
        String version = VersionControlHelper.getVersionIdentifier(part).getValue();
        String versionLatest = VersionControlHelper.getVersionIdentifier(latest).getValue();
        return getLatestIcon(version.equals(versionLatest));
    }

    public Label getSearchDataWTDocument(WTDocument doc) throws WTException {
        WTDocument latest = (WTDocument) VersionControlHelper.service.allVersionsOf(doc.getMaster()).nextElement();
        String version = VersionControlHelper.getVersionIdentifier(doc).getValue();
        String versionLatest = VersionControlHelper.getVersionIdentifier(latest).getValue();
        return getLatestIcon(version.equals(versionLatest));
    }
    public Label getSearchDataEPMDocument(EPMDocument epmDoc) throws WTException {
        EPMDocument latest = (EPMDocument) VersionControlHelper.service.allVersionsOf(epmDoc.getMaster()).nextElement();
        String version = VersionControlHelper.getVersionIdentifier(epmDoc).getValue();
        String versionLatest = VersionControlHelper.getVersionIdentifier(latest).getValue();
        return getLatestIcon(version.equals(versionLatest));
    }


    private Label getLatestIcon(Boolean isLatest){
        Label label ;
        if (isLatest) {
            label = new Label("SI");
            label.setLabel("SI");
        } else {
            label = new Label("NO");
            label.setLabel("NO");
        }
        return label;
    }
}
