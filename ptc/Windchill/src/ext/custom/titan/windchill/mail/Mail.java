package ext.custom.titan.windchill.mail;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
    private Mail() {
    }

    public static void sendSimpleMail(String smtpServer, String smtpPort, String to, String from, String subject, String body) throws AddressException, MessagingException {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", smtpPort);
        Session session = Session.getDefaultInstance(props, (Authenticator)null);
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(from));
        msg.setRecipients(RecipientType.TO, InternetAddress.parse(to, false));
        msg.setSubject(subject);
        msg.setText(body);
        msg.setHeader("X-Mailer", "ADVTechEmail");
        msg.setSentDate(new Date());
        Transport.send(msg);
        System.out.println("Message sent OK.");
    }
}

