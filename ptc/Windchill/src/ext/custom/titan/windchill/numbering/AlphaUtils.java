package ext.custom.titan.windchill.numbering;

public class AlphaUtils {

    private static String indexToColumnRec(int index, char[] alphabet) {
        if (index <= 0) throw new IndexOutOfBoundsException("index must be a positive number");
        if (index <= alphabet.length) return Character.toString(alphabet[index - 1]);
        return indexToColumnRec(--index / alphabet.length, alphabet) + alphabet[index % alphabet.length];
    }

    public static String getNextIndex(String seq){
        char[] alphabet= "ABCEFGHKLMNPQRSTUVWXYZ".toCharArray();
        for(int i =1 ; i<(Math.pow(alphabet.length+2, seq.length())); i++){
            String s= indexToColumnRec(i, alphabet);
            if(s.equalsIgnoreCase(seq)){
                return indexToColumnRec(i+1, alphabet).toUpperCase();
            }
        }
        return "";
    }
}