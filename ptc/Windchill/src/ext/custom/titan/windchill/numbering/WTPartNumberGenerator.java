package ext.custom.titan.windchill.numbering;

import org.apache.log4j.Logger;
import wt.log4j.LogR;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class WTPartNumberGenerator {
    private static final int QUATTRO = 4;
    private static WTPartNumberGenerator wtPartNumberGenerator;
    private static String released;
    private static String provisional;
    private static final Logger log;
    private static Properties properties;

    private WTPartNumberGenerator() {}

    static {
        try {
            log = LogR.getLogger(WTPartNumberGenerator.class.getName());
            properties = new Properties();
        } catch (Exception var1) {
            throw new ExceptionInInitializerError(var1);
        }
    }

    public static WTPartNumberGenerator TitanWTPartNumberGenerator() throws IOException {
        if (wtPartNumberGenerator == null) {
            wtPartNumberGenerator = new WTPartNumberGenerator();
        }
        return wtPartNumberGenerator;
    }

    public synchronized String getNextReleasedWTPartNumber() throws IOException, URISyntaxException {
        properties.load(WTPartNumberGenerator.class.getClass().getResourceAsStream("/ext/custom/titan/windchill/numbering/wtNumber.properties"));
        released = properties.getProperty("wtPart.released");
        String[] split = released.split("\\.");
        if (split.length == 3) {
            released = split[0] + "." + getNext(split[1]) + "." + split[2];
        } else {
            throw new Error("ReleasedWtPartNumber formato errato nel file di properties deve esser esmpio XX.AA104.00");
        }
        properties.setProperty("wtPart.released", released);
        save();
        return released;
    }

    public synchronized String getNextProvisionalWTPartNumber() throws IOException, URISyntaxException {
        properties.load(WTPartNumberGenerator.class.getClass().getResourceAsStream("/ext/custom/titan/windchill/numbering/wtNumber.properties"));
        provisional = properties.getProperty("wtPart.provisional");
        String[] split = provisional.split("\\.");
        if (split.length == 3) {
            provisional = split[0] + "." + getNext(split[1]) + "." + split[2];
        } else {
            throw new Error("Provisional WtPartNumber formato errato nel file di properties deve esser esmpio 14.AA104.00");
        }
        properties.setProperty("wtPart.provisional", provisional);
        save();
        return provisional;
    }

    private void save() throws IOException, URISyntaxException {
        URL resourceUrl = WTPartNumberGenerator.class.getResource("wtNumber.properties");
        File file = new File(resourceUrl.toURI());
        OutputStream output = new FileOutputStream(file);
        properties.store(output, null);
    }

    private  String getNext(String code) {
        if (code.length() != 4) {
            throw new Error("Codice non conforme allo standard minimo una Lettera e 3 numerici");
        }
        String[] splitted = code.split("[A-Za-z]+");
        String numbers = splitted[1];
        splitted = code.split("[0-9]+");
        String letters = splitted[0];

        int number = Integer.parseInt(numbers);
        String oldNumber= numbers;
        number++;
        String newNumbers = number + "";

        if (oldNumber.length() < newNumbers.length()) {
            //Cambio Lettera
            numbers = "0";
            letters = AlphaUtils.getNextIndex(letters);
        } else {
            numbers = newNumbers;
        }

        return letters + getZeros(letters, numbers) + numbers;
    }

    private  String getZeros(String letters, String numbers) {
        int stringLength = QUATTRO - letters.length() - numbers.length();
        switch (stringLength) {
            case 0:
                return "";
            case 1:
                return "0";
            case 2:
                return "00";
        }
        return "";
    }

}
