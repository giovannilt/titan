package ext.custom.titan.windchill;

import wt.util.resource.*;


@RBUUID("ext.custom.titan.windchill.TitianRB")
public class TitianRB extends WTListResourceBundle {

    @RBEntry("PDF - DXF")
    @RBComment("The Content tab on the info page for WTPart")
    public static final String PDF_DXF_Table_NAME = "part.PDF_DXF_Table.description";

    @RBEntry("PDF - DXF")
    @RBComment("The Content tab on the info page for WTPart")
    public static final String PDF_DXF_Table_TITLE = "part.PDF_DXF_Table.title";

    @RBEntry("PDF - DXF")
    @RBComment("The Content tab on the info page for WTPart")
    public static final String PDF_DXF_Table_NAME_TooTip = "part.PDF_DXF_Table.tooltip";

    @RBEntry("Sent To Print Attached PDF")
    public static final String PRIVATE_CONSTANT_205 = "part.sentToPrintSelectedPDFs.title";

    @RBEntry("print_bulk.png")
    @RBPseudo(false)
    public static final String PRIVATE_CONSTANT_206 = "part.sentToPrintSelectedPDFs.icon";

    @RBEntry("Sent To Print Attached PDF")
    public static final String PRIVATE_CONSTANT_207 = "part.sentToPrintSelectedPDFs.description";

    @RBEntry("Sent To Print Attached PDF")
    public static final String PRIVATE_CONSTANT_208 = "part.sentToPrintSelectedPDFs.tooltip";


    @RBEntry("Major Tab: Titan")
    @RBComment("Used for the tooltip on the product tab")
    public static final String PRIVATE_CONSTANT_44_TITAN = "navigation.titanCustomAction.tooltip";

    @RBEntry("Active Major Tab: Titan")
    public static final String PRIVATE_CONSTANT_45_TITAN = "navigation.titanCustomAction.description";

    @RBEntry("navigator_titan.png")
    @RBComment("DO NOT TRANSLATE")
    public static final String PRIVATE_CONSTANT_46_TITAN = "navigation.titanCustomAction.icon";

    @RBEntry("print_bulk.png")
    @RBPseudo(false)
    public static final String PRIVATE_CONSTANT_210 = "object.printMax30PDFbyNumber.icon";

    @RBEntry("Sent To Print Attached PDF")
    public static final String PRIVATE_CONSTANT_211 = "object.printMax30PDFbyNumber.description";

    @RBEntry("Sent To Print Attached PDF")
    public static final String PRIVATE_CONSTANT_212 = "object.printMax30PDFbyNumber.tooltip";


    @RBEntry("part_add.gif")
    @RBPseudo(false)
    public static final String PRIVATE_CONSTANT_213 = "object.partNumberGenerator.icon";

    @RBEntry("WTPart CodeGenerator")
    public static final String PRIVATE_CONSTANT_214 = "object.partNumberGenerator.description";

    @RBEntry("WTPart CodeGenerator")
    public static final String PRIVATE_CONSTANT_215 = "object.partNumberGenerator.tooltip";


}
