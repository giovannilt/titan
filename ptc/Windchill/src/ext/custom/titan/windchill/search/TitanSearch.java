package ext.custom.titan.windchill.search;

import wt.fc.*;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.query.*;
import wt.vc.Iterated;

public class TitanSearch {
    private static final String OID = Persistable.PERSIST_INFO + "." +
            PersistInfo.OBJECT_IDENTIFIER + "." + ObjectIdentifier.ID;
    private static final String MASTER = Iterated.MASTER_REFERENCE + "." +
            ObjectReference.KEY + "." + ObjectIdentifier.ID;

    public static void getAllPartsLatestVersionAndIteration() {
        try {
            QuerySpec qs = new QuerySpec();
            qs.setAdvancedQueryEnabled(true);

            int versions = qs.appendClassList(WTPart.class, false);
            int masters = qs.appendClassList(WTPartMaster.class, false);
            int[] a = {masters, versions};
            int[] b = {versions};

            ClassAttribute versionSortId = new ClassAttribute(WTPart.class, "versionInfo.identifier.versionSortId");
            qs.appendSelect(new ClassAttribute(WTPart.class, WTPart.NAME), false);
            qs.appendSelect(versionSortId, false);

            qs.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.LIKE, new StringBuilder("%").append("20867600").append("%").toString()), b);
            qs.appendAnd();
            qs.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", SearchCondition.IS_TRUE), b);
            qs.appendAnd();
            qs.appendWhere(new SearchCondition(WTPartMaster.class, OID, WTPart.class, MASTER), a);
            qs.appendAnd();

            QuerySpec subSelect = new QuerySpec();
            subSelect.setAdvancedQueryEnabled(true);
            subSelect.getFromClause().setAliasPrefix("B");

            int subVersions = subSelect.appendClassList(WTPart.class, false);
            int subMasters = subSelect.appendClassList(WTPartMaster.class, false);
            int[] aa = {subMasters, subVersions};
            int[] bb = {subVersions};

            int[] a_bb = new int[]{subMasters, masters};

            ClassAttribute versionSortId_B = new ClassAttribute(WTPart.class, "versionInfo.identifier.versionSortId");
            SQLFunction maxFunction = SQLFunction.newSQLFunction(SQLFunction.MAXIMUM, versionSortId_B);
            subSelect.appendSelect(maxFunction, bb, false);
            subSelect.appendWhere(new SearchCondition(WTPartMaster.class, OID, WTPart.class, MASTER), aa);
            subSelect.appendAnd();
            subSelect.appendWhere(new SearchCondition(WTPartMaster.class, OID, WTPartMaster.class, OID), a_bb);

            //   SearchCondition join = new SearchCondition(WTPartMaster.class, OID, WTPartMaster.class, OID);
            //   join.setOuterJoin(SearchCondition.RIGHT_OUTER_JOIN);
            //  subSelect.appendWhere(join, new int[] { masters, subMasters});
            //   subSelect.appendJoin(masters, OID, subVersions);

            qs.appendWhere(new SearchCondition(versionSortId, SearchCondition.IN, new SubSelectExpression(subSelect)), b);


            String sql = qs.toString();

            QueryResult qr = PersistenceServerHelper.manager.query(qs);


            while (qr.hasMoreElements()) {
                Object[] obj = (Object[]) qr.nextElement();
                System.out.println("Number of Objects --->>> " + obj.length);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }

    }
}


/*
This XML file does not appear to have any style information associated with it. The document tree is shown below.
<qml bypassAccessControl="false">
<parameter name="Part Number" type="java.lang.String">
<parameterDefault isMacro="false"> %007% </parameterDefault>
</parameter>
<statement>
<query>
<select distinct="false" group="false">
<column alias="Part (wt.part.WTPart)" heading="Number" isExternal="false" propertyName="number" selectOnly="false" type="java.lang.String"> master>number </column>
<column alias="Part (wt.part.WTPart)" heading="Name" isExternal="false" propertyName="name" selectOnly="false" type="java.lang.String"> master>name </column>
<object alias="Part (wt.part.WTPart)" heading="Iteration Display Identifier" propertyName="iterationDisplayIdentifier">
<property name="iterationDisplayIdentifier"/>
</object>
<column alias="Part (wt.part.WTPart)" heading="State" isExternal="false" propertyName="lifeCycleState" selectOnly="false" type="wt.lifecycle.State"> state.state </column>
</select>
<from>
<table alias="Part (wt.part.WTPart)" isExternal="false"> wt.part.WTPart </table>
</from>
<where>
<compositeCondition type="and">
<condition>
<operand>
<column alias="Part (wt.part.WTPart)" heading="Number" isExternal="false" propertyName="number" selectOnly="false" type="java.lang.String"> master>number </column>
</operand>
<operator type="like"/>
<operand>
<parameterTarget name="Part Number"/>
</operand>
</condition>
<condition>
<operand>
<column alias="Part (wt.part.WTPart)" heading="Latest Iteration" isExternal="false" propertyName="latestIteration" selectOnly="false" type="boolean"> iterationInfo.latest </column>
</operand>
<operator type="equal"/>
<operand>
<constant heading="1" isMacro="false" type="java.lang.Object"> 1 </constant>
</operand>
</condition>
<condition>
<operand>
<column alias="Part (wt.part.WTPart)" heading="versionInfo.identifier.versionSortId" isExternal="false" selectOnly="false" type="java.lang.String"> versionInfo.identifier.versionSortId </column>
</operand>
<inOperator type="in"/>
<inOperand>
<subQuery>
<subQuerySelect distinct="false">
<function heading="Maximum" name="MAX" type="java.lang.Object">
<column alias="Part (wt.part.WTPart) 1" heading="versionInfo.identifier.versionSortId" isExternal="false" selectOnly="false" type="java.lang.String"> versionInfo.identifier.versionSortId </column>
</function>
</subQuerySelect>
<from>
<table alias="Part (wt.part.WTPart) 1" isExternal="false"> wt.part.WTPart </table>
</from>
<where>
<compositeCondition type="and">
<condition>
<operand>
<column alias="Part (wt.part.WTPart) 1" heading="Master Reference.Object Id.Id" isExternal="false" propertyName="masterReference.objectId.id" selectOnly="false" type="long"> masterReference.key.id </column>
</operand>
<operator type="equal"/>
<operand>
<column alias="Part (wt.part.WTPart)" heading="Master Reference.Object Id.Id" isExternal="false" propertyName="masterReference.objectId.id" selectOnly="false" type="long"> masterReference.key.id </column>
</operand>
</condition>
</compositeCondition>
</where>
</subQuery>
</inOperand>
</condition>
</compositeCondition>
</where>
</query>
</statement>
</qml>
* */