package ext.custom.titan.windchill;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class TitanProperties {
    private static TitanProperties properties = null;
    private static final String TITAN_PROPERTIES_FILE_NAME = "titan.properties";
    private static final String ME10PDF_SOFT_TYPE_PROPERTY_KEY = "ext.custom.titan.windchill.me10PDFSoftType";
    private static final String SEPARATOR_PROPERTY_KEY = "ext.custom.titan.windchill.separator";
    private static final String SUB_SEPARATOR_PROPERTY_KEY = "ext.custom.titan.windchill.subseparator";
    private static final String NUMBERING_SCHEMA_SEQ_PROPERTY_KEY = "ext.custom.titan.windchill.numberingSchemaSequence";
    private static final String NUMBERING_SCHEMA_SEQ_XML_PATH_PROPERTY_KEY = "ext.custom.titan.windchill.numberingSchemaSequenceXMLPath";
    private static final String NUMBERING_FAMILIES_DEFINITON = "ext.custom.titan.windchill.numberingFamiliesDefinition";
    private static final String NUMBERING_WORKCENTER_PREFIX = "ext.custom.titan.windchill.numberingWorkCenterPrefix";
    private static final String PRINT_CMD_KEY = "ext.custom.titan.windchill.printCmd";
    private static final String PRINT_QUEUE_KEY = "ext.custom.titan.windchill.printQueueName";
    private static final String PRINTER_MAP_KEY = "ext.custom.titan.windchill.printers";
    private static final String UT_GROUP_NAME_KEY = "ext.custom.titan.windchill.utGroupName";
    private static String me10PDFSoftType = "";
    private static String separator = "";
    private static String subsepartaor = "";
    private static Vector<String> numberingSchemaSequence = null;
    private static String numberingSchemaSequenceXMLPath = "";
    private static TreeMap<String, String> numberingFamilies = null;
    private static TreeMap<String, String[]> numberingFamiliesWorkCenters = null;
    private static String workcenterPrefix = "";
    private static String printCmd = "";
    private static String printQueueName = "";
    private static TreeMap<String, String> printerMap = null;
    private static String utGroupName = "";

    private TitanProperties() throws IOException {
        InputStream propStream = TitanProperties.class.getResourceAsStream("titan.properties");
        Properties prop = new Properties();
        prop.load(propStream);
        me10PDFSoftType = prop.getProperty("ext.custom.titan.windchill.me10PDFSoftType");
        separator = prop.getProperty("ext.custom.titan.windchill.separator", ",");
        subsepartaor = prop.getProperty("ext.custom.titan.windchill.subseparator", "=");
        String numberingSchemaSequenceStr = prop.getProperty("ext.custom.titan.windchill.numberingSchemaSequence");
        if (numberingSchemaSequenceStr != null) {
            String[] numberingSchemaSequenceArray = numberingSchemaSequenceStr.split(separator);
            List<String> numberingSchemaSequenceList = Arrays.asList(numberingSchemaSequenceArray);
            numberingSchemaSequence = new Vector(numberingSchemaSequenceList);
        } else {
            numberingSchemaSequence = new Vector();
        }

        String numberingFamiliesDefinitionStr = prop.getProperty("ext.custom.titan.windchill.numberingFamiliesDefinition");
        String[] printerMapArr;
        if (numberingFamiliesDefinitionStr != null) {
            HashMap<String, String> localNumberingFamilies = new HashMap();
            String[] numberingFamiliesDefinitionArr = numberingFamiliesDefinitionStr.split(separator);

            for(int i = 0; i < numberingFamiliesDefinitionArr.length; ++i) {
                String familyDefinition = numberingFamiliesDefinitionArr[i];
                printerMapArr = familyDefinition.split(subsepartaor);
                localNumberingFamilies.put(printerMapArr[0], printerMapArr[1]);
            }

            numberingFamilies = new TreeMap(localNumberingFamilies);
        } else {
            numberingFamilies = new TreeMap();
        }

        numberingSchemaSequenceXMLPath = prop.getProperty("ext.custom.titan.windchill.numberingSchemaSequenceXMLPath");
        workcenterPrefix = prop.getProperty("ext.custom.titan.windchill.numberingWorkCenterPrefix");
        Set<Object> propSet = prop.keySet();
        HashMap<String, String[]> localNumberingFamiliesWorkCenters = new HashMap();
        Iterator propIter = propSet.iterator();

        String printerMapEntry;
        String[] printerMapEntryArr;
        while(propIter.hasNext()) {
            Object o = propIter.next();
            if (o instanceof String) {
                String key = (String)o;
                if (key.indexOf(workcenterPrefix) > -1) {
                    String family = key.substring(key.lastIndexOf(".") + 1);
                    printerMapEntry = prop.getProperty(key);
                    printerMapEntryArr = printerMapEntry.split(separator);
                    localNumberingFamiliesWorkCenters.put(family, printerMapEntryArr);
                }
            }
        }

        numberingFamiliesWorkCenters = new TreeMap(localNumberingFamiliesWorkCenters);
        printCmd = prop.getProperty("ext.custom.titan.windchill.printCmd");
        printQueueName = prop.getProperty("ext.custom.titan.windchill.printQueueName");
        String printerMapStr = prop.getProperty("ext.custom.titan.windchill.printers");
        if (printerMapStr != null) {
            HashMap<String, String> localPrinteMap = new HashMap();
            printerMapArr = printerMapStr.split(separator);

            for(int i = 0; i < printerMapArr.length; ++i) {
                printerMapEntry = printerMapArr[i];
                printerMapEntryArr = printerMapEntry.split(subsepartaor);
                localPrinteMap.put(printerMapEntryArr[0], printerMapEntryArr[1]);
            }

            printerMap = new TreeMap(localPrinteMap);
        } else {
            printerMap = new TreeMap();
        }

        utGroupName = prop.getProperty("ext.custom.titan.windchill.utGroupName");
        propStream.close();
    }

    public static synchronized TitanProperties getProperties() throws IOException {
        if (properties == null) {
            properties = new TitanProperties();
        }

        return properties;
    }

    public String getMe10PDFSoftType() {
        return me10PDFSoftType;
    }

    public Vector<String> getNumberingSchemaSequence() {
        return numberingSchemaSequence;
    }

    public String getNumberingSchemaSequenceXMLPath() {
        return numberingSchemaSequenceXMLPath;
    }

    public String getSubseparator() {
        return subsepartaor;
    }

    public TreeMap<String, String> getNumberingFamilies() {
        return numberingFamilies;
    }

    public TreeMap<String, String[]> getNumberingFamiliesWorkCenters() {
        return numberingFamiliesWorkCenters;
    }

    public String getPrintCmd() {
        return printCmd;
    }

    public String getPrintQueueName() {
        return printQueueName;
    }

    public TreeMap<String, String> getPrinterMap() {
        return printerMap;
    }

    public String getUtGroupName() {
        return utGroupName;
    }
}
