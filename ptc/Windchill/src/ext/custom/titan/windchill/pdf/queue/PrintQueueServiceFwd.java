package ext.custom.titan.windchill.pdf.queue;

import wt.method.RemoteAccess;
import wt.method.RemoteMethodServer;
import wt.services.Manager;
import wt.services.ManagerServiceFactory;
import wt.util.WTException;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.Vector;

public class PrintQueueServiceFwd implements PrintQueueService, Serializable, RemoteAccess {
    private static final long serialVersionUID = 5974966161767995600L;
    private static final boolean SERVER;
    private static final String FC_RESOURCE = "wt.fc.fcResource";

    static {
        SERVER = RemoteMethodServer.ServerFlag;
    }


    private static Manager getManager() throws WTException {
        Manager manager = ManagerServiceFactory.getDefault().getManager(PrintQueueService.class);
        if (manager == null) {
            Object[] aobj = new Object[]{PrintQueueService.class.getName()};
            throw new WTException("wt.fc.fcResource", "40", aobj);
        } else {
            return manager;
        }
    }

    public void addToPrintQueue(Vector<String> v, String printerName, String paperFormat, String copies, String color) throws WTException {
        try {
            if (SERVER) {
                ((PrintQueueService)getManager()).addToPrintQueue(v, printerName, paperFormat, copies, color);
            } else {
                RemoteMethodServer.getDefault().invoke("addToPrintQueue", (String)null, this, new Class[]{Vector.class, String.class, String.class, String.class, String.class}, new Object[]{v, printerName, paperFormat, copies, color});
            }

        } catch (RemoteException var7) {
            var7.printStackTrace();
            throw new WTException(var7);
        } catch (InvocationTargetException var8) {
            var8.printStackTrace();
            throw new WTException(var8);
        }
    }
}