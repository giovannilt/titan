package ext.custom.titan.windchill.pdf.commands;

import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.impl.TypeIdentifierUtilityHelper;
import ext.custom.titan.windchill.TitanProperties;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Vector;
import wt.content.ApplicationData;
import wt.content.ContentHelper;
import wt.content.ContentHolder;
import wt.content.ContentRoleType;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentType;
import wt.fc.ObjectVector;
import wt.fc.QueryResult;
import wt.method.RemoteAccess;
import wt.part.PartDocHelper;
import wt.part.WTPart;
import wt.part.WTPartHelper;
import wt.util.WTException;
import wt.vc.VersionControlHelper;

public class PDFContentCommand implements RemoteAccess, Serializable {
    private static final long serialVersionUID = -1655410554626312020L;
    private static final String PDF_EXTENSION = ".pdf";
    private static TitanProperties titanProperty;

    static {
        try {
            titanProperty = TitanProperties.getProperties();
        } catch (Exception var1) {
            throw new ExceptionInInitializerError(var1);
        }
    }

    public PDFContentCommand() {
    }

    public static QueryResult getPDFContent(WTPart part) {
        QueryResult returnResult = new QueryResult();
        ObjectVector applDataVector = new ObjectVector();

        try {
            Vector<ApplicationData> applDatas = null;
            EPMDocument drw = getContentDRW(part);
            if (drw != null) {
                applDatas = getHPGLSecondaryContents(drw);
                Iterator var6 = applDatas.iterator();

                while(var6.hasNext()) {
                    ApplicationData a = (ApplicationData)var6.next();
                    applDataVector.addElement(a);
                }
            }

            if (applDatas != null && applDatas.size() < 1) {
                WTDocument doc = getMe10PDF(part);
                if (doc != null) {
                    ApplicationData applData = getMe10PDFPrimaryContent(doc);
                    if (applData != null) {
                        applDataVector.addElement(applData);
                    }
                }
            }
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        returnResult.append(applDataVector);
        return returnResult;
    }

    private static EPMDocument getContentDRW(WTPart part) throws WTException {
        EPMDocument drw = null;
        QueryResult qr = PartDocHelper.service.getAssociatedDocuments(part);

        while(qr.hasMoreElements()) {
            Object o = qr.nextElement();
            if (o instanceof EPMDocument) {
                EPMDocument epm = (EPMDocument)o;
                if (VersionControlHelper.isLatestIteration(epm) && epm.getDocType().equals(EPMDocumentType.toEPMDocumentType("CADDRAWING"))) {
                    drw = epm;
                    break;
                }
            }
        }

        return drw;
    }

    private static Vector<ApplicationData> getHPGLSecondaryContents(EPMDocument epm) throws WTException, PropertyVetoException {
        Vector<ApplicationData> applDatas = new Vector();
        ContentHolder holder = ContentHelper.service.getContents(epm);
        Vector<?> items = ContentHelper.getContentListAll(holder);

        for(int j = 0; j < items.size(); ++j) {
            Object o = items.get(j);
            if (o instanceof ApplicationData) {
                ApplicationData localApplData = (ApplicationData)o;
                if (localApplData.getRole().equals(ContentRoleType.SECONDARY)) {
                    applDatas.add(localApplData);
                }
            }
        }

        return applDatas;
    }

    private static boolean isMe10PDF(WTDocument doc) throws RemoteException, WTException {
        boolean isMe10PDF = false;
        TypeIdentifier type = TypeIdentifierUtilityHelper.service.getTypeIdentifier(doc);
        if (type.getTypename().equals(titanProperty.getMe10PDFSoftType())) {
            isMe10PDF = true;
        }

        return isMe10PDF;
    }

    private static WTDocument getMe10PDF(WTPart part) throws WTException, RemoteException {
        WTDocument doc = null;
        QueryResult qr = WTPartHelper.service.getDescribedByWTDocuments(part);

        while(qr.hasMoreElements()) {
            Object o = qr.nextElement();
            if (o instanceof WTDocument) {
                WTDocument localDoc = (WTDocument)o;
                if (isMe10PDF(localDoc)) {
                    doc = localDoc;
                    break;
                }
            }
        }

        return doc;
    }

    private static ApplicationData getMe10PDFPrimaryContent(WTDocument document) throws WTException, PropertyVetoException {
        ApplicationData applData = null;
        ContentHolder holder = ContentHelper.service.getContents(document);
        Vector<?> items = ContentHelper.getContentListAll(holder);

        for(int j = 0; j < items.size(); ++j) {
            Object o = items.get(j);
            if (o instanceof ApplicationData) {
                ApplicationData localApplData = (ApplicationData)o;
                if (localApplData.getRole().equals(ContentRoleType.PRIMARY) && localApplData.getFileName().toLowerCase().endsWith(".pdf")) {
                    applData = localApplData;
                    break;
                }
            }
        }

        return applData;
    }
}
