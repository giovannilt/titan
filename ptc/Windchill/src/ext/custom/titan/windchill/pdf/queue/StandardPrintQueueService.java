package ext.custom.titan.windchill.pdf.queue;

import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.impl.TypeIdentifierUtilityHelper;
import ext.custom.titan.windchill.mail.Mail;
import ext.custom.titan.windchill.TitanProperties;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Vector;
import org.apache.log4j.Logger;
import wt.content.ApplicationData;
import wt.content.ContentHelper;
import wt.content.ContentHolder;
import wt.content.ContentRoleType;
import wt.content.ContentServerHelper;
import wt.content.HolderToContent;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentType;
import wt.fc.ObjectIdentifier;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.fc.WTReference;
import wt.identity.IdentityFactory;
import wt.log4j.LogR;
import wt.org.WTUser;
import wt.part.PartDocHelper;
import wt.part.WTPart;
import wt.part.WTPartHelper;
import wt.queue.ProcessingQueue;
import wt.queue.QueueEntry;
import wt.queue.QueueHelper;
import wt.services.StandardManager;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.vc.VersionControlHelper;

public class StandardPrintQueueService extends StandardManager implements PrintQueueService, Serializable {
    private static final long serialVersionUID = -1878805643345668154L;
    private static final ReferenceFactory factory = new ReferenceFactory();
    private static TitanProperties titanProperty;
    private static final String PDF_EXTENSION = ".pdf";
    private static final Logger log;
    private static final String CLASSNAME = StandardPrintQueueService.class.getName();

    static {
        try {
            log = LogR.getLogger(CLASSNAME);
            titanProperty = TitanProperties.getProperties();
        } catch (Exception var1) {
            throw new ExceptionInInitializerError(var1);
        }
    }

    public StandardPrintQueueService() {
    }

    public static StandardPrintQueueService newStandardPrintQueueService() throws WTException {
        StandardPrintQueueService manager = new StandardPrintQueueService();
        manager.initialize();
        return manager;
    }

    public void addToPrintQueue(Vector<String> partsOid, String printerName, String paperFormat, String copies, String color) throws WTException {
        log.info("addToPrintQueue - Method Start");
        WTUser user = null;

        try {
            user = (WTUser)SessionHelper.manager.getPrincipal();
            SessionHelper.manager.setAdministrator();
            ProcessingQueue printQueue = this.getPrintQueue();
            if (printQueue == null) {
                throw new WTException("Print queue not found");
            }

            log.debug("addToPrintQueue - Found queue " + printQueue);

            for(int i = 0; i < partsOid.size(); ++i) {
                String oid = (String)partsOid.get(i);
                WTPart part = this.getWTPart(oid);
                if (part == null) {
                    log.debug("addToPrintQueue - Part with oid " + oid + " not persistence");
                } else {
                    ApplicationData applData = this.getPDFApplicationData(part);
                    if (applData == null) {
                        log.debug("addToPrintQueue - No content found for part " + IdentityFactory.getDisplayIdentifier(part));
                    } else {
                        ObjectIdentifier applObj = PersistenceHelper.getObjectIdentifier(applData);
                        ObjectIdentifier holderObj = PersistenceHelper.getObjectIdentifier(applData.getHolderLink());
                        ObjectIdentifier userObj = PersistenceHelper.getObjectIdentifier(user);
                        QueueEntry entry = printQueue.addEntry(user, "print", this.getClass().getName(), new Class[]{ObjectIdentifier.class, ObjectIdentifier.class, ObjectIdentifier.class, String.class, String.class, String.class, String.class}, new Object[]{applObj, holderObj, userObj, printerName, paperFormat, copies, color});
                        log.debug("addToPrintQueue - Created Entry " + entry);
                    }
                }
            }
        } catch (Exception var23) {
            throw new WTException(var23);
        } finally {
            try {
                SessionHelper.manager.setPrincipal(user.getName());
            } catch (Exception var22) {
            }

        }

        log.info("addToPrintQueue - Method End");
    }

    private ProcessingQueue getPrintQueue() throws WTException {
        ProcessingQueue queue = null;
        queue = QueueHelper.manager.getQueue(titanProperty.getPrintQueueName());
        return queue;
    }

    private WTPart getWTPart(String oid) {
        WTPart part = null;

        try {
            WTReference reference = factory.getReference(oid);
            if (reference != null && reference.getObject() instanceof WTPart) {
                part = (WTPart)reference.getObject();
            }
        } catch (WTException var4) {
        }

        return part;
    }

    private ApplicationData getPDFApplicationData(WTPart part) throws WTException, PropertyVetoException, RemoteException {
        log.info("getPDFApplicationData - Method Start");
        ApplicationData applData = null;
        EPMDocument drw = getContentDRW(part);
        if (drw != null) {
            applData = getPDFSecondaryContent(drw);
        }

        if (applData == null) {
            WTDocument doc = getMe10PDF(part);
            if (doc != null) {
                applData = getMe10PDFPrimaryContent(doc);
            }
        }

        log.info("getPDFApplicationData - Method End");
        return applData;
    }

    public static void print(ObjectIdentifier appldataIdentifier, ObjectIdentifier holderLinkIdentifier, ObjectIdentifier userIdentifier, String printerName, String paperFormat, String copies, String color) throws WTException {
        log.info("print - Method Start");
        String downloadPath = "";
        ApplicationData applData = null;
        WTUser user = null;
        HolderToContent holderLink = null;

        try {
            downloadPath = downloadPath + WTProperties.getLocalProperties().getProperty("wt.temp");
            downloadPath = downloadPath + WTProperties.getLocalProperties().getProperty("dir.sep");
            applData = (ApplicationData)PersistenceHelper.manager.refresh(appldataIdentifier);
            holderLink = (HolderToContent)PersistenceHelper.manager.refresh(holderLinkIdentifier);
            user = (WTUser)PersistenceHelper.manager.refresh(userIdentifier);
            applData.setHolderLink(holderLink);
            log.debug("print - link " + applData.getHolderLink());
            log.debug("print - holder " + applData.getHolderLink().getContentHolder());
            downloadPath = downloadPath + applData.getFileName();
            log.debug("print - Downloading " + applData + " to " + downloadPath);
            ContentServerHelper.service.writeContentStream(applData, downloadPath);
            String command = createPrintCommand(titanProperty.getPrintCmd(), printerName, paperFormat, copies, color, downloadPath);
            log.debug("print - exec command " + command);
            Process p = Runtime.getRuntime().exec(command);
            int r = p.waitFor();
            log.debug("print - exec result " + r);
            if (r != 0) {
                ContentHolder holder = holderLink.getContentHolder();
                notifyError(user, holder, printerName);
            }
        } catch (Exception var19) {
            log.debug("print - Exception ", var19);
            throw new WTException(var19);
        } finally {
            File f = new File(downloadPath);
            if (!downloadPath.equals("") && f.exists() && f.canWrite()) {
                f.delete();
            }

        }

        log.info("print - Method End");
    }

    private static void notifyError(WTUser user, ContentHolder contentHolder, String printerName) {
        try {
            WTProperties wtProp = WTProperties.getLocalProperties();
            String subject = "Errore Stampa";
            String body = "Errore rilevato durante la stampa di " + IdentityFactory.getDisplayIdentifier(contentHolder) + " sulla stampate " + printerName;
            Mail.sendSimpleMail(wtProp.getProperty("wt.mail.mailhost"), "25", user.getEMail(), wtProp.getProperty("wt.mail.from"), subject, body);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    private static String createPrintCommand(String rowCommand, String... params) {
        for(int i = 0; i < params.length; ++i) {
            if (rowCommand.indexOf("{" + (i + 1) + "}") > -1) {
                rowCommand = rowCommand.replace("{" + (i + 1) + "}", params[i]);
            }
        }

        return rowCommand;
    }

    private static EPMDocument getContentDRW(WTPart part) throws WTException {
        EPMDocument drw = null;
        QueryResult qr = PartDocHelper.service.getAssociatedDocuments(part);

        while(qr.hasMoreElements()) {
            Object o = qr.nextElement();
            if (o instanceof EPMDocument) {
                EPMDocument epm = (EPMDocument)o;
                if (VersionControlHelper.isLatestIteration(epm) && epm.getDocType().equals(EPMDocumentType.toEPMDocumentType("CADDRAWING"))) {
                    drw = epm;
                    break;
                }
            }
        }

        return drw;
    }

    private static ApplicationData getPDFSecondaryContent(EPMDocument epm) throws WTException, PropertyVetoException {
        ApplicationData applData = null;
        ContentHolder holder = ContentHelper.service.getContents(epm);
        Vector<?> items = ContentHelper.getContentListAll(holder);

        for(int j = 0; j < items.size(); ++j) {
            Object o = items.get(j);
            if (o instanceof ApplicationData) {
                ApplicationData localApplData = (ApplicationData)o;
                if (localApplData.getRole().equals(ContentRoleType.SECONDARY) && localApplData.getFileName().toLowerCase().endsWith(".pdf")) {
                    applData = localApplData;
                    break;
                }
            }
        }

        return applData;
    }

    private static boolean isMe10PDF(WTDocument doc) throws RemoteException, WTException {
        boolean isMe10PDF = false;
        TypeIdentifier type = TypeIdentifierUtilityHelper.service.getTypeIdentifier(doc);
        if (type.getTypename().equals(titanProperty.getMe10PDFSoftType())) {
            isMe10PDF = true;
        }

        return isMe10PDF;
    }

    private static WTDocument getMe10PDF(WTPart part) throws WTException, RemoteException {
        WTDocument doc = null;
        QueryResult qr = WTPartHelper.service.getDescribedByWTDocuments(part);

        while(qr.hasMoreElements()) {
            Object o = qr.nextElement();
            if (o instanceof WTDocument) {
                WTDocument localDoc = (WTDocument)o;
                if (isMe10PDF(localDoc)) {
                    doc = localDoc;
                    break;
                }
            }
        }

        return doc;
    }

    private static ApplicationData getMe10PDFPrimaryContent(WTDocument document) throws WTException, PropertyVetoException {
        ApplicationData applData = null;
        ContentHolder holder = ContentHelper.service.getContents(document);
        Vector<?> items = ContentHelper.getContentListAll(holder);

        for(int j = 0; j < items.size(); ++j) {
            Object o = items.get(j);
            if (o instanceof ApplicationData) {
                ApplicationData localApplData = (ApplicationData)o;
                if (localApplData.getRole().equals(ContentRoleType.PRIMARY) && localApplData.getFileName().toLowerCase().endsWith(".pdf")) {
                    applData = localApplData;
                    break;
                }
            }
        }

        return applData;
    }
}
