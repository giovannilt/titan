package ext.custom.titan.windchill.pdf;

import java.util.ListResourceBundle;

public class pdfResource extends ListResourceBundle {
    static final Object[][] contents = new Object[][]{{"part.pdfContent.description", "PDF e DXF"}, {"part.pdfContent.title", "PDF e DXF"}, {"part.pdfContent.tooltip", "PDF e DXF"}};

    public pdfResource() {
    }

    public Object[][] getContents() {
        return contents;
    }
}
