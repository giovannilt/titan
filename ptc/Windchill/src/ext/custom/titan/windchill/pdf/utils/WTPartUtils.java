package ext.custom.titan.windchill.pdf.utils;


import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.query.QueryException;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.vc.VersionControlHelper;

import java.util.Enumeration;
import java.util.Vector;

public class WTPartUtils {

    private WTPartUtils() {
    }

    public static Vector getWTPartsWithView(String number, String view) throws QueryException, WTException {
        WTPartMaster partmaster = null;
        Vector parts = new Vector();
        QuerySpec queryspec = new QuerySpec();
        int masters = queryspec.appendClassList(WTPartMaster.class, true);
        int[] a = new int[]{masters, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), a);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); partmaster = (WTPartMaster)((Persistable[])enumeration.nextElement())[masters]) {
        }

        if (partmaster != null) {
            QueryResult qr = VersionControlHelper.service.allVersionsOf(partmaster);

            while(qr.hasMoreElements()) {
                WTPart part = (WTPart)qr.nextElement();
                if (part.getViewName().equals(view)) {
                    parts.insertElementAt(part, 0);
                }
            }
        }

        return parts;
    }

    public static WTPart getLatestVersionWTPart(String number, String view) throws QueryException, WTException {
        WTPart part = null;
        Vector v = getWTPartsWithView(number, view);
        if (v.size() != 0) {
            part = (WTPart)v.lastElement();
        }

        return part;
    }
}
