package ext.custom.titan.windchill.pdf.beans;

import ext.custom.titan.windchill.TitanProperties;
import com.ptc.netmarkets.model.NmOid;
import com.ptc.netmarkets.util.beans.NmCommandBean;
import wt.fc.Persistable;

import wt.util.WTException;

import java.util.*;

public class PrinterBean {
    private static TitanProperties titanProperty = null;
    private TreeMap<String, String> printerMap = null;

    static {
        try {
            titanProperty = TitanProperties.getProperties();
        } catch (Exception var1) {
            var1.printStackTrace();
            throw new ExceptionInInitializerError("Error in static property load, check titan properties file existence");
        }
    }

    public PrinterBean() {
        if (this.printerMap == null) {
            this.printerMap = titanProperty.getPrinterMap();
        }

    }

    public TreeMap<String, String> getPrinterMap() {
        return this.printerMap;
    }

    public  List<Persistable> getOidObjects(NmCommandBean commandBean) throws WTException {
        List<Persistable> persistableList = new ArrayList<Persistable>();
        @SuppressWarnings({ "rawtypes" })
        ArrayList oids = commandBean.getSelectedOidForPopup();

        if (oids != null && oids.size() != 0) {
            Set<NmOid> NmOidSet = new HashSet<NmOid>();

            for (Object oid : oids) {
                NmOid nmOid = (NmOid) oid;
                // if same object exist, object doesn't add list
                if (!NmOidSet.contains(nmOid)) {
                    NmOidSet.add(nmOid);
                    persistableList.add(nmOid.getWtRef().getObject());
                }
            }

        } else {
            NmOid nmOid = commandBean.getActionOid();
            if(nmOid!=null){
                persistableList.add(nmOid.getWtRef().getObject());
            }
        }
        return persistableList;
    }
}
