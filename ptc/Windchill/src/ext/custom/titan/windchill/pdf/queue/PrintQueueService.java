package ext.custom.titan.windchill.pdf.queue;

import wt.method.RemoteInterface;
import wt.util.WTException;

import java.util.Vector;
@RemoteInterface
public interface PrintQueueService {
    void addToPrintQueue(Vector<String> v, String paramString1, String paramString2, String paramString3, String paramString4) throws WTException;
}
