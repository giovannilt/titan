package ext.custom.titan.windchill.promotion;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.iba.IBAException;
import wt.iba.definition.AbstractAttributeDefinition;
import wt.iba.definition.BooleanDefinition;
import wt.iba.definition.FloatDefinition;
import wt.iba.definition.IntegerDefinition;
import wt.iba.definition.StringDefinition;
import wt.iba.definition.TimestampDefinition;
import wt.iba.definition.URLDefinition;
import wt.iba.value.BooleanValue;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.FloatValue;
import wt.iba.value.IBAHolder;
import wt.iba.value.IntegerValue;
import wt.iba.value.StringValue;
import wt.iba.value.TimestampValue;
import wt.iba.value.URLValue;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.litevalue.BooleanValueDefaultView;
import wt.iba.value.litevalue.FloatValueDefaultView;
import wt.iba.value.litevalue.IntegerValueDefaultView;
import wt.iba.value.litevalue.StringValueDefaultView;
import wt.iba.value.litevalue.TimestampValueDefaultView;
import wt.iba.value.litevalue.URLValueDefaultView;
import wt.iba.value.service.IBAValueHelper;
import wt.units.service.MeasurementSystemDefaultView;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;

public class IBAHelper {
    public static final String BOOLEAN_TYPE = "BooleanType";
    public static final String STRING_TYPE = "StringType";
    public static final String INT_TYPE = "NumberType";
    public static final String FLOAT_TYPE = "FloatType";
    public static final String TIMESTAM_TYPE = "TimestampType";
    public static final String URL_TYPE = "URLType";

    public IBAHelper() {
    }

    public static void assignIBA(String ibaname, AbstractValueView thisAbstractValueView, DefaultAttributeContainer defaultattributecontainer, String ibavalue, String secondvalue) throws IBAException, WTPropertyVetoException {
        boolean VERBOSE = false;
        if (thisAbstractValueView instanceof BooleanValueDefaultView) {
            BooleanValueDefaultView theIBA = (BooleanValueDefaultView)thisAbstractValueView;
            if (VERBOSE) {
                System.out.println("Value : " + ibavalue);
            }

            if (!ibavalue.equalsIgnoreCase("true") && !ibavalue.equalsIgnoreCase("1") && !ibavalue.equalsIgnoreCase("on")) {
                theIBA.setValue(false);
            } else {
                theIBA.setValue(true);
            }

            defaultattributecontainer.updateAttributeValue(theIBA);
        } else if (thisAbstractValueView instanceof IntegerValueDefaultView) {
            IntegerValueDefaultView theIBA = (IntegerValueDefaultView)thisAbstractValueView;
            long value = 0L;

            try {
                value = Integer.valueOf(ibavalue).longValue();
            } catch (NumberFormatException var11) {
                throw new IBAException("Not a valid value " + ibaname);
            }

            theIBA.setValue(value);
            defaultattributecontainer.updateAttributeValue(theIBA);
        } else if (thisAbstractValueView instanceof StringValueDefaultView) {
            StringValueDefaultView theIBA = (StringValueDefaultView)thisAbstractValueView;
            theIBA.setValue(ibavalue);
            defaultattributecontainer.updateAttributeValue(theIBA);
        } else if (thisAbstractValueView instanceof FloatValueDefaultView) {
            FloatValueDefaultView theIBA = (FloatValueDefaultView)thisAbstractValueView;
            if (VERBOSE) {
                System.out.println("String : " + ibavalue);
            }

            double value = 0.0D;
            int precision = setFloatPrecision(ibavalue);

            try {
                value = Double.valueOf(ibavalue);
            } catch (NumberFormatException var12) {
                throw new IBAException("Not a valid value " + ibaname);
            }

            if (VERBOSE) {
                System.out.println("Double Value : " + value);
            }

            theIBA.setValue(value);
            theIBA.setPrecision(precision);
            defaultattributecontainer.updateAttributeValue(theIBA);
        } else if (thisAbstractValueView instanceof URLValueDefaultView) {
            URLValueDefaultView theIBA = (URLValueDefaultView)thisAbstractValueView;
            theIBA.setValue(ibavalue);
            if (secondvalue != null && !secondvalue.equals("")) {
                theIBA.setDescription(secondvalue);
                if (VERBOSE) {
                    System.out.println("Setting secondary value " + secondvalue);
                }
            }

            defaultattributecontainer.updateAttributeValue(theIBA);
        } else if (thisAbstractValueView instanceof TimestampValueDefaultView) {
            TimestampValueDefaultView theIBA = (TimestampValueDefaultView)thisAbstractValueView;
            SimpleDateFormat df = new SimpleDateFormat(secondvalue);
            Date today = null;

            try {
                today = df.parse(ibavalue);
            } catch (ParseException var13) {
                throw new IBAException("Valore Data non corretto per " + thisAbstractValueView.getDefinition().getName() + " pattern " + secondvalue);
            }

            Timestamp time = new Timestamp(today.getTime());
            theIBA.setValue(time);
            defaultattributecontainer.updateAttributeValue(theIBA);
        }
    }

    public static void setIBAValue(IBAHolder ibaholder, String ibaname, String ibavalue, String secondvalue) throws WTException, RemoteException, WTPropertyVetoException {
        boolean VERBOSE = false;
        boolean found = false;
        ibaholder = IBAValueHelper.service.refreshAttributeContainer(ibaholder, (Object)null, (Locale)null, (MeasurementSystemDefaultView)null);
        if (VERBOSE && ibaholder == null) {
            System.out.println("IBAHelper - setIBAValue - ibaholder is null");
        }

        DefaultAttributeContainer defaultattributecontainer = (DefaultAttributeContainer)ibaholder.getAttributeContainer();
        if (VERBOSE && defaultattributecontainer == null) {
            System.out.println("IBAHelper - setIBAValue - defaultattributecontainer is null");
        }

        AbstractValueView[] abstractValuesView = defaultattributecontainer.getAttributeValues();
        if (VERBOSE) {
            System.out.println("IBAHelper - setIBAValue - abstractValuesView.length" + abstractValuesView.length);
        }

        for(int k = 0; k < abstractValuesView.length; ++k) {
            if (abstractValuesView[k].getDefinition().getName().equals(ibaname)) {
                assignIBA(ibaname, abstractValuesView[k], defaultattributecontainer, ibavalue, secondvalue);
                found = true;
                break;
            }
        }

        if (!found) {
            if (VERBOSE) {
                System.out.println("IBAHelper - setIBAValue - Nessun attributo di nome " + ibaname);
            }

            if (secondvalue != null && !secondvalue.equals("")) {
                createNewIBAValue(ibaholder, ibaname, ibavalue, secondvalue);
            } else {
                createNewIBAValue(ibaholder, ibaname, ibavalue);
            }

            System.out.println("IBAHelper - setIBAValue - Attributo settato: (" + ibaname + "->" + ibavalue + ")");
        } else {
            if (VERBOSE) {
                System.out.println("IBAHelper - setIBAValue - update IBA Value");
            }

            IBAValueHelper.service.updateIBAHolder(ibaholder, (Object)null, (Locale)null, (MeasurementSystemDefaultView)null);
        }

        if (VERBOSE && defaultattributecontainer == null) {
        }

        ibaholder.setAttributeContainer(defaultattributecontainer);
    }

    public static IBAHolder createNewIBAValue(IBAHolder holder, String attributeName, String attributeValue) throws WTException, WTPropertyVetoException, RemoteException {
        AbstractAttributeDefinition newattribute = IBAUtils.findIBADefinition(attributeName);
        if (newattribute == null) {
            throw new IBAException("IBA non trovato!!!");
        } else {
            IBAValueHelper.service.refreshAttributeContainer(holder, (Object)null, (Locale)null, (MeasurementSystemDefaultView)null);
            if (newattribute instanceof StringDefinition) {
                StringValue sv = StringValue.newStringValue((StringDefinition)newattribute, holder, attributeValue);
                PersistenceHelper.manager.store(sv);
                return holder;
            } else {
                BooleanValue bv;
                if (!(newattribute instanceof BooleanDefinition)) {
                    if (newattribute instanceof IntegerDefinition) {
                        long value = 0L;

                        try {
                            value = Integer.valueOf(attributeValue).longValue();
                        } catch (NumberFormatException var8) {
                            throw new IBAException("Valore numerico non corretto per " + newattribute);
                        }

                        IntegerValue intv = IntegerValue.newIntegerValue((IntegerDefinition)newattribute, holder, value);
                        PersistenceHelper.manager.store(intv);
                        return holder;
                    } else if (newattribute instanceof FloatDefinition) {
                        double value = 0.0D;
                        int precision = setFloatPrecision(attributeValue);

                        try {
                            value = Double.valueOf(attributeValue);
                        } catch (NumberFormatException var9) {
                            throw new IBAException("Valore Float non corretto per " + newattribute);
                        }

                        FloatValue fv = FloatValue.newFloatValue((FloatDefinition)newattribute, holder, value, precision);
                        PersistenceHelper.manager.store(fv);
                        return holder;
                    } else if (newattribute instanceof TimestampDefinition) {
                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        bv = null;

                        Date today;
                        try {
                            today = df.parse(attributeValue);
                        } catch (ParseException var11) {
                            System.out.println("Valore Data non corretto per " + newattribute.getName() + " pattern dd-MM-yyyy");

                            try {
                                df = new SimpleDateFormat("dd/MM/yyyy");
                                today = df.parse(attributeValue);
                            } catch (ParseException var10) {
                                throw new IBAException("Valore Data non corretto per " + newattribute.getName() + " pattern dd-MM-yyyy");
                            }
                        }

                        Timestamp time = new Timestamp(today.getTime());
                        TimestampValue tv = TimestampValue.newTimestampValue((TimestampDefinition)newattribute, holder, time);
                        PersistenceHelper.manager.store(tv);
                        return holder;
                    } else {
                        throw new IBAException("Tipo dell'IBA non riconosciuto!!!");
                    }
                } else {
                    boolean temp = false;
                    if (attributeValue.equalsIgnoreCase("true") || attributeValue.equalsIgnoreCase("1") || attributeValue.equalsIgnoreCase("on")) {
                        temp = true;
                    }

                    bv = BooleanValue.newBooleanValue((BooleanDefinition)newattribute, holder, temp);
                    PersistenceHelper.manager.store(bv);
                    return holder;
                }
            }
        }
    }

    public static IBAHolder createNewIBAValue(IBAHolder holder, String attributeName, String attributeValue, String secondValue) throws WTException, WTPropertyVetoException, RemoteException {
        AbstractAttributeDefinition newattribute = IBAUtils.findIBADefinition(attributeName);
        if (newattribute == null) {
            throw new IBAException("IBA non trovato!!!");
        } else {
            IBAValueHelper.service.refreshAttributeContainer(holder, (Object)null, (Locale)null, (MeasurementSystemDefaultView)null);
            if (newattribute instanceof StringDefinition) {
                StringValue sv = StringValue.newStringValue((StringDefinition)newattribute, holder, attributeValue);
                PersistenceHelper.manager.store(sv);
                return holder;
            } else {
                BooleanValue bv;
                if (!(newattribute instanceof BooleanDefinition)) {
                    if (newattribute instanceof IntegerDefinition) {
                        long value = 0L;

                        try {
                            value = Integer.valueOf(attributeValue).longValue();
                        } catch (NumberFormatException var9) {
                            throw new IBAException("Valore numerico non corretto per " + newattribute);
                        }

                        IntegerValue intv = IntegerValue.newIntegerValue((IntegerDefinition)newattribute, holder, value);
                        PersistenceHelper.manager.store(intv);
                        return holder;
                    } else if (newattribute instanceof FloatDefinition) {
                        double value = 0.0D;
                        int precision = setFloatPrecision(attributeValue);

                        try {
                            value = Double.valueOf(attributeValue);
                        } catch (NumberFormatException var10) {
                            throw new IBAException("Valore Float non corretto per " + newattribute);
                        }

                        FloatValue fv = FloatValue.newFloatValue((FloatDefinition)newattribute, holder, value, precision);
                        PersistenceHelper.manager.store(fv);
                        return holder;
                    } else if (newattribute instanceof TimestampDefinition) {
                        SimpleDateFormat df = new SimpleDateFormat(secondValue);
                        bv = null;

                        Date today;
                        try {
                            today = df.parse(attributeValue);
                        } catch (ParseException var11) {
                            throw new IBAException("Valore Data non corretto per " + newattribute.getName() + " pattern " + secondValue);
                        }

                        Timestamp time = new Timestamp(today.getTime());
                        TimestampValue tv = TimestampValue.newTimestampValue((TimestampDefinition)newattribute, holder, time);
                        PersistenceHelper.manager.store(tv);
                        return holder;
                    } else if (newattribute instanceof URLDefinition) {
                        URLValue url = URLValue.newURLValue((URLDefinition)newattribute, holder, attributeValue, secondValue);
                        PersistenceHelper.manager.store(url);
                        return holder;
                    } else {
                        throw new IBAException("Tipo dell'IBA non riconosciuto!!!");
                    }
                } else {
                    boolean temp = false;
                    if (attributeValue.equalsIgnoreCase("true") || attributeValue.equalsIgnoreCase("1") || attributeValue.equalsIgnoreCase("on")) {
                        temp = true;
                    }

                    bv = BooleanValue.newBooleanValue((BooleanDefinition)newattribute, holder, temp);
                    PersistenceHelper.manager.store(bv);
                    return holder;
                }
            }
        }
    }

    private static int setFloatPrecision(String value) {
        boolean VERBOSE = false;
        int p = 2;
        int dot = value.indexOf(".");
        int subPrecision = 0;
        if (dot != -1) {
            String t = value.substring(0, dot);
            String s = value.substring(dot + 1);
            if (VERBOSE) {
                System.out.println("T: " + t + " S: " + s);
            }

            String[] a = new String[s.length()];

            int i;
            for(i = 0; i < s.length(); ++i) {
                a[i] = s.substring(i, i + 1);
            }

            for(i = 0; i < a.length && a[i].equals("0"); ++i) {
                ++subPrecision;
            }

            if (value.startsWith("0.")) {
                p = s.length() - subPrecision;
            } else {
                p = s.length() + t.length();
            }
        }

        if (VERBOSE) {
            System.out.println("Setting Precision: " + p);
        }

        return p;
    }

    public static void updateIBA(IBAHolder holder, Hashtable<String, String> h) throws WTException, WTPropertyVetoException, RemoteException {
        for(Enumeration e = h.keys(); e.hasMoreElements(); holder = (IBAHolder)PersistenceHelper.manager.refresh((Persistable)holder)) {
            String key = (String)e.nextElement();
            String value = (String)h.get(key);
            setIBAValue(holder, key, value, "");
        }

    }
}

