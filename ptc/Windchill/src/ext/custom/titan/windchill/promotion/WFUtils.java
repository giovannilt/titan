package ext.custom.titan.windchill.promotion;

import java.beans.PropertyVetoException;
import java.util.Vector;
import wt.content.ApplicationData;
import wt.content.ContentHelper;
import wt.content.ContentHolder;
import wt.content.ContentRoleType;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentType;
import wt.fc.QueryResult;
import wt.fc.WTObject;
import wt.identity.IdentityFactory;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleManaged;
import wt.lifecycle.State;
import wt.org.WTPrincipal;
import wt.org.WTPrincipalReference;
import wt.part.PartDocHelper;
import wt.part.WTPart;
import wt.part.WTPartHelper;
import wt.project.Role;
import wt.series.MultilevelSeries;
import wt.team.Team;
import wt.team.TeamHelper;
import wt.team.TeamManaged;
import wt.util.WTException;
import wt.vc.VersionControlHelper;
import wt.vc.VersionIdentifier;
import wt.vc.Versioned;

public class WFUtils {
    private static final String CLASSNAME = WFUtils.class.getName();
    private static State CANCELLED = State.toState("CANCELLED");

    public WFUtils() {
    }

    public static boolean isFirstVersion(Versioned versioned) {
        boolean isFirstVersion = false;

        try {
            String seriesName = versioned.getVersionIdentifier().getSeries().getUniqueSeriesName();
            MultilevelSeries series = MultilevelSeries.newMultilevelSeries(seriesName);
            VersionIdentifier versionIdent = VersionIdentifier.newVersionIdentifier(series);
            if (versionIdent.getValue().equals(versioned.getVersionIdentifier().getValue())) {
                isFirstVersion = true;
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }

        return isFirstVersion;
    }

    public static void cancelOldVersion(Versioned obj) {
        try {
            QueryResult qr = VersionControlHelper.service.allVersionsFrom(obj);

            while(true) {
                Versioned versioned;
                do {
                    if (!qr.hasMoreElements()) {
                        return;
                    }

                    versioned = (Versioned)qr.nextElement();
                } while(versioned.getVersionIdentifier().getValue().equals(obj.getVersionIdentifier().getValue()));

                State releaseState = ((LifeCycleManaged)obj).getLifeCycleState();
                LifeCycleManaged lcManaged = (LifeCycleManaged)versioned;
                System.out.println(CLASSNAME + " - cancelOldVersion - lcManaged=" + IdentityFactory.getDisplayIdentity(lcManaged));
                System.out.println(CLASSNAME + " - cancelOldVersion - releaseState=" + releaseState.toString());
                if ((releaseState.toString().equals(State.RELEASED.toString()) || releaseState.toString().equals(CANCELLED.toString())) && !lcManaged.getLifeCycleState().toString().equals(CANCELLED.toString())) {
                    System.out.println(CLASSNAME + " - cancelOldVersion - Set " + CANCELLED);
                    lcManaged = LifeCycleHelper.service.setLifeCycleState(lcManaged, CANCELLED, true);
                }

                if (lcManaged instanceof WTPart) {
                    setMe10State((WTPart)versioned);
                }
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }
    }

    public static void setMe10State(WTPart part) {
        try {
            QueryResult qr = WTPartHelper.service.getDescribedByWTDocuments(part);

            while(qr.hasMoreElements()) {
                LifeCycleManaged lcMan = (LifeCycleManaged)qr.nextElement();
                lcMan = LifeCycleHelper.service.setLifeCycleState(lcMan, part.getLifeCycleState(), true);
                cancelOldVersion((Versioned)lcMan);
            }

            QueryResult qr2 = PartDocHelper.service.getAssociatedDocuments(part);

            while(qr2.hasMoreElements()) {
                LifeCycleManaged lcMan = (LifeCycleManaged)qr2.nextElement();
                lcMan = LifeCycleHelper.service.setLifeCycleState(lcMan, part.getLifeCycleState(), true);
                cancelOldVersion((Versioned)lcMan);
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }

    public static void setSubmitterCreator(WTObject primaryBusinessObject) {
        try {
            Role r = Role.toRole("SUBMITTER");
            WTPrincipalReference creatorReference = ((Versioned)primaryBusinessObject).getCreator();
            WTPrincipal creator = creatorReference.getPrincipal();
            Team t = TeamHelper.service.getTeam((TeamManaged)primaryBusinessObject);
            TeamHelper.service.deleteRole(r, t);
            TeamHelper.service.addRolePrincipalMap(r, creator, t);
            LifeCycleHelper.service.augmentRoles((LifeCycleManaged)primaryBusinessObject);
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    public static boolean checkObjectPrintableFormatExistence(WTPart part) throws WTException, PropertyVetoException {
        EPMDocument drw = getContentDRW(part);
        return drw == null || checkDRWSecondaryExistence(drw);
    }

    private static EPMDocument getContentDRW(WTPart part) throws WTException {
        EPMDocument drw = null;
        QueryResult qr = PartDocHelper.service.getAssociatedDocuments(part);

        while(qr.hasMoreElements()) {
            Object o = qr.nextElement();
            if (o instanceof EPMDocument) {
                EPMDocument epm = (EPMDocument)o;
                if (VersionControlHelper.isLatestIteration(epm) && epm.getDocType().equals(EPMDocumentType.toEPMDocumentType("CADDRAWING"))) {
                    drw = epm;
                    break;
                }
            }
        }

        return drw;
    }

    private static boolean checkDRWSecondaryExistence(EPMDocument epm) throws WTException, PropertyVetoException {
        ContentHolder holder = ContentHelper.service.getContents(epm);
        Vector<?> items = ContentHelper.getContentListAll(holder);

        for(int j = 0; j < items.size(); ++j) {
            Object o = items.get(j);
            if (o instanceof ApplicationData) {
                ApplicationData localApplData = (ApplicationData)o;
                if (localApplData.getRole().equals(ContentRoleType.SECONDARY)) {
                    return true;
                }
            }
        }

        return false;
    }
}

