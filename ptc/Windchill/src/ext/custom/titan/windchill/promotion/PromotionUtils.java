package ext.custom.titan.windchill.promotion;

import wt.fc.QueryResult;
import wt.maturity.MaturityBaseline;
import wt.maturity.PromotionNotice;
import wt.part.WTPart;
import wt.vc.baseline.BaselineHelper;
import wt.vc.baseline.Baselineable;

public class PromotionUtils {
    public PromotionUtils() {
    }

    public static boolean arePromotionObjectsClassified(PromotionNotice pn) {
        boolean areObjectsClassified = true;

        try {
            MaturityBaseline maturityBaseline = pn.getConfiguration();
            QueryResult qr = BaselineHelper.service.getBaselineItems(maturityBaseline);
            Baselineable b;
            while (qr.hasMoreElements()) {
                b = (Baselineable) qr.nextElement();
                if (b instanceof WTPart) {
                    WTPart part = (WTPart) b;
                    areObjectsClassified = areObjectsClassified && ClassificationUtils.isClassfied(part);
                }
            }

        } catch (Exception var7) {
            var7.printStackTrace();
        }

        return areObjectsClassified;
    }

    public static boolean checkPromotionObjectsPrintableFormatExistence(PromotionNotice pn) {
        boolean result = true;

        try {
            MaturityBaseline maturityBaseline = pn.getConfiguration();
            QueryResult qr = BaselineHelper.service.getBaselineItems(maturityBaseline);

            while (qr.hasMoreElements()) {
                Baselineable b = (Baselineable) qr.nextElement();
                if (b instanceof WTPart) {
                    WTPart part = (WTPart) b;
                    if (!WFUtils.checkObjectPrintableFormatExistence(part)) {
                        result = false;
                        break;
                    }
                }
            }
        } catch (Exception var6) {
            var6.printStackTrace();
        }

        return result;
    }
}
