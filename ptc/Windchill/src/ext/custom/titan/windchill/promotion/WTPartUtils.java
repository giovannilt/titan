package ext.custom.titan.windchill.promotion;

import java.util.Enumeration;
import java.util.Vector;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentMaster;
import wt.epm.structure.EPMStructureHelper;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.WTObject;
import wt.inf.container.WTContainerRef;
import wt.part.LineNumber;
import wt.part.Quantity;
import wt.part.QuantityUnit;
import wt.part.WTPart;
import wt.part.WTPartDescribeLink;
import wt.part.WTPartHelper;
import wt.part.WTPartMaster;
import wt.part.WTPartUsageLink;
import wt.query.QueryException;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.VersionControlHelper;
import wt.vc.config.ConfigHelper;
import wt.vc.config.ConfigSpec;
import wt.vc.views.View;

public class WTPartUtils {
    private static final String OID = "thePersistInfo.theObjectIdentifier.id";
    private static final String MASTER = "masterReference.key.id";
    private static final String VIEW = "view.key.id";
    public static final int WTPART_BOM_INDEX = 0;
    public static final int WTPARTUSAGELINK_BOM_INDEX = 1;

    public WTPartUtils() {
    }

    public static WTPart getWTPart(String number, String view) throws QueryException, WTException {
        WTPart part = null;
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{versions, views};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        int[] e = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "view.key.id", View.class, "thePersistInfo.theObjectIdentifier.id"), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), e);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); part = (WTPart)((Persistable[])enumeration.nextElement())[versions]) {
        }

        return part;
    }

    public static Vector getBom(WTPart p, ConfigSpec cs, long livello) {
        Vector distinta = new Vector();
        Vector element = new Vector();

        try {
            element.add(p);
            Quantity q = new Quantity();
            q.setAmount(1.0D);
            q.setUnit(QuantityUnit.EA);
            WTPartUsageLink link = new WTPartUsageLink();
            link.setQuantity(q);
            link.setLineNumber(LineNumber.newLineNumber(livello));
            element.add(link);
            distinta.add(element);
            QueryResult qr = WTPartHelper.service.getUsesWTParts(p, cs);
            Enumeration enumeration = qr.getEnumeration();

            while(enumeration.hasMoreElements()) {
                Persistable[] persistObj = (Persistable[])enumeration.nextElement();
                WTPartUsageLink partLink = (WTPartUsageLink)persistObj[0];
                WTPart partObj = (WTPart)persistObj[1];
                partLink.setLineNumber(LineNumber.newLineNumber(livello + 1L));
                element = new Vector();
                element.add(partObj);
                element.add(partLink);
                distinta.add(element);
            }
        } catch (WTException var13) {
            var13.printStackTrace();
        } catch (WTPropertyVetoException var14) {
            var14.printStackTrace();
        }

        return distinta;
    }

    public static Vector expandAllBom(WTPart p, ConfigSpec cs, long livello) {
        boolean VERBOSE = false;
        Vector distinta = new Vector();

        try {
            if (livello == 0L) {
                Vector element = new Vector();
                element.add(p);
                Quantity q = new Quantity();
                q.setAmount(1.0D);
                q.setUnit(QuantityUnit.EA);
                WTPartUsageLink link = new WTPartUsageLink();
                link.setQuantity(q);
                link.setLineNumber(LineNumber.newLineNumber(livello));
                element.add(link);
                distinta.add(element);
            }

            QueryResult qr = WTPartHelper.service.getUsesWTParts(p, cs);
            Enumeration enumeration = qr.getEnumeration();

            while(enumeration.hasMoreElements()) {
                Persistable[] persistObj = (Persistable[])enumeration.nextElement();
                if (VERBOSE) {
                    System.out.println("Numero Oggetti: " + persistObj.length);
                    System.out.println("Tipo 1: " + persistObj[0].getClass().toString());
                    System.out.println("Tipo 2: " + persistObj[1].getClass().toString());
                    System.out.println("Identity 1: " + persistObj[0].getIdentity());
                    System.out.println("Identity 2: " + persistObj[1].getIdentity());
                }

                WTPartUsageLink partLink = (WTPartUsageLink)persistObj[0];
                WTPart partObj = (WTPart)persistObj[1];
                partLink.setLineNumber(LineNumber.newLineNumber(livello + 1L));
                Vector element = new Vector();
                element.add(partObj);
                element.add(partLink);
                distinta.add(element);
                Vector distintaFigli = expandAllBom(partObj, cs, livello + 1L);

                for(int i = 0; i < distintaFigli.size(); ++i) {
                    distinta.add((Vector)distintaFigli.get(i));
                }
            }
        } catch (WTException var14) {
            var14.printStackTrace();
        } catch (WTPropertyVetoException var15) {
            var15.printStackTrace();
        }

        return distinta;
    }

    public static Vector expandOneLevelBom(Vector distinta, int indice) {
        Vector bom = new Vector();

        for(int i = 0; i < distinta.size(); ++i) {
            bom.add(distinta.get(i));
        }

        Vector v = (Vector)distinta.get(indice);
        WTPart p = (WTPart)v.get(0);
        WTPartUsageLink link = (WTPartUsageLink)v.get(1);
        long livello = link.getLineNumber().getValue();

        try {
            ConfigSpec cs = ConfigHelper.service.getConfigSpecFor(p);
            Vector figli = getBom(p, cs, livello);
            figli.remove(0);

            for(int i = 0; i < figli.size(); ++i) {
                int insertAt = indice + 1 + i;
                if (insertAt > bom.size()) {
                    bom.add((Vector)figli.get(i));
                } else {
                    bom.insertElementAt((Vector)figli.get(i), insertAt);
                }
            }
        } catch (WTException var12) {
            var12.printStackTrace();
        }

        return bom;
    }

    public static WTPart getVersionWTPart(String number, String version, String view) throws WTException, QueryException {
        WTPart part = null;
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{versions, views};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        int[] e = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "view.key.id", View.class, "thePersistInfo.theObjectIdentifier.id"), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "versionInfo.identifier.versionId", "=", version), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), e);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); part = (WTPart)((Persistable[])enumeration.nextElement())[versions]) {
        }

        return part;
    }

    public static WTPart getVersionIterationWTPart(String number, String version, String iteration, String view) throws WTException, QueryException {
        WTPart part = null;
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{versions, views};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        int[] e = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "view.key.id", View.class, "thePersistInfo.theObjectIdentifier.id"), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "versionInfo.identifier.versionId", "=", version), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), e);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            if (part.getIterationIdentifier().getValue().equals(iteration)) {
                break;
            }
        }

        return part;
    }

    public static Vector<WTPart> getContainerWTParts(String number, String name, WTContainerRef wtcontent, String view) throws QueryException, WTException {
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        Vector<WTPart> wtparts = new Vector();
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        int[] c = new int[]{versions, -1};
        int[] d = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.CONTAINER_ID, "=", wtcontent.getObjectId().getId()), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), c);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static WTPart getVersionWTPart(String number, String version) throws WTException, QueryException {
        WTPart part = null;
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "versionInfo.identifier.versionId", "=", version), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), d);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); part = (WTPart)((Persistable[])enumeration.nextElement())[versions]) {
        }

        return part;
    }

    public static Vector getVersionWTParts(String number, String version, String view) throws WTException, QueryException {
        Vector wtparts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{versions, views};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        int[] e = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "view.key.id", View.class, "thePersistInfo.theObjectIdentifier.id"), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "versionInfo.identifier.versionId", "=", version), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), e);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static Vector getWTParts(String number, String name) throws QueryException, WTException {
        Vector wtparts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        int[] c = new int[]{versions, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), c);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static Vector<WTPart> getContainerWTParts(String number, String name, WTContainerRef wtcontent) throws QueryException, WTException {
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        Vector<WTPart> wtparts = new Vector();
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        int[] c = new int[]{versions, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.CONTAINER_ID, "=", wtcontent.getObjectId().getId()), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), c);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static Vector getEndItems(String number, String name) throws QueryException, WTException {
        Vector wtparts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        int[] c = new int[]{versions, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), c);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static Vector getWTParts(String number, String name, String view) throws QueryException, WTException {
        Vector wtparts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int views = queryspec.appendClassList(View.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{versions, views};
        int[] c = new int[]{masters, -1};
        int[] d = new int[]{versions, -1};
        int[] e = new int[]{views, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "view.key.id", View.class, "thePersistInfo.theObjectIdentifier.id"), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), c);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), d);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(View.class, "name", "=", view), e);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            wtparts.add(part);
        }

        return wtparts;
    }

    public static Vector getWTPartsWithView(String number, String view) throws QueryException, WTException {
        WTPartMaster partmaster = null;
        Vector parts = new Vector();
        QuerySpec queryspec = new QuerySpec();
        int masters = queryspec.appendClassList(WTPartMaster.class, true);
        int[] a = new int[]{masters, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), a);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); partmaster = (WTPartMaster)((Persistable[])enumeration.nextElement())[masters]) {
        }

        if (partmaster != null) {
            QueryResult qr = VersionControlHelper.service.allVersionsOf(partmaster);

            while(qr.hasMoreElements()) {
                WTPart part = (WTPart)qr.nextElement();
                if (part.getViewName().equals(view)) {
                    parts.insertElementAt(part, 0);
                }
            }
        }

        return parts;
    }

    public static boolean checkWTPartView(String number, String partversion, String view) throws WTException {
        Vector wtparts = getWTPartsWithView(number, view);

        for(int i = 0; i < wtparts.size(); ++i) {
            WTPart p = (WTPart)wtparts.get(i);
            String version = p.getVersionIdentifier().getValue();
            if (version.startsWith(partversion)) {
                return true;
            }
        }

        return false;
    }

    public static Vector getDescribedByLinkWTDocument(WTPart part) throws WTException {
        Vector links = new Vector();
        QueryResult qr = WTPartHelper.service.getDescribedByWTDocuments(part, false);

        while(qr.hasMoreElements()) {
            Persistable pers = (Persistable)qr.nextElement();
            if (pers instanceof WTPartDescribeLink) {
                links.add((WTPartDescribeLink)pers);
            }
        }

        return links;
    }

    public static Vector getAllIterationWTParts(String number) throws WTException, QueryException {
        Vector parts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            parts.add(part);
        }

        return parts;
    }

    public static Vector getEPMDocuments(WTPart part) throws WTException {
        Vector links = new Vector();
        QueryResult qr = WTPartHelper.service.getDescribedByDocuments(part);

        while(qr.hasMoreElements()) {
            Persistable pers = (Persistable)qr.nextElement();
            if (pers instanceof EPMDocument) {
                links.add((EPMDocument)pers);
            }
        }

        return links;
    }

    public static boolean isAlreadyLinked(WTPart part, EPMDocument caddoc) throws WTException {
        Vector links = getEPMDocuments(part);

        for(int i = 0; i < links.size(); ++i) {
            EPMDocument epm = (EPMDocument)links.get(i);
            if (epm.getNumber().equals(caddoc.getNumber())) {
                return true;
            }
        }

        return false;
    }

    public static WTPart getLatestVersionWTPart(String number, String view) throws QueryException, WTException {
        WTPart part = null;
        Vector v = getWTPartsWithView(number, view);
        if (v.size() != 0) {
            part = (WTPart)v.lastElement();
        }

        return part;
    }

    public static Vector getLatestVersionWTParts(String number, String view) throws QueryException, WTException {
        Vector parts = new Vector();
        Vector appoggio = new Vector();
        Vector allversionparts = getWTParts(number, "*", view);

        int i;
        WTPart p;
        for(i = 0; i < allversionparts.size(); ++i) {
            p = (WTPart)allversionparts.get(i);
            boolean trovato = false;

            for(int j = 0; j < appoggio.size(); ++j) {
                WTPart p2 = (WTPart)appoggio.get(j);
                if (p.getNumber().equals(p2.getNumber())) {
                    trovato = true;
                    break;
                }
            }

            if (!trovato) {
                appoggio.add(p);
            }
        }

        for(i = 0; i < appoggio.size(); ++i) {
            p = (WTPart)appoggio.get(i);
            WTPart latestVersion = getLatestVersionWTPart(p.getNumber(), view);
            if (latestVersion != null) {
                parts.add(latestVersion);
            }
        }

        return parts;
    }

    public static boolean isVersionedLinked(WTPart part, EPMDocument caddoc) throws WTException {
        Vector links = getEPMDocuments(part);

        for(int i = 0; i < links.size(); ++i) {
            EPMDocument epm = (EPMDocument)links.get(i);
            if (epm.getNumber().equals(caddoc.getNumber()) && epm.getVersionIdentifier().getValue().equals(caddoc.getVersionIdentifier().getValue())) {
                return true;
            }
        }

        return false;
    }

    public static Vector getDescribedByLink(WTPart part) throws WTException {
        Vector links = new Vector();
        QueryResult qr = WTPartHelper.service.getDescribedByDocuments(part, false);

        while(qr.hasMoreElements()) {
            Persistable pers = (Persistable)qr.nextElement();
            links.add(pers);
        }

        return links;
    }

    public static Vector getDrwDocuments(WTPart part) throws WTException {
        boolean VERBOSE = true;
        Vector drw_docs = new Vector();
        Vector cad_docs = getEPMDocuments(part);

        for(int i = 0; i < cad_docs.size(); ++i) {
            EPMDocument cad = (EPMDocument)cad_docs.get(i);
            QueryResult qr = EPMStructureHelper.service.navigateReferencedBy((EPMDocumentMaster)cad.getMaster(), (QuerySpec)null, true);

            while(qr.hasMoreElements()) {
                WTObject obj = (WTObject)qr.nextElement();
                if (VERBOSE) {
                    System.out.println("getDrwDocuments: Object in ReferenceLink " + obj.toString());
                }

                if (obj instanceof EPMDocument) {
                    if (VERBOSE) {
                        System.out.println("getDrwDocuments: EPMDocument " + obj.toString());
                    }

                    if (((EPMDocument)obj).getNumber().toUpperCase().endsWith(".DRW")) {
                        drw_docs.add((EPMDocument)obj);
                    }
                }
            }
        }

        return drw_docs;
    }

    public static Vector expandOneLevelTree(Vector distinta, int indice) {
        Vector bom = new Vector();

        for(int i = 0; i < distinta.size(); ++i) {
            bom.add(distinta.get(i));
        }

        Vector v = (Vector)distinta.get(indice);
        WTPart p = (WTPart)v.get(0);
        WTPartUsageLink link = (WTPartUsageLink)v.get(1);
        long livello = link.getLineNumber().getValue();
        int remover = indice + 1;

        while(remover != bom.size()) {
            bom.removeElementAt(remover);
        }

        for(remover = indice - 1; remover >= 0; --remover) {
            Vector v1 = (Vector)bom.get(remover);
            WTPartUsageLink link1 = (WTPartUsageLink)v1.get(1);
            if (link1.getLineNumber().getValue() != link.getLineNumber().getValue()) {
                break;
            }

            bom.removeElementAt(remover);
        }

        try {
            ConfigSpec cs = ConfigHelper.service.getConfigSpecFor(p);
            Vector figli = getBom(p, cs, livello);
            figli.remove(0);

            for(int i = 0; i < figli.size(); ++i) {
                bom.add((Vector)figli.get(i));
            }
        } catch (WTException var12) {
            var12.printStackTrace();
        }

        return bom;
    }

    public static Vector expandOneLevelBom(Vector distinta, ConfigSpec cs, int indice) {
        Vector bom = new Vector();

        for(int i = 0; i < distinta.size(); ++i) {
            bom.add(distinta.get(i));
        }

        Vector v = (Vector)distinta.get(indice);
        WTPart p = (WTPart)v.get(0);
        WTPartUsageLink link = (WTPartUsageLink)v.get(1);
        long livello = link.getLineNumber().getValue();
        Vector figli = getBom(p, cs, livello);
        figli.remove(0);

        for(int i = 0; i < figli.size(); ++i) {
            int insertAt = indice + 1 + i;
            if (insertAt > bom.size()) {
                bom.add((Vector)figli.get(i));
            } else {
                bom.insertElementAt((Vector)figli.get(i), insertAt);
            }
        }

        return bom;
    }

    public static Vector expandOneLevelTree(Vector distinta, ConfigSpec cs, int indice) {
        Vector bom = new Vector();

        for(int i = 0; i < distinta.size(); ++i) {
            bom.add(distinta.get(i));
        }

        Vector v = (Vector)distinta.get(indice);
        WTPart p = (WTPart)v.get(0);
        WTPartUsageLink link = (WTPartUsageLink)v.get(1);
        long livello = link.getLineNumber().getValue();
        int remover = indice + 1;

        while(remover != bom.size()) {
            bom.removeElementAt(remover);
        }

        Vector figli;
        for(remover = indice - 1; remover >= 0; --remover) {
            figli = (Vector)bom.get(remover);
            WTPartUsageLink link1 = (WTPartUsageLink)figli.get(1);
            if (link1.getLineNumber().getValue() != link.getLineNumber().getValue()) {
                break;
            }

            bom.removeElementAt(remover);
        }

        figli = getBom(p, cs, livello);
        figli.remove(0);

        for(int i = 0; i < figli.size(); ++i) {
            bom.add((Vector)figli.get(i));
        }

        return bom;
    }

    public static Vector getAllIterationWTParts(String number, String name) throws WTException, QueryException {
        Vector parts = new Vector();
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        QuerySpec queryspec = new QuerySpec();
        int versions = queryspec.appendClassList(WTPart.class, true);
        int masters = queryspec.appendClassList(WTPartMaster.class, false);
        int[] a = new int[]{masters, versions};
        int[] b = new int[]{masters, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id", WTPart.class, "masterReference.key.id"), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), b);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), b);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            WTPart part = (WTPart)((Persistable[])enumeration.nextElement())[versions];
            parts.add(part);
        }

        return parts;
    }

    public static Vector collapseBom(Vector bom, int index) {
        boolean trovato = false;
        Vector v = (Vector)bom.get(index);
        WTPartUsageLink link1 = (WTPartUsageLink)v.get(1);
        long l1 = link1.getLineNumber().getValue();

        for(int i = index + 1; i < bom.size(); ++i) {
            v = (Vector)bom.get(i);
            WTPartUsageLink link2 = (WTPartUsageLink)v.get(1);
            long l2 = link2.getLineNumber().getValue();
            if (l2 > l1) {
                bom.removeElementAt(i);
                --i;
            }
        }

        return bom;
    }

    public static WTPartMaster getPartMaster(String number) throws QueryException, WTException {
        WTPartMaster master = null;
        QuerySpec queryspec = new QuerySpec();
        int masters = queryspec.appendClassList(WTPartMaster.class, true);
        int[] a = new int[]{masters, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "=", number), a);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);

        for(Enumeration enumeration = queryresult.getEnumeration(); enumeration.hasMoreElements(); master = (WTPartMaster)((Persistable[])enumeration.nextElement())[masters]) {
        }

        return master;
    }

    public static WTPart getWTPartWithView(String number, String partversion, String view) throws WTException {
        WTPart p = null;
        Vector wtparts = getWTPartsWithView(number, view);

        for(int i = 0; i < wtparts.size(); ++i) {
            WTPart app = (WTPart)wtparts.get(i);
            String version = app.getVersionIdentifier().getValue();
            if (version.startsWith(partversion)) {
                p = app;
                break;
            }
        }

        return p;
    }

    public static Vector getPartMasters(String number, String name) throws QueryException, WTException {
        number = number.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        name = name.replace("*".toCharArray()[0], "%".toCharArray()[0]);
        Vector masters = new Vector();
        WTPartMaster master = null;
        QuerySpec queryspec = new QuerySpec();
        int m = queryspec.appendClassList(WTPartMaster.class, true);
        int[] a = new int[]{m, -1};
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "number", "LIKE", number), a);
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPartMaster.class, "name", "LIKE", name), a);
        QueryResult queryresult = PersistenceHelper.manager.find(queryspec);
        Enumeration enumeration = queryresult.getEnumeration();

        while(enumeration.hasMoreElements()) {
            master = (WTPartMaster)((Persistable[])enumeration.nextElement())[m];
            masters.add(master);
        }

        return masters;
    }

    public static boolean areAlreadyLinked(WTPart p, WTDocument d) {
        boolean b = false;
        QueryResult qr = null;

        try {
            qr = WTPartHelper.service.getDescribedByWTDocuments(p);

            while(qr.hasMoreElements()) {
                WTDocument temp = (WTDocument)qr.nextElement();
                if (temp.getNumber().equals(d.getNumber())) {
                    b = true;
                    break;
                }
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }

        return b;
    }
}
