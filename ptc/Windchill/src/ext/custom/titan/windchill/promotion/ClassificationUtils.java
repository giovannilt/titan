package ext.custom.titan.windchill.promotion;

import com.ptc.core.lwc.server.PersistableAdapter;
import com.ptc.core.meta.common.AttributeTypeIdentifier;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.windchill.csm.common.CsmConstants;
import wt.facade.classification.ClassificationFacade;
import wt.part.WTPart;
import wt.type.ClientTypedUtility;
import wt.util.WTException;
import wt.util.WTProperties;

import java.util.Locale;
import java.util.Set;

public class ClassificationUtils {
    public static final String DEFAULT_PATH_SEP = "/";
    private static WTProperties wtp;
    private static boolean VERBOSE;

    static {
        try {
            wtp = WTProperties.getLocalProperties();
            VERBOSE = wtp.getProperty("ext.com.adv.windchill.cl.verbose", false);
        } catch (Exception var1) {
            var1.printStackTrace();
        }

    }

    private ClassificationUtils() {
    }

    public static boolean isClassfied(WTPart part) throws WTException {


        TypeIdentifier part_tid = ClientTypedUtility.getTypeIdentifier(part);

        ClassificationFacade facadeInstance = ClassificationFacade.getInstance();

        /* retrieve the binding attribute(s) from part type */

        String bind_attr_name = null;
        Set<AttributeTypeIdentifier> atis = facadeInstance.getClassificationConstraintAttributes(part_tid);
        for (AttributeTypeIdentifier ati : atis) {
            bind_attr_name = ati.getAttributeName();
            /* Ignore other binding attributes for the example */
            break;
        }

        /* retrieve binding attribute value */

        String bind_attr_value = null;
        if (bind_attr_name != null) {
            System.out.println("The classification binding attribute on " + part + " is " + bind_attr_name);
            PersistableAdapter obj = new PersistableAdapter(part, null, Locale.US, null);
            obj.load(bind_attr_name);
            bind_attr_value = (String) obj.get(bind_attr_name);
            if (bind_attr_value != null) {
                System.out.println("The classification node internal name is " + bind_attr_value);
                String localizedName = facadeInstance.getLocalizedDisplayNameForClassificationNode(bind_attr_value,
                        CsmConstants.NAMESPACE, Locale.US);
                System.out.println("The localized name for " + bind_attr_name + " is " + localizedName);
                String localizedHierarchy = facadeInstance.getLocalizedHierarchyForClassificationNode(bind_attr_value,
                        CsmConstants.NAMESPACE, Locale.US);
                System.out.println("The classification path for " + bind_attr_name + " is " + localizedHierarchy);
            }
            return true;
        }


        return false;
    }
}
