package com.ptc.core.ui;

import wt.util.resource.*;

public final class componentRB_en_GB extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * 
    * <Need description here>
    * 
    **/
   @RBEntry("dd/MM/yyyy")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String STANDARD_DATE_DISPLAY_FORMAT = "STANDARD_DATE_DISPLAY_FORMAT";

   @RBEntry("dd/MM/yyyy  HH:mm")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String STANDARD_DATE_TIME_DISPLAY_FORMAT = "STANDARD_DATE_TIME_DISPLAY_FORMAT";

   @RBEntry("dd/MM/yyyy  HH:mm zzz")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT = "STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT";

   @RBEntry("d/M/yy")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String SHORT_DATE_DISPLAY_FORMAT = "SHORT_DATE_DISPLAY_FORMAT";

   @RBEntry("d/M/yy HH:mm")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String SHORT_DATE_TIME_DISPLAY_FORMAT = "SHORT_DATE_TIME_DISPLAY_FORMAT";

   @RBEntry("d/M/yy HH:mm zzz")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT = "SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT";

   @RBEntry("dd MMMM yyyy")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String LONG_DATE_DISPLAY_FORMAT = "LONG_DATE_DISPLAY_FORMAT";

   @RBEntry("dd MMMM yyyy HH:mm")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String LONG_DATE_TIME_DISPLAY_FORMAT = "LONG_DATE_TIME_DISPLAY_FORMAT";

   @RBEntry("dd MMMM yyyy HH:mm zzz")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String LONG_DATE_TIME_ZONE_DISPLAY_FORMAT = "LONG_DATE_TIME_ZONE_DISPLAY_FORMAT";

   /**
    * #############################################################
    * !!!! IMPORTANT: The following format and position entries need to be in sync for the calendar widget to work correctly
    * DATE_ENTRY_FORMAT
    * CALENDAR_PARSE_DATE
    * DAY_POSITION
    * MONTH_POSITION
    * YEAR_POSITION
    * DATE_ERROR
    * ##############################################################
    **/
   @RBEntry("dd/MM/yyyy")
   @RBComment("This serves as an instruction to the user for date entry on the UI. It needs to be fully translated (i.e. rearranging the order of MM,DD,YYYY and actual translation of MM,DD,YYYY themselves). - Needs to be in sync with the CALENDAR_PARSE_DATE, DAY_POSITION, MONTH_POSITION and YEAR_POSITION")
   public static final String DATE_ENTRY_FORMAT = "DATE_ENTRY_FORMAT";

   @RBEntry("dd/MM/yyyy")
   @RBPseudo(false)
   @RBComment("Used for calendar JavaScript only. Do not translate the date characters (M D Y), but modify the date syntax according to your locale-specific date syntax. This format needs to be in sync with DATE_ENTRY_FORMAT, DAY_POSITION, MONTH_POSITION and YEAR_POSITION.")
   public static final String CALENDAR_PARSE_DATE = "CALENDAR_PARSE_DATE";

   /**
    * These POSITION entries are used for the calendar widget javascript generation
    * 
    * One of the keys DAY_POSITION, MONTH_POSITION, YEAR_POSITION must be 1, one must be 2, and one must be 3.
    **/
   @RBEntry("1")
   @RBPseudo(false)
   @RBComment("The position of the day field in a date. May be 1, 2, or 3. (For ex in french, DAY_POSITION is 1 and in English it is 2). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE, MONTH_POSITION and YEAR_POSITION")
   public static final String DAY_POSITION = "DAY_POSITION";

   @RBEntry("2")
   @RBPseudo(false)
   @RBComment("The position of the month field in a date. May be 1, 2, or 3. (For ex in french, MONTH_POSITION is 2 and in English it is 1). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and YEAR_POSITION")
   public static final String MONTH_POSITION = "MONTH_POSITION";

   @RBEntry("3")
   @RBPseudo(false)
   @RBComment("The position of the year field in a date. May be 1, 2, or 3. (For ex in japanese, YEAR_POSITION is 1 and in English it is 3). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and MONTH_POSITION")
   public static final String YEAR_POSITION = "YEAR_POSITION";

   @RBEntry("The date is either not in the required format dd/MM/yyyy or it is out of range.")
   @RBComment("Needs to be in sync with the CALENDAR_PARSE_DATE and the DATE_ENTRY_FORMAT")
   public static final String DATE_ERROR = "DATE_ERROR";


}
