package com.ptc.core.ui;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBPseudo;
import wt.util.resource.RBPseudoTrans;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;
@RBUUID("com.ptc.core.ui.componentRB")
public final class componentRB_it extends WTListResourceBundle {
    /**
     * /* bcwti
     *
     * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
     *
     * This software is the confidential and proprietary information of PTC and is subject to the terms of a software
     * license agreement. You shall not disclose such confidential information and shall use it only in accordance with
     * the terms of the license agreement.
     *
     * ecwti
     *
     * <Need description here>
     *
     **/
    @RBEntry("Classificazioni")
    @RBComment("Classifications table header.")
    public static final String Classifications = "CLASSIFICATIONS";
    @RBEntry("Numero")
    @RBComment("Table column label for \"Number\".")
    public static final String NUMBER = "NUMBER";
    @RBEntry("Predecessore")
    @RBComment("Table column label for \"Predecessor\" .")
    public static final String PREDECESSOR = "PREDECESSOR";
    @RBEntry("Autorizzazione")
    @RBComment("Table column label for \"Authorization\" .")
    public static final String AUTHORIZATION = "AUTHORIZATION";
    @RBEntry("Data rilascio")
    @RBComment("Table column label for \"Released On\" .")
    public static final String RELEASED_ON = "RELEASED_ON";
    @RBEntry("Promosso il")
    @RBComment("Table column label for \"Promoted On\" .")
    public static final String PROMOTED_ON = "PROMOTED_ON";
    @RBEntry("Responsabile promozione")
    @RBComment("Table column label for \"Promoted On\" .")
    public static final String PROMOTED_BY = "PROMOTED_BY";
    @RBEntry("Proprietario")
    @RBComment("Table column label for \"Owner\".")
    public static final String OWNER = "OWNER";
    @RBEntry("Autore modifiche")
    @RBComment("Table column label for \"Modified By\" (2/9/2006 Terminology CFT decision)")
    public static final String UPDATED_BY = "UPDATED_BY";
    @RBEntry("Commenti")
    @RBComment("Table column label for \"Comments\" .")
    public static final String COMMENTS = "COMMENTS";
    @RBEntry("Tipo di effettività")
    @RBComment("Table column label for \"Effectivity Type\" .")
    public static final String EFFECTIVITY_TYPE = "EFFECTIVITY_TYPE";
    @RBEntry("Contesto di effettività")
    @RBComment("Table column label for \"Effectivity Context\" .")
    public static final String EFFECTIVITY_CONTEXT = "EFFECTIVITY_CONTEXT";
    @RBEntry("Qualificatore di effettività")
    @RBComment("Table column label for \"Effectivity Qualifier\" .")
    public static final String EFFECTIVITY_QUALIFIER = "EFFECTIVITY_QUALIFIER";
    @RBEntry("Intervallo di effettività")
    @RBComment("Table column label for \"Effectivity Range\" .")
    public static final String EFFECTIVITY_RANGE = "EFFECTIVITY_RANGE";
    @RBEntry("ID organizzazione")
    @RBComment("Table column label for \"Organization ID\" .")
    public static final String ORGANIZATION_ID = "ORGANIZATION_ID";
    @RBEntry("Direzione")
    @RBComment("Table column label for \"Direction\" .")
    public static final String DIRECTION = "DIRECTION";
    @RBEntry("Salvato da")
    @RBComment("Table column label for \"Saved By\".")
    public static final String SAVED_BY = "SAVED_BY";
    @RBEntry("Ultimo salvataggio")
    @RBComment("Table column label for \"Saved\".")
    public static final String SAVED_ON = "SAVED_ON";
    @RBEntry("Numero salvataggio")
    @RBComment("Table column label for \"Save-As Number\".")
    public static final String SAVE_AS_NUMBER = "SAVE_AS_NUMBER";
    @RBEntry("Versione salvataggio")
    @RBComment("Table column label for \"Save-As Version\".")
    public static final String SAVE_AS_VERSION = "SAVE_AS_VERSION";
    @RBEntry("Iterazione salvataggio")
    @RBComment("Table column label for \"Save-As Iteration\".")
    public static final String SAVE_AS_ITERATION = "SAVE_AS_ITERATION";
    @RBEntry("{0} salvataggio")
    @RBComment("Table column label for \"Save-As Organization ID\" ")
    public static final String SAVE_AS_ORGANIZATION = "SAVE_AS_ORGANIZATION";
    @RBEntry("Nome file")
    @RBComment("Table column label for \"File name\".")
    public static final String FILE_NAME = "FILE_NAME";
    @RBEntry("Dimensione")
    @RBComment("Table column label for \"File size\".")
    public static final String FILE_SIZE = "FILE_SIZE";
    @RBEntry("Iterazione")
    @RBComment("Table column label for \"Iteration\".")
    public static final String ITERATION = "ITERATION";
    @RBEntry("Richieste di promozione")
    @RBComment("Table column label for \"Promotion Requests\".")
    public static final String NOTE = "NOTE";
    @RBEntry("Data ultima modifica")
    @RBComment("The last modification date/timestep column (per Terminology CFT 4/15/2005)")
    public static final String LAST_UPDATED = "LAST_UPDATED";
    @RBEntry("Nome")
    @RBComment("Table column label for \"Name\" for tables against interfaces where Name is not a common attribute.")
    public static final String NAME = "NAME";
    @RBEntry("Azioni")
    @RBComment("Table column label for the standard \"Actions\" column")
    public static final String ACTIONS = "ACTIONS";
    @RBEntry("Apri contenuto associato")
    @RBComment("Table column label for the standard \"Open Associated Content Action\" column")
    public static final String OPEN_FILE_ACTION = "OPEN_FILE_ACTION";
    @RBEntry("Data ultima modifica")
    @RBComment("The last modification date/timestep column")
    public static final String LAST_MODIFIED = "LAST_MODIFIED";
    @RBEntry("Versione")
    @RBComment("The Version (example A.3) of an item")
    public static final String VERSION = "VERSION";
    @RBEntry("Revisione")
    @RBComment("The revision of an item (example A)")
    public static final String REVISION = "REVISION";
    @RBEntry("Attributi")
    @RBComment("Title of the table for setting object attributes in object creation and editing wizards")
    public static final String ATTRIBUTES_TABLE_HEADER = "ATTRIBUTES_TABLE_HEADER";
    @RBEntry("*")
    @RBComment("Header for the column containing required/nonrequired info in the table for setting object attributes in object creation and editing wizards")
    public static final String REQUIRED_COL_HEADER = "REQUIRED_COL_HEADER";
    @RBEntry("Nome")
    @RBComment("Header for the attribute name column in the table for setting object attributes in object creation and editing wizards")
    public static final String NAME_COL_HEADER = "NAME_COL_HEADER";
    @RBEntry("Valore")
    @RBComment("Header for the attribute value column in the table for setting object attributes in object creation and editing wizards")
    public static final String VALUE_COL_HEADER = "VALUE_COL_HEADER";
    @RBEntry("Stato del ciclo di vita")
    @RBComment("The lifecycle state of an item (example \"Released\")")
    public static final String STATE = "STATE";
    @RBEntry("Stato del ciclo di vita:")
    @RBComment("The lifecycle state, as shown on the item ID line of the info page, with colon.")
    public static final String STATE_LABEL_WITH_COLON = "STATE_LABEL_WITH_COLON";
    @RBEntry("Descrizione")
    @RBComment("The description of the object")
    public static final String DESCRIPTION = "DESCRIPTION";
    @RBEntry("Miniatura")
    @RBComment("The thumbnail icon of the object")
    public static final String THUMBNAIL = "THUMBNAIL";
    @RBEntry("Autore")
    @RBComment("The user who created the object.")
    public static final String CREATOR = "CREATOR";
    @RBEntry("Data creazione")
    @RBComment("The date on which the object was created (Per Terminology CFT 05 13 2005  SPR 1321929 01)")
    public static final String CREATEDON = "CREATEDON";
    @RBEntry("Seleziona automaticamente cartella {0}")
    @RBComment("Used as the label for the auto select radio button on the location picker in create clients.")
    public static final String LOCATION_AUTO_SELECT = "LOCATION_AUTO_SELECT";
    @RBEntry("Seleziona cartella")
    @RBComment("Used as the label for the override radio button on the location picker in create clients.")
    public static final String LOCATION_MANUAL_SELECT = "LOCATION_MANUAL_SELECT";
    @RBEntry("(generato)")
    @RBComment("Used to indicate that the value for Number is server-generated.")
    public static final String NUMBER_GENERATED_DISPLAY_STRING = "NUMBER_GENERATED_DISPLAY_STRING";
    @RBEntry("Tipo:")
    @RBComment("Label for type picker in object creation wizards")
    public static final String TYPE_PICKER_LABEL = "TYPE_PICKER_LABEL";
    @RBEntry("In stato di Check-In")
    @RBComment("The text associated with the Status attribute when an object is checked in")
    public static final String CHECKED_IN_STATUS = "CHECKED_IN";
    @RBEntry("Sottoposto a Check-Out")
    @RBComment("The text associated with the Status attribute when an object is checked out by me")
    public static final String WIP_CHECKED_OUT_BY_ME = "WIP_CHECKED_OUT_BY_ME";
    @RBEntry("In stato di Check-Out {0}")
    @RBComment("The text associated with the Status attribute when an object is checked out by someone else")
    public static final String WIP_CHECKED_OUT_BY_OTHER = "WIP_CHECKED_OUT_BY_OTHER";
    @RBEntry("Posizione")
    @RBComment("Used as the label for the location picker attribute in create clients.")
    public static final String FOLDER_ID_LABEL = "FOLDER_ID";
    @RBEntry("Programma")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PROGRAM_CONTAINER_NAME_LABEL = "PROGRAM_CONTAINER_NAME_LABEL";
    @RBEntry("Prodotto")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PRODUCT_CONTAINER_NAME_LABEL = "PRODUCT_CONTAINER_NAME_LABEL";
    @RBEntry("Libreria")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String LIBRARY_CONTAINER_NAME_LABEL = "LIBRARY_CONTAINER_NAME_LABEL";
    @RBEntry("Qualità")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String QUALITY_CONTAINER_NAME_LABEL = "QUALITY_CONTAINER_NAME_LABEL";
    @RBEntry("Progetto")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PROJECT_CONTAINER_NAME_LABEL = "PROJECT_CONTAINER_NAME_LABEL";
    @RBEntry("Organizzazione")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String ORG_CONTAINER_NAME_LABEL = "ORG_CONTAINER_NAME_LABEL";
    @RBEntry("Sito")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String SITE_CONTAINER_NAME_LABEL = "SITE_CONTAINER_NAME_LABEL";
    @RBEntry("Contesto contenitore")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String GENERIC_CONTAINER_NAME_LABEL = "GENERIC_CONTAINER_NAME_LABEL";
    @RBEntry("Data consegna")
    @RBComment("Table column label \"Need Date\"")
    public static final String NEED_DATE = "NEED_DATE";
    @RBEntry("Assegnatario")
    @RBComment("Label for the workflow assignee role")
    public static final String ROLE_ASSIGNEE = "ROLE_ASSIGNEE";
    @RBEntry("Esaminatore")
    @RBComment("Label for the workflow reviewer role")
    public static final String ROLE_REVIEWER = "ROLE_REVIEWER";
    @RBEntry("Tipo")
    @RBComment("Table column label \" \"")
    public static final String ITEM_TYPE = "ITEM_TYPE";
    /**
     * Below entries are for Edit component preferences, *NOTE* please preserve the html tags in them.
     **/
    @RBEntry("Visualizzazione pulsante Salva nella finestra Modifica")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_DISPLAY = "ALLOW_SAVE_WITHOUT_CHECKIN_DISPLAY";
    @RBEntry("Determina se il pulsante Salva è visualizzato nelle finestre di modifica per gli oggetti sottoposti a Check-Out")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_DESC = "ALLOW_SAVE_WITHOUT_CHECKIN_DESC";
    @RBEntry("È possibile che un oggetto non sia pronto per il Check-In ma che si desideri comunque salvare le modifiche apportate fino a un dato momento all'oggetto in Check-Out. L'impostazione di default per questa preferenza (<b>Yes</b>) è di visualizzare il pulsante <b>Salva</b> nelle finestre di modifica. Scegliere <b>No</b> per non visualizzare il pulsante <b>Salva</b>.")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_LONG_DESC = "ALLOW_SAVE_WITHOUT_CHECKIN_LONG_DESC";
    @RBEntry("Messaggio di avvertenza \"Oggetto in stato di Check-Out\"")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_DISPLAY = "EDIT_ACTION_CANCEL_BEHAVIOR_DISPLAY";
    @RBEntry("Determina se, quando l'utente fa clic su Annulla nelle finestre di modifica, viene visualizzato un avviso che l'oggetto è ancora sottoposto a Check-Out.")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_DESC = "EDIT_ACTION_CANCEL_BEHAVIOR_DESC";
    @RBEntry("Scegliere Yes se si desidera che, quando l'utente fa clic su Annulla in una finestra di modifica, venga visualizzato un messaggio di avviso per informarlo che un oggetto è ancora sottoposto a Check-Out. Scegliere No se non si desidera che il messaggio venga visualizzato. Per default, il messaggio viene visualizzato.")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_LONG_DESC = "EDIT_ACTION_CANCEL_BEHAVIOR_LONG_DESC";
    @RBEntry("Azione Check-Out e modifica")
    public static final String EDIT_ACTION_ON_CHECKED_IN_DISPLAY = "EDIT_ACTION_ON_CHECKED_IN_DISPLAY";
    @RBEntry("Determina se l'azione Check-Out e modifica è visualizzata per gli oggetti sottoposti a Check-In.")
    public static final String EDIT_ACTION_ON_CHECKED_IN_DESC = "EDIT_ACTION_ON_CHECKED_IN_DESC";
    @RBEntry("Scegliere Yes per consentire agli utenti di sottoporre un oggetto a Check-Out e modificarlo in un unico passo. Se si seleziona No, gli utenti devono sottoporre un oggetto a Check-Out prima che l'azione Modifica divenga disponibile. Per default, l'azione Chec-Out e modifica è attiva.")
    public static final String EDIT_ACTION_ON_CHECKED_IN_LONG_DESC = "EDIT_ACTION_ON_CHECKED_IN_LONG_DESC";
    @RBEntry("Azione Sostituisci contenuto")
    public static final String REPLACE_CONTENT_OFFERED_DISPLAY = "REPLACE_CONTENT_OFFERED_DISPLAY";
    @RBEntry("Determina se l'azione Sostituisci contenuto viene mostrata all'utente per gli oggetti sottoposti a Check-In.")
    public static final String REPLACE_CONTENT_OFFERED_DESC = "REPLACE_CONTENT_OFFERED_DESC";
    @RBEntry("Selezionare Yes per mostrare l'azione Sostituisci contenuto per gli oggetti sottoposti a Check-In. Per default, l'azione è visualizzata.")
    public static final String REPLACE_CONTENT_OFFERED_LONG_DESC = "REPLACE_CONTENT_OFFERED_LONG_DESC";
    @RBEntry("Propaga da oggetto")
    public static final String PROPAGATION_LABEL = "PROPAGATION_LABEL";
    @RBEntry("Altro")
    @RBComment("More hyperlink for TextDisplayComponent")
    public static final String MORE_LABEL = "1";
    @RBEntry("Sì")
    @RBComment("Label for the Boolean value \"True\"")
    public static final String BOOLEAN_TRUE = "BOOLEAN_TRUE";
    @RBEntry("No")
    @RBComment("Label for the Boolean value \"False\"")
    public static final String BOOLEAN_FALSE = "BOOLEAN_FALSE";
    @RBEntry("Non definito")
    @RBComment("Label for the value \"Undefined\"")
    public static final String UNSET = "UNSET";
    @RBEntry("dd/MM/yyyy")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_DISPLAY_FORMAT = "STANDARD_DATE_DISPLAY_FORMAT";
    @RBEntry("dd/MM/yyyy, HH.mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_TIME_DISPLAY_FORMAT = "STANDARD_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("dd/MM/yyyy, HH.mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT = "STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("d/M/yy")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_DISPLAY_FORMAT = "SHORT_DATE_DISPLAY_FORMAT";
    @RBEntry("d/M/yy, HH.mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_TIME_DISPLAY_FORMAT = "SHORT_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("d/M/yy, HH.mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT = "SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("dd/MMM/yyyy")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_DISPLAY_FORMAT = "LONG_DATE_DISPLAY_FORMAT";
    @RBEntry("dd/MMM/yyyy, HH.mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_TIME_DISPLAY_FORMAT = "LONG_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("dd/MMM/yyyy, HH.mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_TIME_ZONE_DISPLAY_FORMAT = "LONG_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("24")
    @RBPseudo(false)
    @RBComment("Indicates whether hours are shown as 1-12 with am and pm strings or 1-24, in the input mode. Must be 12 or 24.")
    public static final String HOUR_FORMAT = "HOUR_FORMAT";
    @RBEntry("{0} AM")
    @RBComment("If the time display is based on 12 hours, show \"7 AM\" for example.  The order in which the hour and the AM/PM label is displayed depends on locale (for example, Korean language will be \"AM 7\" (SPR 2155026)).")
    public static final String AM_STRING = "AM_STRING";
    @RBEntry("{0} PM")
    @RBComment("If the time display is based on 12 hours, show \"7 PM\" for example.  The order in which the hour and the AM/PM label is displayed depends on locale. (for example, Korean language will be \"PM 7\" (SPR 2155026)).")
    public static final String PM_STRING = "PM_STRING";
    /**
     * ############################################################# !!!! IMPORTANT: The following format and position
     * entries need to be in sync for the calendar widget to work correctly DATE_ENTRY_FORMAT CALENDAR_PARSE_DATE
     * DAY_POSITION MONTH_POSITION YEAR_POSITION DATE_ERROR
     * ##############################################################
     **/
    @RBEntry("gg/mm/aaaa")
    @RBComment("This serves as an instruction to the user for date entry on the UI. It needs to be fully translated (i.e. rearranging the order of MM,DD,YYYY and actual translation of MM,DD,YYYY themselves). - Needs to be in sync with the CALENDAR_PARSE_DATE, DAY_POSITION, MONTH_POSITION and YEAR_POSITION")
    public static final String DATE_ENTRY_FORMAT = "DATE_ENTRY_FORMAT";
    @RBEntry("dd/MM/yyyy, HH.mm zzz")
    @RBPseudo(false)
    @RBComment("This is the date format used only when there is defined set of date values to choose from in the UI (i.e. legal value list) and the attribute is configured as date-and-time for input.  Do not translate the date characters (M d Y h m), but modify the date syntax according to locale-specific date syntax.  Needs to be kept in sync with DATE_ENTRY_FORMAT.")
    public static final String DATE_AND_TIME_ENTRY_FORMAT = "DATE_AND_TIME_ENTRY_FORMAT";
    @RBEntry("Formato di data non valido. È necessario immettere la data nel formato gg/mm/aaaa")
    @RBComment("This serves as an instruction to the user for date entry on the UI, the format yyyy-mm-dd should match the same value as DATE_ENTRY_FORMAT")
    public static final String INVALID_DATEENTRY = "INVALID_DATEENTRY";
    @RBEntry("dd/MM/yyyy")
    @RBPseudo(false)
    @RBComment("Used for calendar JavaScript only. Do not translate the date characters (M D Y), but modify the date syntax according to your locale-specific date syntax. This format needs to be in sync with DATE_ENTRY_FORMAT, DAY_POSITION, MONTH_POSITION and YEAR_POSITION.")
    public static final String CALENDAR_PARSE_DATE = "CALENDAR_PARSE_DATE";
    @RBEntry("d/m/Y")
    @RBPseudo(false)
    @RBComment("This is the date format used by the EXT date field.  Do not translate or change the case of the date characters (m d Y), but modify the date syntax according to locale-specific date syntax.")
    public static final String EXT_DATEENTRY_FORMAT = "EXT_DATEENTRY_FORMAT";
    /**
     * These POSITION entries are used for the calendar widget javascript generation
     *
     * One of the keys DAY_POSITION, MONTH_POSITION, YEAR_POSITION must be 1, one must be 2, and one must be 3.
     **/
    @RBEntry("1")
    @RBPseudo(false)
    @RBComment("The position of the day field in a date. May be 1, 2, or 3. (For ex in french, DAY_POSITION is 1 and in English it is 2). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE, MONTH_POSITION and YEAR_POSITION")
    public static final String DAY_POSITION = "DAY_POSITION";
    @RBEntry("2")
    @RBPseudo(false)
    @RBComment("The position of the month field in a date. May be 1, 2, or 3. (For ex in french, MONTH_POSITION is 2 and in English it is 1). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and YEAR_POSITION")
    public static final String MONTH_POSITION = "MONTH_POSITION";
    @RBEntry("3")
    @RBPseudo(false)
    @RBComment("The position of the year field in a date. May be 1, 2, or 3. (For ex in japanese, YEAR_POSITION is 1 and in English it is 3). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and MONTH_POSITION")
    public static final String YEAR_POSITION = "YEAR_POSITION";
    @RBEntry("La data non è nel formato richiesto (gg/mm/aaaa) o è fuori dall'intervallo.")
    @RBComment("Needs to be in sync with the CALENDAR_PARSE_DATE and the DATE_ENTRY_FORMAT")
    public static final String DATE_ERROR = "DATE_ERROR";
    @RBEntry("Calendario")
    public static final String CALENDAR_TITLE = "CALENDAR_TITLE";
    @RBEntry("Do")
    @RBPseudoTrans("S失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Sunday")
    public static final String WEEK_SUN = "WEEK_SUN";
    @RBEntry("Lu")
    @RBPseudoTrans("M失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Monday")
    public static final String WEEK_MON = "WEEK_MON";
    @RBEntry("Ma")
    @RBPseudoTrans("T失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Tuesday")
    public static final String WEEK_TUE = "WEEK_TUE";
    @RBEntry("Me")
    @RBPseudoTrans("W失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Wednesday")
    public static final String WEEK_WED = "WEEK_WED";
    @RBEntry("Gi")
    @RBPseudoTrans("T失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Thursday")
    public static final String WEEK_THU = "WEEK_THU";
    @RBEntry("Ve")
    @RBPseudoTrans("F失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Friday")
    public static final String WEEK_FRI = "WEEK_FRI";
    @RBEntry("Sa")
    @RBPseudoTrans("S失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Saturday")
    public static final String WEEK_SAT = "WEEK_SAT";
    /**
     *
     * The month strings can either be the name of the month or the number(e.g Asian languages)
     *
     **/
    @RBEntry("gennaio")
    public static final String MON_JAN = "MON_JAN";
    @RBEntry("febbraio")
    public static final String MON_FEB = "MON_FEB";
    @RBEntry("marzo")
    public static final String MON_MAR = "MON_MAR";
    @RBEntry("aprile")
    public static final String MON_APR = "MON_APR";
    @RBEntry("maggio")
    public static final String MON_MAY = "MON_MAY";
    @RBEntry("giugno")
    public static final String MON_JUN = "MON_JUN";
    @RBEntry("luglio")
    public static final String MON_JUL = "MON_JUL";
    @RBEntry("agosto")
    public static final String MON_AUG = "MON_AUG";
    @RBEntry("settembre")
    public static final String MON_SEP = "MON_SEP";
    @RBEntry("ottobre")
    public static final String MON_OCT = "MON_OCT";
    @RBEntry("novembre")
    public static final String MON_NOV = "MON_NOV";
    @RBEntry("dicembre")
    public static final String MON_DEC = "MON_DEC";
    @RBEntry("Autore")
    @RBComment("Label used for the column header or label for the full name of the creator of an object.")
    public static final String CREATOR_FULL_NAME = "CREATOR_FULL_NAME";
    @RBEntry("Invia per e-mail all'autore")
    @RBComment("Label for the Mail to Creator column.")
    public static final String CREATOR_MAILTO = "CREATOR_MAILTO";
    @RBEntry("Contesto")
    @RBComment("Label used for the column header or label for the container of an object.")
    public static final String CONTAINER_NAME = "CONTAINER_NAME";
    @RBEntry("Nome rappresentazione")
    @RBComment("Table column label for the name of the representation associated with a published content link.")
    public static final String REPRESENTATION_NAME = "REPRESENTATION_NAME";
    @RBEntry("Tipo di link")
    @RBComment("Table column label for the type of the published content links.")
    public static final String PUBLISHED_CONTENT_LINK_TYPE = "PUBLISHED_CONTENT_LINK_TYPE";
    @RBEntry("Mantieni in stato di Check-Out dopo il Check-In")
    @RBComment("Label for the Keep Checked Out checkbox on the Set Attributes step of object creation wizards")
    public static final String KEEP_CHECKED_OUT_CHECKBOX_LABEL = "KEEP_CHECKED_OUT_CHECKBOX_LABEL";
    @RBEntry(" (Default)")
    @RBComment("Used to append to selected value of a combobox.")
    public static final String DEFAULT_DISPLAY_STRING = "DEFAULT_DISPLAY_STRING";
    @RBEntry(" (Corrente)")
    @RBComment("Used to append to current selected value of a combobox.")
    public static final String CURRENT_DISPLAY_STRING = "CURRENT_DISPLAY_STRING";
    @RBEntry("Definisci oggetto")
    @RBComment("Define Object label")
    public static final String PRIVATE_CONSTANT_0 = "object.defineItemWizStep.description";
    @RBEntry("Definisci oggetto")
    public static final String PRIVATE_CONSTANT_1 = "object.defineItemWizStep.tooltip";
    @RBEntry("Definisci oggetto")
    public static final String PRIVATE_CONSTANT_2 = "object.defineItemWizStep.title";
    @RBEntry("Imposta attributi")
    @RBComment("Set Attributes label")
    public static final String PRIVATE_CONSTANT_3 = "object.defineItemAttributesWizStep.description";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_4 = "object.defineItemAttributesWizStep.tooltip";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_5 = "object.defineItemAttributesWizStep.title";
    @RBEntry("Imposta attributi")
    @RBComment("Set Attributes label")
    public static final String PRIVATE_CONSTANT_6 = "object.setAttributesWizStep.description";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_7 = "object.setAttributesWizStep.tooltip";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_8 = "object.setAttributesWizStep.title";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_9 = "object.editAttributesWizStep.description";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_10 = "object.editAttributesWizStep.tooltip";
    @RBEntry("Imposta attributi")
    public static final String PRIVATE_CONSTANT_11 = "object.editAttributesWizStep.title";
    @RBEntry("Imposta attributi condivisi")
    @RBComment("Set Shared Attributes")
    public static final String PRIVATE_CONSTANT_12 = "object.typeRefWizStep.description";
    @RBEntry("Imposta attributi condivisi")
    public static final String PRIVATE_CONSTANT_13 = "object.typeRefWizStep.tooltip";
    @RBEntry("Imposta attributi condivisi")
    public static final String PRIVATE_CONSTANT_14 = "object.typeRefWizStep.title";
    @RBEntry("Vai a più recente")
    @RBComment("Used as the description for the go to latest hyperlink.")
    public static final String GO_TO_LATEST = "GO_TO_LATEST";
    @RBEntry("(Vai a versione originale)")
    @RBComment("Used as the description for the go to original hyperlink.")
    public static final String GO_TO_ORIGINAL = "GO_TO_ORIGINAL";
    @RBEntry("(Vai a Copia in modifica)")
    @RBComment("Used as the description for the go to working hyperlink.")
    public static final String GO_TO_WORKING = "GO_TO_WORKING";
    @RBEntry("(Non assegnabile)")
    @RBComment("Used to display as value for read only attributes with null values in create and edit UIs.")
    public static final String UNASSIGNABLE_DISPLAY_STRING = "UNASSIGNABLE_DISPLAY_STRING";
    @RBEntry("(Nascosto)")
    @RBComment("Used to display as value for hidden value constrained attributes in all modes (create, edit and view modes).")
    public static final String HIDDEN_DISPLAY_STRING = "HIDDEN_DISPLAY_STRING";
    @RBEntry("Messaggio")
    @RBComment("Default title the message dialog title as per UI standard: http://ui/standards/standards.php?id=28&disp=all")
    public static final String MESSAGE_DIALOG_TITLE = "MESSAGE_DIALOG_TITLE";
    @RBEntry("Trova organizzazione")
    @RBComment("Label for org picker title launched using OrgIDDataUtility")
    public static final String ORG_PICKER_LABEL = "ORG_PICKER_LABEL";
    @RBEntry("Host")
    @RBComment("Table column label \"Host\"")
    public static final String PROJECT_HOST = "PROJECT_HOST";
    @RBEntry("Frame contenuto pagina")
    @RBComment("Setting for the html attribute of title on the iframe for template processing main page content. This is read to users using screen readers.")
    public static final String TP_IFRAME_TITLE = "TP_IFRAME_TITLE";
    @RBEntry("Usa default")
    @RBComment("Label for the use default button that is sometimes displayed on attributes in wizards. (Depending on a pref) when pressed the default value for the attribute is used in the field.")
    public static final String USE_DEFAULT = "USE_DEFAULT";
    @RBEntry("Inizio")
    @RBComment("Default label for the \"Start\" component in a date range.")
    public static final String DATE_RANGE_START = "DATE_RANGE_START";
    @RBEntry("Fine")
    @RBComment("Default label for the \"End\" component in a date range.")
    public static final String DATE_RANGE_END = "DATE_RANGE_END";
    @RBEntry("La visualizzazione dell'attributo RTF non è supportata nella vista tabella dell'oggetto")
    @RBComment("Tooltip value to show for attribute that is rich text when rich text is not being shown.")
    public static final String RICH_TEXT_HIDDEN_TOOLTIP_MSG = "RICH_TEXT_HIDDEN_TOOLTIP_MSG";
    @RBEntry("Errore durante il rendering")
    @RBComment("Error message that gets displayed for an attribute value when an error occurs rendering the attribute.")
    public static final String RENDERING_ATTR_VALUE_ERROR_MESSAGE = "RENDERING_ATTR_VALUE_ERROR_MESSAGE";
    @RBEntry("Formato non valido: {0}")
    @RBComment("Error message that gets displayed for an attribute value when an the date format string is invalid.")
    public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
    @RBEntry("Aggiungi in base a nome")
    @RBComment("The label that appears inside the auto suggest panel for related entities for name.")
    public static final String ADD_BY_NAME = "ADD_BY_NAME";
    @RBEntry("Aggiungi in base a numero")
    @RBComment("The label that appears inside the auto suggest panel for related entities for number.")
    public static final String ADD_BY_NUMBER = "ADD_BY_NUMBER";
    @RBEntry("Etichetta URL")
    @RBComment("The label for the URL Label input field for a Hyperlink attribute.")
    public static final String HYPERLINK_LABEL = "HYPERLINK_LABEL";
    @RBEntry("URL")
    @RBComment("The label for the URL input field for a Hyperlink attribute.")
    public static final String HYPERLINK_URL = "HYPERLINK_URL";
    @RBEntry("Le modifiche apportate alla scheda Pubblico vengono applicate alle home page degli utenti del sito tranne per quegli utenti che hanno già modificato le loro home page.")
    @RBComment("Message thats displayed in an inline message on the site admins public homepage tab")
    public static final String SITE_ADMIN_PUBLIC_HOMEPAGE_TAB_INLINE_HELP = "SITE_ADMIN_PUBLIC_HOMEPAGE_TAB_INLINE_HELP";
    // added for defect D-20061
    @RBEntry("Seleziona classificazione")
    @RBComment("Title for Classification Picker")
    public static final String CLASSIFICATION_PICKER_TITLE = "CLASSIFICATION_PICKER_TITLE";
    @RBEntry("Seleziona")
    @RBComment("Title for Structured Enumeration Picker")
    public static final String STRUCTURED_ENUMERATION_PICKER_TITLE = "STRUCTURED_ENUMERATION_PICKER_TITLE";
    @RBEntry("-- Selezionare --")
    @RBComment("Inline Help text that appears as the top item in a required combobox.")
    public static final String REQUIRED_ENTRY_COMBOBOX_INLINE_HELP = "REQUIRED_ENTRY_COMBOBOX_INLINE_HELP";
    // Tooltips to be displayed on the "traffic light" multi-state graphical attribute widget
    @RBEntry("Rosso")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the red/stopped state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_RED_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_RED_STATE";
    @RBEntry("Giallo")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the yellow/caution state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_YELLOW_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_YELLOW_STATE";
    @RBEntry("Verde")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the green/go state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_GREEN_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_GREEN_STATE";
    // end of "traffic light" tooltips
    @RBEntry("{0}: {1} in [{2} .. {3}]")
    @RBComment("Tooltip to be displayed on the progress bar graphical representation. For example 75%; 7.5 in [0 .. 10]")
    public static final String PROGRESS_BAR_TOOLTIP = "PROGRESS_BAR_TOOLTIP";
    @RBEntry("(sconosciuto)")
    @RBComment("Short message displayed in table cell to indicate progress bar cannot be displayed (detailed explanation in the tooltip)")
    public static final String PROGRESS_BAR_ERROR = "PROGRESS_BAR_ERROR";
    @RBEntry("Intervallo non valido: il parametro \"{0}\" è uguale al parametro \"{1}\"")
    @RBComment("Error message to be displayed in a progress bar tooltip when the range is empty. {0} will be 'begin' and {1} will be 'end'")
    public static final String PROGRESS_BAR_EMPTY_RANGE = "PROGRESS_BAR_EMPTY_RANGE";
    @RBEntry("Errore: il valore non è un numero")
    @RBComment("Error message to be displayed in a progress bar tooltip when the percent complete computation fails to produce a number")
    public static final String PROGRESS_BAR_NAN_ERROR = "PROGRESS_BAR_NAN_ERROR";
    @RBEntry("Errore: valore troppo grande")
    @RBComment("Error message to be displayed in a progress bar tooltip when the percent complete result is 'infinite'")
    public static final String PROGRESS_BAR_INFINITY_ERROR = "PROGRESS_BAR_INFINITY_ERROR";
    @RBEntry("[{0}% .. {1}%)")
    @RBComment("Label used to identify progress bar groups, e.g. '[10% .. 20%)', '[20% .. 30%)'.  N.B. These are half-open intervals. ")
    public static final String PROGRESS_BAR_GROUP_LABEL = "PROGRESS_BAR_GROUP_LABEL";
    @RBEntry("Nessuno")
    @RBComment("Label used to identify the table group with no progress bars")
    public static final String PROGRESS_BAR_NONE_GROUP_LABEL = "PROGRESS_BAR_NONE_GROUP_LABEL";
    @RBEntry("{0}%")
    @RBComment("Tooltip to be displayed on the percent complete graphical representation")
    public static final String PERCENT_COMPLETE_TOOLTIP = "PERCENT_COMPLETE_TOOLTIP";
    @RBEntry("Il valore della percentuale presenta unità non previste: {0}")
    @RBComment("Error message displayed when a percent complete value has unexpected units")
    public static final String PERCENT_COMPLETE_BAD_UNITS = "PERCENT_COMPLETE_BAD_UNITS";
    @RBEntry("Impossibile convertire il valore in un numero: {0}")
    @RBComment("Error message displayed when a graphical attribute configuration value is non-numeric")
    public static final String GA_UNABLE_TO_CONVERT_VALUE_TO_NUMBER = "GA_UNABLE_TO_CONVERT_VALUE_TO_NUMBER";
    @RBEntry("Impossibile convertire il parametro \"{0}\" in un numero: {1}")
    @RBComment("Error message displayed when a graphical attribute configuration parameter is non-numeric")
    public static final String GA_UNABLE_TO_CONVERT_PARAMETER_TO_NUMBER = "GA_UNABLE_TO_CONVERT_PARAMETER_TO_NUMBER";
    @RBEntry("Valore non specificato")
    @RBComment("Error message displayed when a graphical attribute value is missing")
    public static final String GA_MISSING_VALUE = "GA_MISSING_VALUE";
    @RBEntry("Parametro \"{0}\" non definito")
    @RBComment("Error message displayed when a graphical attribute parameter is missing")
    public static final String GA_MISSING_PARAMETER = "GA_MISSING_PARAMETER";
    @RBEntry("Il valore non è un numero: {0}")
    @RBComment("Error message displayed when a graphical attribute value is not recognized as numeric")
    public static final String GA_UNRECOGNIZED_NUMERIC_VALUE = "GA_UNRECOGNIZED_NUMERIC_VALUE";
    @RBEntry("Il parametro \"{0}\" non è un numero: {1}")
    @RBComment("Error message displayed when a graphical attribute parameter is not recognized as numeric")
    public static final String GA_UNRECOGNIZED_NUMERIC_PARAMETER = "GA_UNRECOGNIZED_NUMERIC_PARAMETER";
    @RBEntry("Errore di conversione unità imprevisto: {0}")
    @RBComment("Error message displayed when an unexpected unit conversion error is encountered")
    public static final String GA_UNEXPECTED_UNIT_CONVERSION_ERROR = "GA_UNEXPECTED_UNIT_CONVERSION_ERROR";
    @RBEntry("Il valore \"{0}\" non include le unità previste: {1}")
    @RBComment("Error message displayed when a graphical attribute value incompatible or missing units")
    public static final String GA_BAD_VALUE_UNITS = "GA_BAD_VALUE_UNITS";
    @RBEntry("Contattare l'amministratore per assistenza")
    @RBComment("Error message displayed when any other graphical attribute errors are generated")
    public static final String GA_CONTACT_ADMIN = "GA_CONTACT_ADMIN";
    @RBEntry("??")
    @RBComment("Placeholder used in the tooltip string when values are unknown. For example '??%; 10 in [0 .. ??]'")
    public static final String GA_VALUE_UNKNOWN = "GA_VALUE_UNKNOWN";
    @RBEntry("Parametri non validi")
    @RBComment("Error message shown when the progress bar parameters are null or empty.")
    public static final String GA_INVALID_PARAMETERS = "GA_INVALID_PARAMETERS";

}
