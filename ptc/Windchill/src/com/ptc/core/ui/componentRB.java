package com.ptc.core.ui;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBPseudo;
import wt.util.resource.RBPseudoTrans;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;
@RBUUID("com.ptc.core.ui.componentRB")
public final class componentRB extends WTListResourceBundle {
    /**
     * /* bcwti
     *
     * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
     *
     * This software is the confidential and proprietary information of PTC and is subject to the terms of a software
     * license agreement. You shall not disclose such confidential information and shall use it only in accordance with
     * the terms of the license agreement.
     *
     * ecwti
     *
     * <Need description here>
     *
     **/
    @RBEntry("Classifications")
    @RBComment("Classifications table header.")
    public static final String Classifications = "CLASSIFICATIONS";
    @RBEntry("Number")
    @RBComment("Table column label for \"Number\".")
    public static final String NUMBER = "NUMBER";
    @RBEntry("Predecessor")
    @RBComment("Table column label for \"Predecessor\" .")
    public static final String PREDECESSOR = "PREDECESSOR";
    @RBEntry("Authorization")
    @RBComment("Table column label for \"Authorization\" .")
    public static final String AUTHORIZATION = "AUTHORIZATION";
    @RBEntry("Released On")
    @RBComment("Table column label for \"Released On\" .")
    public static final String RELEASED_ON = "RELEASED_ON";
    @RBEntry("Promoted On")
    @RBComment("Table column label for \"Promoted On\" .")
    public static final String PROMOTED_ON = "PROMOTED_ON";
    @RBEntry("Promoted By")
    @RBComment("Table column label for \"Promoted On\" .")
    public static final String PROMOTED_BY = "PROMOTED_BY";
    @RBEntry("Owner")
    @RBComment("Table column label for \"Owner\".")
    public static final String OWNER = "OWNER";
    @RBEntry("Modified By")
    @RBComment("Table column label for \"Modified By\" (2/9/2006 Terminology CFT decision)")
    public static final String UPDATED_BY = "UPDATED_BY";
    @RBEntry("Comments")
    @RBComment("Table column label for \"Comments\" .")
    public static final String COMMENTS = "COMMENTS";
    @RBEntry("Effectivity Type")
    @RBComment("Table column label for \"Effectivity Type\" .")
    public static final String EFFECTIVITY_TYPE = "EFFECTIVITY_TYPE";
    @RBEntry("Effectivity Context")
    @RBComment("Table column label for \"Effectivity Context\" .")
    public static final String EFFECTIVITY_CONTEXT = "EFFECTIVITY_CONTEXT";
    @RBEntry("Effectivity Qualifier")
    @RBComment("Table column label for \"Effectivity Qualifier\" .")
    public static final String EFFECTIVITY_QUALIFIER = "EFFECTIVITY_QUALIFIER";
    @RBEntry("Effectivity Range")
    @RBComment("Table column label for \"Effectivity Range\" .")
    public static final String EFFECTIVITY_RANGE = "EFFECTIVITY_RANGE";
    @RBEntry("Organization ID")
    @RBComment("Table column label for \"Organization ID\" .")
    public static final String ORGANIZATION_ID = "ORGANIZATION_ID";
    @RBEntry("Direction")
    @RBComment("Table column label for \"Direction\" .")
    public static final String DIRECTION = "DIRECTION";
    @RBEntry("Saved By")
    @RBComment("Table column label for \"Saved By\".")
    public static final String SAVED_BY = "SAVED_BY";
    @RBEntry("Saved")
    @RBComment("Table column label for \"Saved\".")
    public static final String SAVED_ON = "SAVED_ON";
    @RBEntry("Save-As Number")
    @RBComment("Table column label for \"Save-As Number\".")
    public static final String SAVE_AS_NUMBER = "SAVE_AS_NUMBER";
    @RBEntry("Save-As Version")
    @RBComment("Table column label for \"Save-As Version\".")
    public static final String SAVE_AS_VERSION = "SAVE_AS_VERSION";
    @RBEntry("Save-As Iteration")
    @RBComment("Table column label for \"Save-As Iteration\".")
    public static final String SAVE_AS_ITERATION = "SAVE_AS_ITERATION";
    @RBEntry("Save-As {0}")
    @RBComment("Table column label for \"Save-As Organization ID\" ")
    public static final String SAVE_AS_ORGANIZATION = "SAVE_AS_ORGANIZATION";
    @RBEntry("File Name")
    @RBComment("Table column label for \"File name\".")
    public static final String FILE_NAME = "FILE_NAME";
    @RBEntry("Size")
    @RBComment("Table column label for \"File size\".")
    public static final String FILE_SIZE = "FILE_SIZE";
    @RBEntry("Iteration")
    @RBComment("Table column label for \"Iteration\".")
    public static final String ITERATION = "ITERATION";
    @RBEntry("Promotion Requests")
    @RBComment("Table column label for \"Promotion Requests\".")
    public static final String NOTE = "NOTE";
    @RBEntry("Last Modified")
    @RBComment("The last modification date/timestep column (per Terminology CFT 4/15/2005)")
    public static final String LAST_UPDATED = "LAST_UPDATED";
    @RBEntry("Name")
    @RBComment("Table column label for \"Name\" for tables against interfaces where Name is not a common attribute.")
    public static final String NAME = "NAME";
    @RBEntry("Actions")
    @RBComment("Table column label for the standard \"Actions\" column")
    public static final String ACTIONS = "ACTIONS";
    @RBEntry("Open Associated Content")
    @RBComment("Table column label for the standard \"Open Associated Content Action\" column")
    public static final String OPEN_FILE_ACTION = "OPEN_FILE_ACTION";
    @RBEntry("Last Modified")
    @RBComment("The last modification date/timestep column")
    public static final String LAST_MODIFIED = "LAST_MODIFIED";
    @RBEntry("Version")
    @RBComment("The Version (example A.3) of an item")
    public static final String VERSION = "VERSION";
    @RBEntry("Revision")
    @RBComment("The revision of an item (example A)")
    public static final String REVISION = "REVISION";
    @RBEntry("Attributes")
    @RBComment("Title of the table for setting object attributes in object creation and editing wizards")
    public static final String ATTRIBUTES_TABLE_HEADER = "ATTRIBUTES_TABLE_HEADER";
    @RBEntry("*")
    @RBComment("Header for the column containing required/nonrequired info in the table for setting object attributes in object creation and editing wizards")
    public static final String REQUIRED_COL_HEADER = "REQUIRED_COL_HEADER";
    @RBEntry("Name")
    @RBComment("Header for the attribute name column in the table for setting object attributes in object creation and editing wizards")
    public static final String NAME_COL_HEADER = "NAME_COL_HEADER";
    @RBEntry("Value")
    @RBComment("Header for the attribute value column in the table for setting object attributes in object creation and editing wizards")
    public static final String VALUE_COL_HEADER = "VALUE_COL_HEADER";
    @RBEntry("State")
    @RBComment("The lifecycle state of an item (example \"Released\")")
    public static final String STATE = "STATE";
    @RBEntry("State:")
    @RBComment("The lifecycle state, as shown on the item ID line of the info page, with colon.")
    public static final String STATE_LABEL_WITH_COLON = "STATE_LABEL_WITH_COLON";
    @RBEntry("Description")
    @RBComment("The description of the object")
    public static final String DESCRIPTION = "DESCRIPTION";
    @RBEntry("Thumbnail")
    @RBComment("The thumbnail icon of the object")
    public static final String THUMBNAIL = "THUMBNAIL";
    @RBEntry("Created By")
    @RBComment("The user who created the object.")
    public static final String CREATOR = "CREATOR";
    @RBEntry("Created On")
    @RBComment("The date on which the object was created (Per Terminology CFT 05 13 2005  SPR 1321929 01)")
    public static final String CREATEDON = "CREATEDON";
    @RBEntry("Autoselect Folder {0}")
    @RBComment("Used as the label for the auto select radio button on the location picker in create clients.")
    public static final String LOCATION_AUTO_SELECT = "LOCATION_AUTO_SELECT";
    @RBEntry("Select Folder")
    @RBComment("Used as the label for the override radio button on the location picker in create clients.")
    public static final String LOCATION_MANUAL_SELECT = "LOCATION_MANUAL_SELECT";
    @RBEntry("(Generated)")
    @RBComment("Used to indicate that the value for Number is server-generated.")
    public static final String NUMBER_GENERATED_DISPLAY_STRING = "NUMBER_GENERATED_DISPLAY_STRING";
    @RBEntry("Type:")
    @RBComment("Label for type picker in object creation wizards")
    public static final String TYPE_PICKER_LABEL = "TYPE_PICKER_LABEL";
    @RBEntry("Checked In")
    @RBComment("The text associated with the Status attribute when an object is checked in")
    public static final String CHECKED_IN_STATUS = "CHECKED_IN";
    @RBEntry("Checked Out")
    @RBComment("The text associated with the Status attribute when an object is checked out by me")
    public static final String WIP_CHECKED_OUT_BY_ME = "WIP_CHECKED_OUT_BY_ME";
    @RBEntry("Checked Out {0}")
    @RBComment("The text associated with the Status attribute when an object is checked out by someone else")
    public static final String WIP_CHECKED_OUT_BY_OTHER = "WIP_CHECKED_OUT_BY_OTHER";
    @RBEntry("Location")
    @RBComment("Used as the label for the location picker attribute in create clients.")
    public static final String FOLDER_ID_LABEL = "FOLDER_ID";
    @RBEntry("Program")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PROGRAM_CONTAINER_NAME_LABEL = "PROGRAM_CONTAINER_NAME_LABEL";
    @RBEntry("Product")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PRODUCT_CONTAINER_NAME_LABEL = "PRODUCT_CONTAINER_NAME_LABEL";
    @RBEntry("Library")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String LIBRARY_CONTAINER_NAME_LABEL = "LIBRARY_CONTAINER_NAME_LABEL";
    @RBEntry("Quality")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String QUALITY_CONTAINER_NAME_LABEL = "QUALITY_CONTAINER_NAME_LABEL";
    @RBEntry("Project")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String PROJECT_CONTAINER_NAME_LABEL = "PROJECT_CONTAINER_NAME_LABEL";
    @RBEntry("Organization")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String ORG_CONTAINER_NAME_LABEL = "ORG_CONTAINER_NAME_LABEL";
    @RBEntry("Site")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String SITE_CONTAINER_NAME_LABEL = "SITE_CONTAINER_NAME_LABEL";
    @RBEntry("Container Context")
    @RBComment("Label used for read-only container name field on create wizard steps")
    public static final String GENERIC_CONTAINER_NAME_LABEL = "GENERIC_CONTAINER_NAME_LABEL";
    @RBEntry("Need Date")
    @RBComment("Table column label \"Need Date\"")
    public static final String NEED_DATE = "NEED_DATE";
    @RBEntry("Assignee")
    @RBComment("Label for the workflow assignee role")
    public static final String ROLE_ASSIGNEE = "ROLE_ASSIGNEE";
    @RBEntry("Reviewer")
    @RBComment("Label for the workflow reviewer role")
    public static final String ROLE_REVIEWER = "ROLE_REVIEWER";
    @RBEntry("Type")
    @RBComment("Table column label \" \"")
    public static final String ITEM_TYPE = "ITEM_TYPE";
    /**
     * Below entries are for Edit component preferences, *NOTE* please preserve the html tags in them.
     **/
    @RBEntry("Save Button Display on Edit Window")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_DISPLAY = "ALLOW_SAVE_WITHOUT_CHECKIN_DISPLAY";
    @RBEntry("Determines whether the save button displays on an Edit window for checked-out objects")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_DESC = "ALLOW_SAVE_WITHOUT_CHECKIN_DESC";
    @RBEntry("Although an object may not be ready to be checked in, a user may want to save the intermediate changes made to the checked-out object. The default for this preference (<b>Yes</b>) is to display a <b>Save</b> button on edit windows. Choose <b>No</b> to hide the <b>Save</b> button.")
    public static final String ALLOW_SAVE_WITHOUT_CHECKIN_LONG_DESC = "ALLOW_SAVE_WITHOUT_CHECKIN_LONG_DESC";
    @RBEntry("'Object Checked Out' Alert Message")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_DISPLAY = "EDIT_ACTION_CANCEL_BEHAVIOR_DISPLAY";
    @RBEntry("Determines whether a message will appear when a user clicks Cancel on an Edit window, alerting the user that the object is still checked out.")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_DESC = "EDIT_ACTION_CANCEL_BEHAVIOR_DESC";
    @RBEntry("Select Yes if you want a message to appear when a user clicks Cancel on an Edit window, alerting the user that the object is still checked out. Select No if you don't want an alert message to appear. The default is to display an alert message.")
    public static final String EDIT_ACTION_CANCEL_BEHAVIOR_LONG_DESC = "EDIT_ACTION_CANCEL_BEHAVIOR_LONG_DESC";
    @RBEntry("Check Out and Edit Action")
    public static final String EDIT_ACTION_ON_CHECKED_IN_DISPLAY = "EDIT_ACTION_ON_CHECKED_IN_DISPLAY";
    @RBEntry("Determines whether the Check Out and Edit action displays for checked-in objects.")
    public static final String EDIT_ACTION_ON_CHECKED_IN_DESC = "EDIT_ACTION_ON_CHECKED_IN_DESC";
    @RBEntry("Select Yes to allow users to check out and edit an object in a single step. If No is selected users will need to check out the object before the Edit action is available. The Check Out and Edit action is enabled by default.")
    public static final String EDIT_ACTION_ON_CHECKED_IN_LONG_DESC = "EDIT_ACTION_ON_CHECKED_IN_LONG_DESC";
    @RBEntry("Replace Content Action")
    public static final String REPLACE_CONTENT_OFFERED_DISPLAY = "REPLACE_CONTENT_OFFERED_DISPLAY";
    @RBEntry("Determines whether the Replace Content action displays for checked-in objects.")
    public static final String REPLACE_CONTENT_OFFERED_DESC = "REPLACE_CONTENT_OFFERED_DESC";
    @RBEntry("Select Yes to display the Replace Content Action for checked-in objects. The Replace Content action is enabled by default.")
    public static final String REPLACE_CONTENT_OFFERED_LONG_DESC = "REPLACE_CONTENT_OFFERED_LONG_DESC";
    @RBEntry("Propagate from object")
    public static final String PROPAGATION_LABEL = "PROPAGATION_LABEL";
    @RBEntry("More")
    @RBComment("More hyperlink for TextDisplayComponent")
    public static final String MORE_LABEL = "1";
    @RBEntry("Yes")
    @RBComment("Label for the Boolean value \"True\"")
    public static final String BOOLEAN_TRUE = "BOOLEAN_TRUE";
    @RBEntry("No")
    @RBComment("Label for the Boolean value \"False\"")
    public static final String BOOLEAN_FALSE = "BOOLEAN_FALSE";
    @RBEntry("Undefined")
    @RBComment("Label for the value \"Undefined\"")
    public static final String UNSET = "UNSET";
    @RBEntry("yyyy-MM-dd")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_DISPLAY_FORMAT = "STANDARD_DATE_DISPLAY_FORMAT";
    @RBEntry("yyyy-MM-dd HH:mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_TIME_DISPLAY_FORMAT = "STANDARD_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("yyyy-MM-dd HH:mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT = "STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("yy-M-d")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_DISPLAY_FORMAT = "SHORT_DATE_DISPLAY_FORMAT";
    @RBEntry("yy-M-d HH:mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_TIME_DISPLAY_FORMAT = "SHORT_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("yy-M-d HH:mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT = "SHORT_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("yyyy-MMM-dd")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_DISPLAY_FORMAT = "LONG_DATE_DISPLAY_FORMAT";
    @RBEntry("yyyy-MMM-dd HH:mm")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_TIME_DISPLAY_FORMAT = "LONG_DATE_TIME_DISPLAY_FORMAT";
    @RBEntry("yyyy-MMM-dd HH:mm zzz")
    @RBPseudo(false)
    @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
    public static final String LONG_DATE_TIME_ZONE_DISPLAY_FORMAT = "LONG_DATE_TIME_ZONE_DISPLAY_FORMAT";
    @RBEntry("12")
    @RBPseudo(false)
    @RBComment("Indicates whether hours are shown as 1-12 with am and pm strings or 1-24, in the input mode. Must be 12 or 24.")
    public static final String HOUR_FORMAT = "HOUR_FORMAT";
    @RBEntry("{0} AM")
    @RBComment("If the time display is based on 12 hours, show \"7 AM\" for example.  The order in which the hour and the AM/PM label is displayed depends on locale (for example, Korean language will be \"AM 7\" (SPR 2155026)).")
    public static final String AM_STRING = "AM_STRING";
    @RBEntry("{0} PM")
    @RBComment("If the time display is based on 12 hours, show \"7 PM\" for example.  The order in which the hour and the AM/PM label is displayed depends on locale. (for example, Korean language will be \"PM 7\" (SPR 2155026)).")
    public static final String PM_STRING = "PM_STRING";
    /**
     * ############################################################# !!!! IMPORTANT: The following format and position
     * entries need to be in sync for the calendar widget to work correctly DATE_ENTRY_FORMAT CALENDAR_PARSE_DATE
     * DAY_POSITION MONTH_POSITION YEAR_POSITION DATE_ERROR
     * ##############################################################
     **/
    @RBEntry("yyyy-mm-dd")
    @RBComment("This serves as an instruction to the user for date entry on the UI. It needs to be fully translated (i.e. rearranging the order of MM,DD,YYYY and actual translation of MM,DD,YYYY themselves). - Needs to be in sync with the CALENDAR_PARSE_DATE, DAY_POSITION, MONTH_POSITION and YEAR_POSITION")
    public static final String DATE_ENTRY_FORMAT = "DATE_ENTRY_FORMAT";
    @RBEntry("yyyy-MM-dd HH:mm zzz")
    @RBPseudo(false)
    @RBComment("This is the date format used only when there is defined set of date values to choose from in the UI (i.e. legal value list) and the attribute is configured as date-and-time for input.  Do not translate the date characters (M d Y h m), but modify the date syntax according to locale-specific date syntax.  Needs to be kept in sync with DATE_ENTRY_FORMAT.")
    public static final String DATE_AND_TIME_ENTRY_FORMAT = "DATE_AND_TIME_ENTRY_FORMAT";
    @RBEntry("Invalid date format, the date should be entered in the format yyyy-mm-dd")
    @RBComment("This serves as an instruction to the user for date entry on the UI, the format yyyy-mm-dd should match the same value as DATE_ENTRY_FORMAT")
    public static final String INVALID_DATEENTRY = "INVALID_DATEENTRY";
    @RBEntry("yyyy-MM-dd")
    @RBPseudo(false)
    @RBComment("Used for calendar JavaScript only. Do not translate the date characters (M D Y), but modify the date syntax according to your locale-specific date syntax. This format needs to be in sync with DATE_ENTRY_FORMAT, DAY_POSITION, MONTH_POSITION and YEAR_POSITION.")
    public static final String CALENDAR_PARSE_DATE = "CALENDAR_PARSE_DATE";
    @RBEntry("Y-m-d")
    @RBPseudo(false)
    @RBComment("This is the date format used by the EXT date field.  Do not translate or change the case of the date characters (m d Y), but modify the date syntax according to locale-specific date syntax.")
    public static final String EXT_DATEENTRY_FORMAT = "EXT_DATEENTRY_FORMAT";
    /**
     * These POSITION entries are used for the calendar widget javascript generation
     *
     * One of the keys DAY_POSITION, MONTH_POSITION, YEAR_POSITION must be 1, one must be 2, and one must be 3.
     **/
    @RBEntry("3")
    @RBPseudo(false)
    @RBComment("The position of the day field in a date. May be 1, 2, or 3. (For ex in french, DAY_POSITION is 1 and in English it is 2). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE, MONTH_POSITION and YEAR_POSITION")
    public static final String DAY_POSITION = "DAY_POSITION";
    @RBEntry("2")
    @RBPseudo(false)
    @RBComment("The position of the month field in a date. May be 1, 2, or 3. (For ex in french, MONTH_POSITION is 2 and in English it is 1). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and YEAR_POSITION")
    public static final String MONTH_POSITION = "MONTH_POSITION";
    @RBEntry("1")
    @RBPseudo(false)
    @RBComment("The position of the year field in a date. May be 1, 2, or 3. (For ex in japanese, YEAR_POSITION is 1 and in English it is 3). Needs to be in sync with DATE_ENTRY_FORMAT, CALENDAR_PARSE_DATE,  DAY_POSITION and MONTH_POSITION")
    public static final String YEAR_POSITION = "YEAR_POSITION";
    @RBEntry("The date is either not in the required format yyyy-mm-dd or it is out of range.")
    @RBComment("Needs to be in sync with the CALENDAR_PARSE_DATE and the DATE_ENTRY_FORMAT")
    public static final String DATE_ERROR = "DATE_ERROR";
    @RBEntry("Calendar")
    public static final String CALENDAR_TITLE = "CALENDAR_TITLE";
    @RBEntry("Su")
    @RBPseudoTrans("S失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Sunday")
    public static final String WEEK_SUN = "WEEK_SUN";
    @RBEntry("Mo")
    @RBPseudoTrans("M失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Monday")
    public static final String WEEK_MON = "WEEK_MON";
    @RBEntry("Tu")
    @RBPseudoTrans("T失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Tuesday")
    public static final String WEEK_TUE = "WEEK_TUE";
    @RBEntry("We")
    @RBPseudoTrans("W失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Wednesday")
    public static final String WEEK_WED = "WEEK_WED";
    @RBEntry("Th")
    @RBPseudoTrans("T失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Thursday")
    public static final String WEEK_THU = "WEEK_THU";
    @RBEntry("Fr")
    @RBPseudoTrans("F失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Friday")
    public static final String WEEK_FRI = "WEEK_FRI";
    @RBEntry("Sa")
    @RBPseudoTrans("S失")
    @RBComment("Abbreviated date string for calendar popup, two or less characters representing day of week: Saturday")
    public static final String WEEK_SAT = "WEEK_SAT";
    /**
     *
     * The month strings can either be the name of the month or the number(e.g Asian languages)
     *
     **/
    @RBEntry("January")
    public static final String MON_JAN = "MON_JAN";
    @RBEntry("February")
    public static final String MON_FEB = "MON_FEB";
    @RBEntry("March")
    public static final String MON_MAR = "MON_MAR";
    @RBEntry("April")
    public static final String MON_APR = "MON_APR";
    @RBEntry("May")
    public static final String MON_MAY = "MON_MAY";
    @RBEntry("June")
    public static final String MON_JUN = "MON_JUN";
    @RBEntry("July")
    public static final String MON_JUL = "MON_JUL";
    @RBEntry("August")
    public static final String MON_AUG = "MON_AUG";
    @RBEntry("September")
    public static final String MON_SEP = "MON_SEP";
    @RBEntry("October")
    public static final String MON_OCT = "MON_OCT";
    @RBEntry("November")
    public static final String MON_NOV = "MON_NOV";
    @RBEntry("December")
    public static final String MON_DEC = "MON_DEC";
    @RBEntry("Created By")
    @RBComment("Label used for the column header or label for the full name of the creator of an object.")
    public static final String CREATOR_FULL_NAME = "CREATOR_FULL_NAME";
    @RBEntry("Mail to Creator")
    @RBComment("Label for the Mail to Creator column.")
    public static final String CREATOR_MAILTO = "CREATOR_MAILTO";
    @RBEntry("Context")
    @RBComment("Label used for the column header or label for the container of an object.")
    public static final String CONTAINER_NAME = "CONTAINER_NAME";
    @RBEntry("Representation Name")
    @RBComment("Table column label for the name of the representation associated with a published content link.")
    public static final String REPRESENTATION_NAME = "REPRESENTATION_NAME";
    @RBEntry("Link Type")
    @RBComment("Table column label for the type of the published content links.")
    public static final String PUBLISHED_CONTENT_LINK_TYPE = "PUBLISHED_CONTENT_LINK_TYPE";
    @RBEntry("Keep checked out after checkin")
    @RBComment("Label for the Keep Checked Out checkbox on the Set Attributes step of object creation wizards")
    public static final String KEEP_CHECKED_OUT_CHECKBOX_LABEL = "KEEP_CHECKED_OUT_CHECKBOX_LABEL";
    @RBEntry(" (Default)")
    @RBComment("Used to append to selected value of a combobox.")
    public static final String DEFAULT_DISPLAY_STRING = "DEFAULT_DISPLAY_STRING";
    @RBEntry(" (Current)")
    @RBComment("Used to append to current selected value of a combobox.")
    public static final String CURRENT_DISPLAY_STRING = "CURRENT_DISPLAY_STRING";
    @RBEntry("Define Object")
    @RBComment("Define Object label")
    public static final String PRIVATE_CONSTANT_0 = "object.defineItemWizStep.description";
    @RBEntry("Define Object")
    public static final String PRIVATE_CONSTANT_1 = "object.defineItemWizStep.tooltip";
    @RBEntry("Define Object")
    public static final String PRIVATE_CONSTANT_2 = "object.defineItemWizStep.title";
    @RBEntry("Set Attributes")
    @RBComment("Set Attributes label")
    public static final String PRIVATE_CONSTANT_3 = "object.defineItemAttributesWizStep.description";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_4 = "object.defineItemAttributesWizStep.tooltip";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_5 = "object.defineItemAttributesWizStep.title";
    @RBEntry("Set Attributes")
    @RBComment("Set Attributes label")
    public static final String PRIVATE_CONSTANT_6 = "object.setAttributesWizStep.description";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_7 = "object.setAttributesWizStep.tooltip";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_8 = "object.setAttributesWizStep.title";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_9 = "object.editAttributesWizStep.description";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_10 = "object.editAttributesWizStep.tooltip";
    @RBEntry("Set Attributes")
    public static final String PRIVATE_CONSTANT_11 = "object.editAttributesWizStep.title";
    @RBEntry("Set Shared Attributes")
    @RBComment("Set Shared Attributes")
    public static final String PRIVATE_CONSTANT_12 = "object.typeRefWizStep.description";
    @RBEntry("Set Shared Attributes")
    public static final String PRIVATE_CONSTANT_13 = "object.typeRefWizStep.tooltip";
    @RBEntry("Set Shared Attributes")
    public static final String PRIVATE_CONSTANT_14 = "object.typeRefWizStep.title";
    @RBEntry("Go to Latest")
    @RBComment("Used as the description for the go to latest hyperlink.")
    public static final String GO_TO_LATEST = "GO_TO_LATEST";
    @RBEntry("(Go to Original Version)")
    @RBComment("Used as the description for the go to original hyperlink.")
    public static final String GO_TO_ORIGINAL = "GO_TO_ORIGINAL";
    @RBEntry("(Go to Working Copy)")
    @RBComment("Used as the description for the go to working hyperlink.")
    public static final String GO_TO_WORKING = "GO_TO_WORKING";
    @RBEntry("(Unassignable)")
    @RBComment("Used to display as value for read only attributes with null values in create and edit UIs.")
    public static final String UNASSIGNABLE_DISPLAY_STRING = "UNASSIGNABLE_DISPLAY_STRING";
    @RBEntry("(Hidden)")
    @RBComment("Used to display as value for hidden value constrained attributes in all modes (create, edit and view modes).")
    public static final String HIDDEN_DISPLAY_STRING = "HIDDEN_DISPLAY_STRING";
    @RBEntry("Message")
    @RBComment("Default title the message dialog title as per UI standard: http://ui/standards/standards.php?id=28&disp=all")
    public static final String MESSAGE_DIALOG_TITLE = "MESSAGE_DIALOG_TITLE";
    @RBEntry("Find Organization")
    @RBComment("Label for org picker title launched using OrgIDDataUtility")
    public static final String ORG_PICKER_LABEL = "ORG_PICKER_LABEL";
    @RBEntry("Host")
    @RBComment("Table column label \"Host\"")
    public static final String PROJECT_HOST = "PROJECT_HOST";
    @RBEntry("Page Content Frame")
    @RBComment("Setting for the html attribute of title on the iframe for template processing main page content. This is read to users using screen readers.")
    public static final String TP_IFRAME_TITLE = "TP_IFRAME_TITLE";
    @RBEntry("Use Default")
    @RBComment("Label for the use default button that is sometimes displayed on attributes in wizards. (Depending on a pref) when pressed the default value for the attribute is used in the field.")
    public static final String USE_DEFAULT = "USE_DEFAULT";
    @RBEntry("Start")
    @RBComment("Default label for the \"Start\" component in a date range.")
    public static final String DATE_RANGE_START = "DATE_RANGE_START";
    @RBEntry("End")
    @RBComment("Default label for the \"End\" component in a date range.")
    public static final String DATE_RANGE_END = "DATE_RANGE_END";
    @RBEntry("Rich text attribute display is not supported in table view for this object")
    @RBComment("Tooltip value to show for attribute that is rich text when rich text is not being shown.")
    public static final String RICH_TEXT_HIDDEN_TOOLTIP_MSG = "RICH_TEXT_HIDDEN_TOOLTIP_MSG";
    @RBEntry("Error occurred while rendering")
    @RBComment("Error message that gets displayed for an attribute value when an error occurs rendering the attribute.")
    public static final String RENDERING_ATTR_VALUE_ERROR_MESSAGE = "RENDERING_ATTR_VALUE_ERROR_MESSAGE";
    @RBEntry("Invalid format: '{0}'")
    @RBComment("Error message that gets displayed for an attribute value when an the date format string is invalid.")
    public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
    @RBEntry("Add by Name")
    @RBComment("The label that appears inside the auto suggest panel for related entities for name.")
    public static final String ADD_BY_NAME = "ADD_BY_NAME";
    @RBEntry("Add by Number")
    @RBComment("The label that appears inside the auto suggest panel for related entities for number.")
    public static final String ADD_BY_NUMBER = "ADD_BY_NUMBER";
    @RBEntry("URL Label")
    @RBComment("The label for the URL Label input field for a Hyperlink attribute.")
    public static final String HYPERLINK_LABEL = "HYPERLINK_LABEL";
    @RBEntry("URL")
    @RBComment("The label for the URL input field for a Hyperlink attribute.")
    public static final String HYPERLINK_URL = "HYPERLINK_URL";
    @RBEntry("Changes you make to the Public tab are carried forward to the home pages of all users in your site except those users who have already modified their home pages.")
    @RBComment("Message thats displayed in an inline message on the site admins public homepage tab")
    public static final String SITE_ADMIN_PUBLIC_HOMEPAGE_TAB_INLINE_HELP = "SITE_ADMIN_PUBLIC_HOMEPAGE_TAB_INLINE_HELP";
    // added for defect D-20061
    @RBEntry("Select Classification")
    @RBComment("Title for Classification Picker")
    public static final String CLASSIFICATION_PICKER_TITLE = "CLASSIFICATION_PICKER_TITLE";
    @RBEntry("Select")
    @RBComment("Title for Structured Enumeration Picker")
    public static final String STRUCTURED_ENUMERATION_PICKER_TITLE = "STRUCTURED_ENUMERATION_PICKER_TITLE";
    @RBEntry("-- Select --")
    @RBComment("Inline Help text that appears as the top item in a required combobox.")
    public static final String REQUIRED_ENTRY_COMBOBOX_INLINE_HELP = "REQUIRED_ENTRY_COMBOBOX_INLINE_HELP";
    // Tooltips to be displayed on the "traffic light" multi-state graphical attribute widget
    @RBEntry("Red")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the red/stopped state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_RED_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_RED_STATE";
    @RBEntry("Yellow")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the yellow/caution state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_YELLOW_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_YELLOW_STATE";
    @RBEntry("Green")
    @RBComment("The tooltip to be displayed on the traffic light icon when it is displaying the green/go state.")
    public static final String TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_GREEN_STATE = "TRAFFIC_LIGHT_WIDGET_TOOLTIP_FOR_GREEN_STATE";
    // end of "traffic light" tooltips
    @RBEntry("{0}; {1} in [{2} .. {3}]")
    @RBComment("Tooltip to be displayed on the progress bar graphical representation. For example 75%; 7.5 in [0 .. 10]")
    public static final String PROGRESS_BAR_TOOLTIP = "PROGRESS_BAR_TOOLTIP";
    @RBEntry("(Unknown)")
    @RBComment("Short message displayed in table cell to indicate progress bar cannot be displayed (detailed explanation in the tooltip)")
    public static final String PROGRESS_BAR_ERROR = "PROGRESS_BAR_ERROR";
    @RBEntry("Invalid range: Parameter \"{0}\" is the same as parameter \"{1}\"")
    @RBComment("Error message to be displayed in a progress bar tooltip when the range is empty. {0} will be 'begin' and {1} will be 'end'")
    public static final String PROGRESS_BAR_EMPTY_RANGE = "PROGRESS_BAR_EMPTY_RANGE";
    @RBEntry("Evaluation error: Value is not a number")
    @RBComment("Error message to be displayed in a progress bar tooltip when the percent complete computation fails to produce a number")
    public static final String PROGRESS_BAR_NAN_ERROR = "PROGRESS_BAR_NAN_ERROR";
    @RBEntry("Evaluation error: Value is too large")
    @RBComment("Error message to be displayed in a progress bar tooltip when the percent complete result is 'infinite'")
    public static final String PROGRESS_BAR_INFINITY_ERROR = "PROGRESS_BAR_INFINITY_ERROR";
    @RBEntry("[{0}% .. {1}%)")
    @RBComment("Label used to identify progress bar groups, e.g. '[10% .. 20%)', '[20% .. 30%)'.  N.B. These are half-open intervals. ")
    public static final String PROGRESS_BAR_GROUP_LABEL = "PROGRESS_BAR_GROUP_LABEL";
    @RBEntry("None")
    @RBComment("Label used to identify the table group with no progress bars")
    public static final String PROGRESS_BAR_NONE_GROUP_LABEL = "PROGRESS_BAR_NONE_GROUP_LABEL";
    @RBEntry("{0}%")
    @RBComment("Tooltip to be displayed on the percent complete graphical representation")
    public static final String PERCENT_COMPLETE_TOOLTIP = "PERCENT_COMPLETE_TOOLTIP";
    @RBEntry("Percent value has unexpected units: {0}")
    @RBComment("Error message displayed when a percent complete value has unexpected units")
    public static final String PERCENT_COMPLETE_BAD_UNITS = "PERCENT_COMPLETE_BAD_UNITS";
    @RBEntry("Unable to convert value to a number: {0}")
    @RBComment("Error message displayed when a graphical attribute configuration value is non-numeric")
    public static final String GA_UNABLE_TO_CONVERT_VALUE_TO_NUMBER = "GA_UNABLE_TO_CONVERT_VALUE_TO_NUMBER";
    @RBEntry("Unable to convert parameter \"{0}\" to a number: {1}")
    @RBComment("Error message displayed when a graphical attribute configuration parameter is non-numeric")
    public static final String GA_UNABLE_TO_CONVERT_PARAMETER_TO_NUMBER = "GA_UNABLE_TO_CONVERT_PARAMETER_TO_NUMBER";
    @RBEntry("Value is not specified")
    @RBComment("Error message displayed when a graphical attribute value is missing")
    public static final String GA_MISSING_VALUE = "GA_MISSING_VALUE";
    @RBEntry("Parameter \"{0}\" is undefined")
    @RBComment("Error message displayed when a graphical attribute parameter is missing")
    public static final String GA_MISSING_PARAMETER = "GA_MISSING_PARAMETER";
    @RBEntry("Value is not a number: {0}")
    @RBComment("Error message displayed when a graphical attribute value is not recognized as numeric")
    public static final String GA_UNRECOGNIZED_NUMERIC_VALUE = "GA_UNRECOGNIZED_NUMERIC_VALUE";
    @RBEntry("Parameter \"{0}\" is not a number: {1}")
    @RBComment("Error message displayed when a graphical attribute parameter is not recognized as numeric")
    public static final String GA_UNRECOGNIZED_NUMERIC_PARAMETER = "GA_UNRECOGNIZED_NUMERIC_PARAMETER";
    @RBEntry("Unexpected unit conversion error: {0}")
    @RBComment("Error message displayed when an unexpected unit conversion error is encountered")
    public static final String GA_UNEXPECTED_UNIT_CONVERSION_ERROR = "GA_UNEXPECTED_UNIT_CONVERSION_ERROR";
    @RBEntry("Value \"{0}\" does not have expected units: {1}")
    @RBComment("Error message displayed when a graphical attribute value incompatible or missing units")
    public static final String GA_BAD_VALUE_UNITS = "GA_BAD_VALUE_UNITS";
    @RBEntry("Contact your administrator for assistance")
    @RBComment("Error message displayed when any other graphical attribute errors are generated")
    public static final String GA_CONTACT_ADMIN = "GA_CONTACT_ADMIN";
    @RBEntry("??")
    @RBComment("Placeholder used in the tooltip string when values are unknown. For example '??%; 10 in [0 .. ??]'")
    public static final String GA_VALUE_UNKNOWN = "GA_VALUE_UNKNOWN";
    @RBEntry("Invalid parameters")
    @RBComment("Error message shown when the progress bar parameters are null or empty.")
    public static final String GA_INVALID_PARAMETERS = "GA_INVALID_PARAMETERS";


    @RBEntry("Valid State")
    @RBComment("Valid State")
    public static final String SEARCH_TL = "SEARCH_TL";

    @RBEntry("PDF")
    @RBComment("PDF")
    public static final String PDF_LINK = "PDF_LINK";

    @RBEntry("Is Latest")
    @RBComment("Is Latest")
    public static final String IS_LATEST = "IS_LATEST";
}
